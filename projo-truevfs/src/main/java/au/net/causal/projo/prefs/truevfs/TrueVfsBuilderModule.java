package au.net.causal.projo.prefs.truevfs;

import au.net.causal.projo.prefs.ProjoBuilderModule;
import au.net.causal.projo.prefs.ProjoStoreBuilder;

public class TrueVfsBuilderModule implements ProjoBuilderModule
{
	@Override
	public void configure(ProjoStoreBuilder builder)
	{
		if (builder == null)
			throw new NullPointerException("builder == null");
		
		TrueVfsPathToStringTransformer.configureBuilder(builder);
	}

}
