package au.net.causal.projo.prefs.truevfs;

import java.nio.file.Path;
import java.nio.file.Paths;

import net.java.truevfs.access.TPath;
import au.net.causal.projo.prefs.PreferencesException;
import au.net.causal.projo.prefs.ProjoStoreBuilder;
import au.net.causal.projo.prefs.StandardTransformPhase;
import au.net.causal.projo.prefs.transform.PathToStringTransformer;

public class TrueVfsPathToStringTransformer extends PathToStringTransformer
{
	public static void configureBuilder(ProjoStoreBuilder builder)
	{
		builder.replaceTransformer(PathToStringTransformer.class, new TrueVfsPathToStringTransformer(), StandardTransformPhase.DATATYPE);
	}
	
	private boolean isWindowsNetworkPath(String s)
	{
		return(s.startsWith("\\\\"));
	}
	
	@Override
	protected String pathToStringForSpecialFileSystem(Path path) 
	throws PreferencesException
	{
		//If the path is a TrueVFS path then just write the path
		if (path instanceof TPath)
			return(path.toString());
		
		//Otherwise use default behaviour
		return(null);
	}
	
	@Override
	protected Path stringToPathForSpecialFileSystem(String s) 
	throws PreferencesException 
	{
		//Need some special handling for Windows network names since TrueVFS seems to choke on them when they start with backslashes
		//So in this case we force Windows network names to be resolved by local file system directly
		if (isWindowsNetworkPath(s))
			return(Paths.get(s));
		
		//Otherwise make a TPath
		TPath tPath = new TPath(s);
		return(tPath);
	}
}
