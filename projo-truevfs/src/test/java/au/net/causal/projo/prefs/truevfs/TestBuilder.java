package au.net.causal.projo.prefs.truevfs;

import static org.junit.Assert.*;

import java.io.File;
import java.nio.file.Path;
import java.util.Properties;

import net.java.truevfs.access.TPath;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import au.net.causal.projo.annotation.Preference;
import au.net.causal.projo.annotation.PreferenceRoot;
import au.net.causal.projo.annotation.Projo;
import au.net.causal.projo.prefs.ProjoStore;
import au.net.causal.projo.prefs.ProjoStoreBuilder;
import au.net.causal.projo.prefs.properties.PropertiesPreferenceNode;

public class TestBuilder
{
	private static final Logger log = LoggerFactory.getLogger(TestBuilder.class);
		
	@Test
	public void testSave()
	throws Exception
	{
		Properties props = new Properties();
		PropertiesPreferenceNode node = new PropertiesPreferenceNode(props);
		
		ProjoStore store = ProjoStoreBuilder.builder().defaults()
							.node(node)
							.configureModule(new TrueVfsBuilderModule())
							.build();		
		
		MyConfig config = new MyConfig("galah", new TPath(File.separator + "tmp"));
		store.save(config);
		
		log.info(props.toString());
		
		String name = props.getProperty("config.name");
		String configDir = props.getProperty("config.configDirectory");
				
		assertEquals("Wrong name.", "galah", name);
		assertEquals("Wrong path.", File.separator + "tmp", configDir);
	}
	
	@Test
	public void testLoad()
	throws Exception
	{
		Properties props = new Properties();
		props.setProperty("config.name", "galah");
		props.setProperty("config.configDirectory", File.separator + "tmp");
		
		PropertiesPreferenceNode node = new PropertiesPreferenceNode(props);
		
		ProjoStore store = ProjoStoreBuilder.builder().defaults()
							.node(node)
							.configureModule(new TrueVfsBuilderModule())
							.build();		
		
		MyConfig config = store.read(MyConfig.class);
		log.info(ToStringBuilder.reflectionToString(config, ToStringStyle.SHORT_PREFIX_STYLE));
		
		assertEquals("Wrong name.", "galah", config.getName());
		assertEquals("Wrong path.", new TPath(File.separator + "tmp"), config.getConfigDirectory());
	}
	
	@Projo
	@PreferenceRoot(name="config")
	public static class MyConfig
	{
		@Preference
		private String name;
		
		@Preference
		private Path configDirectory;
		
		public MyConfig()
		{
			this(null, null);
		}
		
		public MyConfig(String name, Path configDirectory)
		{
			this.name = name;
			this.configDirectory = configDirectory;
		}

		public String getName()
		{
			return(name);
		}

		public void setName(String name)
		{
			this.name = name;
		}

		public Path getConfigDirectory()
		{
			return(configDirectory);
		}

		public void setConfigDirectory(Path configDirectory)
		{
			this.configDirectory = configDirectory;
		}
	}

}
