package au.net.causal.projo.prefs.truevfs;

import static org.junit.Assert.*;

import java.io.File;
import java.nio.file.Path;

import net.java.truevfs.access.TPath;

import org.junit.Test;

import au.net.causal.projo.prefs.PreferenceKeyMetadata;
import au.net.causal.projo.prefs.PreferencesException;
import au.net.causal.projo.prefs.StandardTransformPhase;
import au.net.causal.projo.prefs.TransformerPreferenceNode;
import au.net.causal.projo.prefs.memory.InMemoryPreferenceNode;
import au.net.causal.projo.prefs.transform.Base64Transformer;
import au.net.causal.projo.prefs.transform.IntToStringTransformer;
import au.net.causal.projo.prefs.transform.ListTransformer;
import au.net.causal.projo.prefs.transform.NumericCastTransformer;
import au.net.causal.projo.prefs.transform.RectangleTransformer;
import au.net.causal.projo.prefs.transform.StringToByteTransformer;

public class TestTrueVfsPathTransform
{
	@Test
	public void testPut()
	throws PreferencesException
	{
		InMemoryPreferenceNode<String> node = new InMemoryPreferenceNode<>(String.class);
		TransformerPreferenceNode xNode = new TransformerPreferenceNode(node);
		xNode.addTransformer(new NumericCastTransformer(), StandardTransformPhase.DATATYPE);
		xNode.addTransformer(new IntToStringTransformer(), StandardTransformPhase.DATATYPE);
		xNode.addTransformer(new StringToByteTransformer(), StandardTransformPhase.DATATYPE);
		xNode.addTransformer(new Base64Transformer(), StandardTransformPhase.DATATYPE);
		xNode.addTransformer(new RectangleTransformer(), StandardTransformPhase.DATATYPE);
		xNode.addTransformer(new ListTransformer(), StandardTransformPhase.DATATYPE);
		xNode.addTransformer(new TrueVfsPathToStringTransformer(), StandardTransformPhase.DATATYPE);
		
		TPath path = new TPath(File.separator + "tmp");
		xNode.putValue("dir", path, new PreferenceKeyMetadata<>(Path.class));
		
		String nativeValue = node.getNativeValue("dir");
		assertEquals("Wrong native value.", File.separator + "tmp", nativeValue);
	}
	
	@Test
	public void testGet()
	throws PreferencesException
	{
		InMemoryPreferenceNode<String> node = new InMemoryPreferenceNode<>(String.class);
		node.putNativeValue("dir", File.separator + "tmp");
		
		TransformerPreferenceNode xNode = new TransformerPreferenceNode(node);
		xNode.addTransformer(new NumericCastTransformer(), StandardTransformPhase.DATATYPE);
		xNode.addTransformer(new IntToStringTransformer(), StandardTransformPhase.DATATYPE);
		xNode.addTransformer(new StringToByteTransformer(), StandardTransformPhase.DATATYPE);
		xNode.addTransformer(new Base64Transformer(), StandardTransformPhase.DATATYPE);
		xNode.addTransformer(new RectangleTransformer(), StandardTransformPhase.DATATYPE);
		xNode.addTransformer(new ListTransformer(), StandardTransformPhase.DATATYPE);
		xNode.addTransformer(new TrueVfsPathToStringTransformer(), StandardTransformPhase.DATATYPE);
		
		Path result = xNode.getValue("dir", new PreferenceKeyMetadata<>(Path.class));
		assertEquals("Wrong result.", new TPath(File.separator + "tmp"), result);
	}
	
	@Test
	public void testRemove()
	throws PreferencesException
	{
		InMemoryPreferenceNode<String> node = new InMemoryPreferenceNode<>(String.class);
		node.putNativeValue("dir", File.separator + "tmp");
		
		TransformerPreferenceNode xNode = new TransformerPreferenceNode(node);
		xNode.addTransformer(new NumericCastTransformer(), StandardTransformPhase.DATATYPE);
		xNode.addTransformer(new IntToStringTransformer(), StandardTransformPhase.DATATYPE);
		xNode.addTransformer(new StringToByteTransformer(), StandardTransformPhase.DATATYPE);
		xNode.addTransformer(new Base64Transformer(), StandardTransformPhase.DATATYPE);
		xNode.addTransformer(new RectangleTransformer(), StandardTransformPhase.DATATYPE);
		xNode.addTransformer(new ListTransformer(), StandardTransformPhase.DATATYPE);
		xNode.addTransformer(new TrueVfsPathToStringTransformer(), StandardTransformPhase.DATATYPE);
		
		xNode.removeValue("dir", new PreferenceKeyMetadata<>(Path.class));
		
		String data = node.getNativeValue("dir");
		assertNull("Value should not exist.", data);
	}
	
	@Test
	public void testDataTypeSupport()
	throws PreferencesException
	{
		InMemoryPreferenceNode<String> node = new InMemoryPreferenceNode<>(String.class);
		TransformerPreferenceNode xNode = new TransformerPreferenceNode(node);
		xNode.addTransformer(new NumericCastTransformer(), StandardTransformPhase.DATATYPE);
		xNode.addTransformer(new IntToStringTransformer(), StandardTransformPhase.DATATYPE);
		xNode.addTransformer(new StringToByteTransformer(), StandardTransformPhase.DATATYPE);
		xNode.addTransformer(new Base64Transformer(), StandardTransformPhase.DATATYPE);
		xNode.addTransformer(new RectangleTransformer(), StandardTransformPhase.DATATYPE);
		xNode.addTransformer(new ListTransformer(), StandardTransformPhase.DATATYPE);
		xNode.addTransformer(new TrueVfsPathToStringTransformer(), StandardTransformPhase.DATATYPE);
		
		boolean pathSupport = xNode.isDataTypeSupported(new PreferenceKeyMetadata<>(Path.class));
		assertTrue("Wrong support.", pathSupport);
	}
}
