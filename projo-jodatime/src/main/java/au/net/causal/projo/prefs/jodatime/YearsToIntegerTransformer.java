package au.net.causal.projo.prefs.jodatime;

import org.joda.time.Years;

import au.net.causal.projo.prefs.PreferencesException;
import au.net.causal.projo.prefs.transform.GenericToIntegerTransformer;

public class YearsToIntegerTransformer extends GenericToIntegerTransformer<Years>
{
	public YearsToIntegerTransformer()
	{
		super(Years.class);
	}

	@Override
	protected int valueToInteger(Years value) throws PreferencesException
	{
		return(value.getYears());
	}

	@Override
	protected Years integerToValue(int n) throws PreferencesException
	{
		return(Years.years(n));
	}

}
