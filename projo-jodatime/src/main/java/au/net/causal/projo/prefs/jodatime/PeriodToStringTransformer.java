package au.net.causal.projo.prefs.jodatime;

import org.joda.time.Period;
import org.joda.time.format.ISOPeriodFormat;
import org.joda.time.format.PeriodFormatter;

import au.net.causal.projo.prefs.PreferencesException;
import au.net.causal.projo.prefs.transform.GenericToStringTransformer;

public class PeriodToStringTransformer extends GenericToStringTransformer<Period>
{
	private final PeriodFormatter formatter;
	
	public PeriodToStringTransformer(PeriodFormatter formatter)
	{
		super(Period.class);
		
		if (formatter == null)
			throw new NullPointerException("formatter == null");
		
		this.formatter = formatter;
	}
	
	public PeriodToStringTransformer()
	{
		this(ISOPeriodFormat.standard());
	}

	@Override
	protected String valueToString(Period value) throws PreferencesException
	{
		if (value == null)
			return(null);
		
		return(value.toString(formatter));
	}

	@Override
	protected Period stringToValue(String s) throws PreferencesException
	{
		if (s == null)
			return(null);
		
		try
		{
			return(Period.parse(s, formatter));
		}
		catch (IllegalArgumentException e)
		{
			//Thrown when parsing fails
			throw new PreferencesException("Could not parse period '" + s + "'.", e);
		}
	}
}
