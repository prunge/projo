package au.net.causal.projo.prefs.jodatime;

import org.joda.time.Months;

import au.net.causal.projo.prefs.PreferencesException;
import au.net.causal.projo.prefs.transform.GenericToIntegerTransformer;

public class MonthsToIntegerTransformer extends GenericToIntegerTransformer<Months>
{
	public MonthsToIntegerTransformer()
	{
		super(Months.class);
	}

	@Override
	protected int valueToInteger(Months value) throws PreferencesException
	{
		return(value.getMonths());
	}

	@Override
	protected Months integerToValue(int n) throws PreferencesException
	{
		return(Months.months(n));
	}

}
