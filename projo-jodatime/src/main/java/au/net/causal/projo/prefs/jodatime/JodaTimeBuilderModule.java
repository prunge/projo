package au.net.causal.projo.prefs.jodatime;

import au.net.causal.projo.prefs.ProjoBuilderModule;
import au.net.causal.projo.prefs.ProjoStoreBuilder;
import au.net.causal.projo.prefs.StandardTransformPhase;

public class JodaTimeBuilderModule implements ProjoBuilderModule
{
	@Override
	public void configure(ProjoStoreBuilder builder)
	{
		if (builder == null)
			throw new NullPointerException("builder == null");
		
		builder.addTransformer(new DateTimeToStringTransformer(), StandardTransformPhase.DATATYPE);
		builder.addTransformer(new MutableDateTimeToStringTransformer(), StandardTransformPhase.DATATYPE);
		builder.addTransformer(new LocalDateTimeToStringTransformer(), StandardTransformPhase.DATATYPE);
		builder.addTransformer(new LocalDateToStringTransformer(), StandardTransformPhase.DATATYPE);
		builder.addTransformer(new LocalTimeToStringTransformer(), StandardTransformPhase.DATATYPE);
		builder.addTransformer(new DateTimeZoneToStringTransformer(), StandardTransformPhase.DATATYPE);
		builder.addTransformer(new InstantToStringTransformer(), StandardTransformPhase.DATATYPE);
		
		builder.addTransformer(new SecondsToIntegerTransformer(), StandardTransformPhase.DATATYPE);
		builder.addTransformer(new MinutesToIntegerTransformer(), StandardTransformPhase.DATATYPE);
		builder.addTransformer(new HoursToIntegerTransformer(), StandardTransformPhase.DATATYPE);
		builder.addTransformer(new DaysToIntegerTransformer(), StandardTransformPhase.DATATYPE);
		builder.addTransformer(new WeeksToIntegerTransformer(), StandardTransformPhase.DATATYPE);
		builder.addTransformer(new MonthsToIntegerTransformer(), StandardTransformPhase.DATATYPE);
		builder.addTransformer(new YearsToIntegerTransformer(), StandardTransformPhase.DATATYPE);
		
		builder.addTransformer(new MonthDayToStringTransformer(), StandardTransformPhase.DATATYPE);
		builder.addTransformer(new YearMonthToStringTransformer(), StandardTransformPhase.DATATYPE);
		
		builder.addTransformer(new IntervalToStringTransformer(), StandardTransformPhase.DATATYPE);
		builder.addTransformer(new MutableIntervalToStringTransformer(), StandardTransformPhase.DATATYPE);
		builder.addTransformer(new PeriodToStringTransformer(), StandardTransformPhase.DATATYPE);
		builder.addTransformer(new MutablePeriodToStringTransformer(), StandardTransformPhase.DATATYPE);
	}

}
