package au.net.causal.projo.prefs.jodatime;

import org.joda.time.Days;

import au.net.causal.projo.prefs.PreferencesException;
import au.net.causal.projo.prefs.transform.GenericToIntegerTransformer;

public class DaysToIntegerTransformer extends GenericToIntegerTransformer<Days>
{
	public DaysToIntegerTransformer()
	{
		super(Days.class);
	}

	@Override
	protected int valueToInteger(Days value) throws PreferencesException
	{
		return(value.getDays());
	}

	@Override
	protected Days integerToValue(int n) throws PreferencesException
	{
		return(Days.days(n));
	}

}
