package au.net.causal.projo.prefs.jodatime;

import org.joda.time.MutableInterval;

import au.net.causal.projo.prefs.PreferencesException;
import au.net.causal.projo.prefs.transform.GenericToStringTransformer;

public class MutableIntervalToStringTransformer extends GenericToStringTransformer<MutableInterval>
{
	public MutableIntervalToStringTransformer()
	{
		super(MutableInterval.class);
	}
	
	@Override
	protected String valueToString(MutableInterval value) throws PreferencesException
	{
		if (value == null)
			return(null);
		
		return(value.toString());
	}

	@Override
	protected MutableInterval stringToValue(String s) throws PreferencesException
	{
		if (s == null)
			return(null);
		
		try
		{
			return(MutableInterval.parse(s));
		}
		catch (IllegalArgumentException e)
		{
			//Thrown when parsing fails
			throw new PreferencesException("Could not parse interval '" + s + "'.", e);
		}
	}
}
