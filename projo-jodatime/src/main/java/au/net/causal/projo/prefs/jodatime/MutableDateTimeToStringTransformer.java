package au.net.causal.projo.prefs.jodatime;

import org.joda.time.MutableDateTime;
import org.joda.time.format.DateTimeFormatter;
import org.joda.time.format.ISODateTimeFormat;

import au.net.causal.projo.prefs.PreferencesException;
import au.net.causal.projo.prefs.transform.GenericToStringTransformer;

public class MutableDateTimeToStringTransformer extends GenericToStringTransformer<MutableDateTime>
{
	private final DateTimeFormatter formatter;
	
	public MutableDateTimeToStringTransformer(DateTimeFormatter formatter)
	{
		super(MutableDateTime.class);
		
		if (formatter == null)
			throw new NullPointerException("formatter == null");
		
		this.formatter = formatter;
	}
	
	public MutableDateTimeToStringTransformer()
	{
		this(ISODateTimeFormat.dateTime().withOffsetParsed());
	}

	@Override
	protected String valueToString(MutableDateTime value) throws PreferencesException
	{
		if (value == null)
			return(null);
		
		return(value.toString(formatter));
	}

	@Override
	protected MutableDateTime stringToValue(String s) throws PreferencesException
	{
		if (s == null)
			return(null);
		
		try
		{
			return(MutableDateTime.parse(s, formatter));
		}
		catch (IllegalArgumentException e)
		{
			//Thrown when parsing fails
			throw new PreferencesException("Could not parse date '" + s + "'.", e);
		}
	}
}
