package au.net.causal.projo.prefs.jodatime;

import org.joda.time.Minutes;

import au.net.causal.projo.prefs.PreferencesException;
import au.net.causal.projo.prefs.transform.GenericToIntegerTransformer;

public class MinutesToIntegerTransformer extends GenericToIntegerTransformer<Minutes>
{
	public MinutesToIntegerTransformer()
	{
		super(Minutes.class);
	}

	@Override
	protected int valueToInteger(Minutes value) throws PreferencesException
	{
		return(value.getMinutes());
	}

	@Override
	protected Minutes integerToValue(int n) throws PreferencesException
	{
		return(Minutes.minutes(n));
	}

}
