package au.net.causal.projo.prefs.jodatime;

import org.joda.time.LocalTime;
import org.joda.time.format.DateTimeFormatter;
import org.joda.time.format.DateTimeFormatterBuilder;
import org.joda.time.format.ISODateTimeFormat;

import au.net.causal.projo.prefs.PreferencesException;
import au.net.causal.projo.prefs.transform.GenericToStringTransformer;

public class LocalTimeToStringTransformer extends GenericToStringTransformer<LocalTime>
{
	private final DateTimeFormatter formatter;
	
	public LocalTimeToStringTransformer(DateTimeFormatter formatter)
	{
		super(LocalTime.class);
		
		if (formatter == null)
			throw new NullPointerException("formatter == null");
		
		this.formatter = formatter;
	}
	
	public LocalTimeToStringTransformer()
	{
		//this(ISODateTimeFormat.localDateOptionalTimeParser());
		this(new DateTimeFormatterBuilder().append(ISODateTimeFormat.time().getPrinter(), ISODateTimeFormat.localTimeParser().getParser()).toFormatter());
	}

	@Override
	protected String valueToString(LocalTime value) throws PreferencesException
	{
		if (value == null)
			return(null);
		
		return(value.toString(formatter));
	}

	@Override
	protected LocalTime stringToValue(String s) throws PreferencesException
	{
		if (s == null)
			return(null);
		
		try
		{
			return(LocalTime.parse(s, formatter));
		}
		catch (IllegalArgumentException e)
		{
			//Thrown when parsing fails
			throw new PreferencesException("Could not parse time '" + s + "'.", e);
		}
	}
}
