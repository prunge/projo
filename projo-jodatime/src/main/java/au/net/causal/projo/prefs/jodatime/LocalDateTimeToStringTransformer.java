package au.net.causal.projo.prefs.jodatime;

import org.joda.time.LocalDateTime;
import org.joda.time.format.DateTimeFormatter;
import org.joda.time.format.DateTimeFormatterBuilder;
import org.joda.time.format.ISODateTimeFormat;

import au.net.causal.projo.prefs.PreferencesException;
import au.net.causal.projo.prefs.transform.GenericToStringTransformer;

public class LocalDateTimeToStringTransformer extends GenericToStringTransformer<LocalDateTime>
{
	private final DateTimeFormatter formatter;
	
	public LocalDateTimeToStringTransformer(DateTimeFormatter formatter)
	{
		super(LocalDateTime.class);
		
		if (formatter == null)
			throw new NullPointerException("formatter == null");
		
		this.formatter = formatter;
	}
	
	public LocalDateTimeToStringTransformer()
	{
		//this(ISODateTimeFormat.localDateOptionalTimeParser());
		this(new DateTimeFormatterBuilder().append(ISODateTimeFormat.dateTime().getPrinter(), ISODateTimeFormat.localDateOptionalTimeParser().getParser()).toFormatter());
	}

	@Override
	protected String valueToString(LocalDateTime value) throws PreferencesException
	{
		if (value == null)
			return(null);
		
		return(value.toString(formatter));
	}

	@Override
	protected LocalDateTime stringToValue(String s) throws PreferencesException
	{
		if (s == null)
			return(null);
		
		try
		{
			return(LocalDateTime.parse(s, formatter));
		}
		catch (IllegalArgumentException e)
		{
			//Thrown when parsing fails
			throw new PreferencesException("Could not parse date '" + s + "'.", e);
		}
	}
}
