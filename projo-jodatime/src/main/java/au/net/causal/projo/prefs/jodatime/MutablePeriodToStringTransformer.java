package au.net.causal.projo.prefs.jodatime;

import org.joda.time.MutablePeriod;
import org.joda.time.format.ISOPeriodFormat;
import org.joda.time.format.PeriodFormatter;

import au.net.causal.projo.prefs.PreferencesException;
import au.net.causal.projo.prefs.transform.GenericToStringTransformer;

public class MutablePeriodToStringTransformer extends GenericToStringTransformer<MutablePeriod>
{
	private final PeriodFormatter formatter;
	
	public MutablePeriodToStringTransformer(PeriodFormatter formatter)
	{
		super(MutablePeriod.class);
		
		if (formatter == null)
			throw new NullPointerException("formatter == null");
		
		this.formatter = formatter;
	}
	
	public MutablePeriodToStringTransformer()
	{
		this(ISOPeriodFormat.standard());
	}

	@Override
	protected String valueToString(MutablePeriod value) throws PreferencesException
	{
		if (value == null)
			return(null);
		
		return(value.toString(formatter));
	}

	@Override
	protected MutablePeriod stringToValue(String s) throws PreferencesException
	{
		if (s == null)
			return(null);
		
		try
		{
			return(MutablePeriod.parse(s, formatter));
		}
		catch (IllegalArgumentException e)
		{
			//Thrown when parsing fails
			throw new PreferencesException("Could not parse period '" + s + "'.", e);
		}
	}
}
