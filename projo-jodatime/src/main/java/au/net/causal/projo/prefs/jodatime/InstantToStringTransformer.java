package au.net.causal.projo.prefs.jodatime;

import org.joda.time.Instant;
import org.joda.time.format.DateTimeFormatter;
import org.joda.time.format.ISODateTimeFormat;

import au.net.causal.projo.prefs.PreferencesException;
import au.net.causal.projo.prefs.transform.GenericToStringTransformer;

public class InstantToStringTransformer extends GenericToStringTransformer<Instant>
{
	private final DateTimeFormatter formatter;
	
	public InstantToStringTransformer(DateTimeFormatter formatter)
	{
		super(Instant.class);
		
		if (formatter == null)
			throw new NullPointerException("formatter == null");
		
		this.formatter = formatter;
	}
	
	public InstantToStringTransformer()
	{
		this(ISODateTimeFormat.dateTime());
	}

	@Override
	protected String valueToString(Instant value) throws PreferencesException
	{
		if (value == null)
			return(null);
		
		return(value.toString(formatter));
	}

	@Override
	protected Instant stringToValue(String s) throws PreferencesException
	{
		if (s == null)
			return(null);
		
		try
		{
			return(Instant.parse(s, formatter));
		}
		catch (IllegalArgumentException e)
		{
			//Thrown when parsing fails
			throw new PreferencesException("Could not parse date '" + s + "'.", e);
		}
	}
}
