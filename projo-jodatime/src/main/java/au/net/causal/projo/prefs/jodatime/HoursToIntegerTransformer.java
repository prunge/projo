package au.net.causal.projo.prefs.jodatime;

import org.joda.time.Hours;

import au.net.causal.projo.prefs.PreferencesException;
import au.net.causal.projo.prefs.transform.GenericToIntegerTransformer;

public class HoursToIntegerTransformer extends GenericToIntegerTransformer<Hours>
{
	public HoursToIntegerTransformer()
	{
		super(Hours.class);
	}

	@Override
	protected int valueToInteger(Hours value) throws PreferencesException
	{
		return(value.getHours());
	}

	@Override
	protected Hours integerToValue(int n) throws PreferencesException
	{
		return(Hours.hours(n));
	}

}
