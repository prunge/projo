package au.net.causal.projo.prefs.jodatime;

import org.joda.time.DateTimeZone;

import au.net.causal.projo.prefs.PreferencesException;
import au.net.causal.projo.prefs.transform.GenericToStringTransformer;

public class DateTimeZoneToStringTransformer extends GenericToStringTransformer<DateTimeZone>
{
	public DateTimeZoneToStringTransformer()
	{
		super(DateTimeZone.class);
	}
	
	@Override
	protected String valueToString(DateTimeZone value) throws PreferencesException
	{
		if (value == null)
			return(null);
		
		return(value.getID());
	}

	@Override
	protected DateTimeZone stringToValue(String s) throws PreferencesException
	{
		if (s == null)
			return(null);
		
		try
		{
			return(DateTimeZone.forID(s));
		}
		catch (IllegalArgumentException e)
		{
			//Thrown when parsing fails
			throw new PreferencesException("Invalid timezone ID '" + s + "'.", e);
		}
	}
}
