package au.net.causal.projo.prefs.jodatime;

import java.util.Arrays;

import org.joda.time.DateTimeFieldType;
import org.joda.time.MonthDay;
import org.joda.time.format.DateTimeFormatter;
import org.joda.time.format.ISODateTimeFormat;

import au.net.causal.projo.prefs.PreferencesException;
import au.net.causal.projo.prefs.transform.GenericToStringTransformer;

public class MonthDayToStringTransformer extends GenericToStringTransformer<MonthDay>
{
	private static final DateTimeFormatter DEFAULT_FORMAT = ISODateTimeFormat.forFields(Arrays.asList(DateTimeFieldType.monthOfYear(), DateTimeFieldType.dayOfMonth()), true, false);
	
	private final DateTimeFormatter formatter;
	
	public MonthDayToStringTransformer(DateTimeFormatter formatter)
	{
		super(MonthDay.class);
		
		if (formatter == null)
			throw new NullPointerException("formatter == null");
		
		this.formatter = formatter;
	}
	
	public MonthDayToStringTransformer()
	{
		this(DEFAULT_FORMAT);
	}

	@Override
	protected String valueToString(MonthDay value) throws PreferencesException
	{
		if (value == null)
			return(null);
		
		return(value.toString(formatter));
	}

	@Override
	protected MonthDay stringToValue(String s) throws PreferencesException
	{
		if (s == null)
			return(null);
		
		try
		{
			return(MonthDay.parse(s, formatter));
		}
		catch (IllegalArgumentException e)
		{
			//Thrown when parsing fails
			throw new PreferencesException("Could not parse date '" + s + "'.", e);
		}
	}
}
