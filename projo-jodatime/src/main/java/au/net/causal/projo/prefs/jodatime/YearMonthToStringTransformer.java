package au.net.causal.projo.prefs.jodatime;

import org.joda.time.YearMonth;
import org.joda.time.format.DateTimeFormatter;
import org.joda.time.format.DateTimeFormatterBuilder;
import org.joda.time.format.ISODateTimeFormat;

import au.net.causal.projo.prefs.PreferencesException;
import au.net.causal.projo.prefs.transform.GenericToStringTransformer;

public class YearMonthToStringTransformer extends GenericToStringTransformer<YearMonth>
{
	private static final DateTimeFormatter DEFAULT_FORMAT = new DateTimeFormatterBuilder().append(
																ISODateTimeFormat.yearMonth().getPrinter(),
																ISODateTimeFormat.localDateOptionalTimeParser().getParser()).toFormatter();
	
	private final DateTimeFormatter formatter;
	
	public YearMonthToStringTransformer(DateTimeFormatter formatter)
	{
		super(YearMonth.class);
		
		if (formatter == null)
			throw new NullPointerException("formatter == null");
		
		this.formatter = formatter;
	}
	
	public YearMonthToStringTransformer()
	{
		this(DEFAULT_FORMAT);
	}

	@Override
	protected String valueToString(YearMonth value) throws PreferencesException
	{
		if (value == null)
			return(null);
		
		return(value.toString(formatter));
	}

	@Override
	protected YearMonth stringToValue(String s) throws PreferencesException
	{
		if (s == null)
			return(null);
		
		try
		{
			return(YearMonth.parse(s, formatter));
		}
		catch (IllegalArgumentException e)
		{
			//Thrown when parsing fails
			throw new PreferencesException("Could not parse date '" + s + "'.", e);
		}
	}
}
