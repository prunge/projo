package au.net.causal.projo.prefs.jodatime;

import org.joda.time.Interval;

import au.net.causal.projo.prefs.PreferencesException;
import au.net.causal.projo.prefs.transform.GenericToStringTransformer;

public class IntervalToStringTransformer extends GenericToStringTransformer<Interval>
{
	public IntervalToStringTransformer()
	{
		super(Interval.class);
	}
	
	@Override
	protected String valueToString(Interval value) throws PreferencesException
	{
		if (value == null)
			return(null);
		
		return(value.toString());
	}

	@Override
	protected Interval stringToValue(String s) throws PreferencesException
	{
		if (s == null)
			return(null);
		
		try
		{
			return(Interval.parse(s));
		}
		catch (IllegalArgumentException e)
		{
			//Thrown when parsing fails
			throw new PreferencesException("Could not parse interval '" + s + "'.", e);
		}
	}
}
