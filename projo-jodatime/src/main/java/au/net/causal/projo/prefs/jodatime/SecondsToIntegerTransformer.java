package au.net.causal.projo.prefs.jodatime;

import org.joda.time.Seconds;

import au.net.causal.projo.prefs.PreferencesException;
import au.net.causal.projo.prefs.transform.GenericToIntegerTransformer;

public class SecondsToIntegerTransformer extends GenericToIntegerTransformer<Seconds>
{
	public SecondsToIntegerTransformer()
	{
		super(Seconds.class);
	}

	@Override
	protected int valueToInteger(Seconds value) throws PreferencesException
	{
		return(value.getSeconds());
	}

	@Override
	protected Seconds integerToValue(int n) throws PreferencesException
	{
		return(Seconds.seconds(n));
	}

}
