package au.net.causal.projo.prefs.jodatime;

import org.joda.time.Weeks;

import au.net.causal.projo.prefs.PreferencesException;
import au.net.causal.projo.prefs.transform.GenericToIntegerTransformer;

public class WeeksToIntegerTransformer extends GenericToIntegerTransformer<Weeks>
{
	public WeeksToIntegerTransformer()
	{
		super(Weeks.class);
	}

	@Override
	protected int valueToInteger(Weeks value) throws PreferencesException
	{
		return(value.getWeeks());
	}

	@Override
	protected Weeks integerToValue(int n) throws PreferencesException
	{
		return(Weeks.weeks(n));
	}

}
