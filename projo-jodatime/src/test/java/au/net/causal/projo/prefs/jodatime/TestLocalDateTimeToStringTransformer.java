package au.net.causal.projo.prefs.jodatime;

import static org.junit.Assert.*;

import org.joda.time.LocalDateTime;
import org.junit.Test;

import au.net.causal.projo.prefs.PreferenceKeyMetadata;
import au.net.causal.projo.prefs.PreferencesException;
import au.net.causal.projo.prefs.StandardTransformPhase;
import au.net.causal.projo.prefs.TransformerPreferenceNode;
import au.net.causal.projo.prefs.memory.InMemoryPreferenceNode;
import au.net.causal.projo.prefs.transform.IntToStringTransformer;
import au.net.causal.projo.prefs.transform.StringToByteTransformer;

public class TestLocalDateTimeToStringTransformer
{
	@Test
	public void testPut()
	throws PreferencesException
	{
		InMemoryPreferenceNode<String> node = new InMemoryPreferenceNode<>(String.class);
		TransformerPreferenceNode xNode = new TransformerPreferenceNode(node);
		xNode.addTransformer(new LocalDateTimeToStringTransformer(), StandardTransformPhase.DATATYPE);
		xNode.addTransformer(new IntToStringTransformer(), StandardTransformPhase.DATATYPE);
		xNode.addTransformer(new StringToByteTransformer(), StandardTransformPhase.DATATYPE);
		
		xNode.putValue("val", new LocalDateTime(1977, 1, 9, 14, 50), new PreferenceKeyMetadata<>(LocalDateTime.class));
		
		String data = node.getNativeValue("val");
		assertEquals("Wrong value.", "1977-01-09T14:50:00.000", data);
	}
	
	@Test
	public void testGet()
	throws PreferencesException
	{
		InMemoryPreferenceNode<String> node = new InMemoryPreferenceNode<>(String.class);
		node.putNativeValue("val", "1977-01-09T14:50:00.000");
		
		TransformerPreferenceNode xNode = new TransformerPreferenceNode(node);
		xNode.addTransformer(new LocalDateTimeToStringTransformer(), StandardTransformPhase.DATATYPE);
		xNode.addTransformer(new IntToStringTransformer(), StandardTransformPhase.DATATYPE);
		xNode.addTransformer(new StringToByteTransformer(), StandardTransformPhase.DATATYPE);
		
		LocalDateTime result = xNode.getValue("val", new PreferenceKeyMetadata<>(LocalDateTime.class));
		assertEquals("Wrong result.",  new LocalDateTime(1977, 1, 9, 14, 50), result);
	}
	
	@Test
	public void testGetNoMillis()
	throws PreferencesException
	{
		InMemoryPreferenceNode<String> node = new InMemoryPreferenceNode<>(String.class);
		node.putNativeValue("val", "1977-01-09T14:50:00");
		
		TransformerPreferenceNode xNode = new TransformerPreferenceNode(node);
		xNode.addTransformer(new LocalDateTimeToStringTransformer(), StandardTransformPhase.DATATYPE);
		xNode.addTransformer(new IntToStringTransformer(), StandardTransformPhase.DATATYPE);
		xNode.addTransformer(new StringToByteTransformer(), StandardTransformPhase.DATATYPE);
		
		LocalDateTime result = xNode.getValue("val", new PreferenceKeyMetadata<>(LocalDateTime.class));
		assertEquals("Wrong result.",  new LocalDateTime(1977, 1, 9, 14, 50), result);
	}
	
	@Test
	public void testGetNoTime()
	throws PreferencesException
	{
		InMemoryPreferenceNode<String> node = new InMemoryPreferenceNode<>(String.class);
		node.putNativeValue("val", "1977-01-09");
		
		TransformerPreferenceNode xNode = new TransformerPreferenceNode(node);
		xNode.addTransformer(new LocalDateTimeToStringTransformer(), StandardTransformPhase.DATATYPE);
		xNode.addTransformer(new IntToStringTransformer(), StandardTransformPhase.DATATYPE);
		xNode.addTransformer(new StringToByteTransformer(), StandardTransformPhase.DATATYPE);
		
		LocalDateTime result = xNode.getValue("val", new PreferenceKeyMetadata<>(LocalDateTime.class));
		assertEquals("Wrong result.",  new LocalDateTime(1977, 1, 9, 0, 0), result);
	}
	
	@Test
	public void testRemove()
	throws PreferencesException
	{
		InMemoryPreferenceNode<String> node = new InMemoryPreferenceNode<>(String.class);
		node.putNativeValue("val", "1977-01-09T14:50:00.000");
		
		TransformerPreferenceNode xNode = new TransformerPreferenceNode(node);
		xNode.addTransformer(new LocalDateTimeToStringTransformer(), StandardTransformPhase.DATATYPE);
		xNode.addTransformer(new IntToStringTransformer(), StandardTransformPhase.DATATYPE);
		xNode.addTransformer(new StringToByteTransformer(), StandardTransformPhase.DATATYPE);
		
		xNode.removeValue("val", new PreferenceKeyMetadata<>(LocalDateTime.class));
		
		String data = node.getNativeValue("val");
		assertNull("Value should not exist.", data);
	}
	
	@Test
	public void testDataTypeSupport()
	throws PreferencesException
	{
		InMemoryPreferenceNode<String> node = new InMemoryPreferenceNode<>(String.class);
		TransformerPreferenceNode xNode = new TransformerPreferenceNode(node);
		xNode.addTransformer(new LocalDateTimeToStringTransformer(), StandardTransformPhase.DATATYPE);
		xNode.addTransformer(new IntToStringTransformer(), StandardTransformPhase.DATATYPE);
		xNode.addTransformer(new StringToByteTransformer(), StandardTransformPhase.DATATYPE);
		
		boolean support = xNode.isDataTypeSupported(new PreferenceKeyMetadata<>(LocalDateTime.class));
		
		assertTrue("Should have support.", support);
	}
}
