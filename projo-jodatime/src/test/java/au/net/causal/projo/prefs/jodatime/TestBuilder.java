package au.net.causal.projo.prefs.jodatime;

import static org.junit.Assert.*;

import java.util.Properties;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import org.joda.time.DateTime;
import org.joda.time.DateTimeZone;
import org.joda.time.Seconds;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import au.net.causal.projo.annotation.Preference;
import au.net.causal.projo.annotation.PreferenceRoot;
import au.net.causal.projo.annotation.Projo;
import au.net.causal.projo.prefs.ProjoStore;
import au.net.causal.projo.prefs.ProjoStoreBuilder;
import au.net.causal.projo.prefs.properties.PropertiesPreferenceNode;

public class TestBuilder
{
	private static final Logger log = LoggerFactory.getLogger(TestBuilder.class);
		
	@Test
	public void testSave()
	throws Exception
	{
		Properties props = new Properties();
		PropertiesPreferenceNode node = new PropertiesPreferenceNode(props);
		
		ProjoStore store = ProjoStoreBuilder.builder().defaults()
							.node(node)
							.configureModule(new JodaTimeBuilderModule())
							.build();		
		
		MyConfig config = new MyConfig("galah", new DateTime(1977, 1, 9, 14, 50, DateTimeZone.forID("GMT")), Seconds.seconds(42));
		store.save(config);
		
		log.info(props.toString());
		
		String name = props.getProperty("config.name");
		String date = props.getProperty("config.date");
		String seconds = props.getProperty("config.seconds");
				
		assertEquals("Wrong name.", "galah", name);
		assertEquals("Wrong date.", "1977-01-09T14:50:00.000Z", date);
		assertEquals("Wrong seconds.", "42", seconds);
	}
	
	@Test
	public void testLoad()
	throws Exception
	{
		Properties props = new Properties();
		props.setProperty("config.name", "galah");
		props.setProperty("config.date", "1977-01-09T14:50:00.000Z");
		props.setProperty("config.seconds", "42");
		
		PropertiesPreferenceNode node = new PropertiesPreferenceNode(props);
		
		ProjoStore store = ProjoStoreBuilder.builder().defaults()
							.node(node)
							.configureModule(new JodaTimeBuilderModule())
							.build();		
		
		MyConfig config = store.read(MyConfig.class);
		log.info(ToStringBuilder.reflectionToString(config, ToStringStyle.SHORT_PREFIX_STYLE));
		
		assertEquals("Wrong name.", "galah", config.getName());
		assertEquals("Wrong date.", new DateTime(1977, 1, 9, 14, 50, DateTimeZone.forID("GMT")).toDate(), config.getDate().toDate());
		assertEquals("Wrong seconds.", Seconds.seconds(42), config.getSeconds());
	}
	
	@Projo
	@PreferenceRoot(name="config")
	public static class MyConfig
	{
		@Preference
		private String name;
		
		@Preference
		private DateTime date;
		
		@Preference
		private Seconds seconds;
		
		public MyConfig()
		{
			this(null, null, null);
		}
		
		public MyConfig(String name, DateTime date, Seconds seconds)
		{
			this.name = name;
			this.date = date;
			this.seconds = seconds;
		}

		public String getName()
		{
			return(name);
		}

		public void setName(String name)
		{
			this.name = name;
		}

		public DateTime getDate()
		{
			return(date);
		}

		public void setDate(DateTime date)
		{
			this.date = date;
		}

		public Seconds getSeconds()
		{
			return(seconds);
		}

		public void setSeconds(Seconds seconds)
		{
			this.seconds = seconds;
		}
	}

}
