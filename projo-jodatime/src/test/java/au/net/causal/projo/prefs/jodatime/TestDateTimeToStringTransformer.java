package au.net.causal.projo.prefs.jodatime;

import static org.junit.Assert.*;

import org.joda.time.DateTime;
import org.joda.time.DateTimeZone;
import org.junit.Test;

import au.net.causal.projo.prefs.PreferenceKeyMetadata;
import au.net.causal.projo.prefs.PreferencesException;
import au.net.causal.projo.prefs.StandardTransformPhase;
import au.net.causal.projo.prefs.TransformerPreferenceNode;
import au.net.causal.projo.prefs.memory.InMemoryPreferenceNode;
import au.net.causal.projo.prefs.transform.IntToStringTransformer;
import au.net.causal.projo.prefs.transform.StringToByteTransformer;

public class TestDateTimeToStringTransformer
{
	@Test
	public void testPut()
	throws PreferencesException
	{
		InMemoryPreferenceNode<String> node = new InMemoryPreferenceNode<>(String.class);
		TransformerPreferenceNode xNode = new TransformerPreferenceNode(node);
		xNode.addTransformer(new DateTimeToStringTransformer(), StandardTransformPhase.DATATYPE);
		xNode.addTransformer(new IntToStringTransformer(), StandardTransformPhase.DATATYPE);
		xNode.addTransformer(new StringToByteTransformer(), StandardTransformPhase.DATATYPE);
		
		xNode.putValue("val", new DateTime(1977, 1, 9, 14, 50, DateTimeZone.forID("GMT")), new PreferenceKeyMetadata<>(DateTime.class));
		
		String data = node.getNativeValue("val");
		assertEquals("Wrong value.", "1977-01-09T14:50:00.000Z", data);
	}
	
	@Test
	public void testGet()
	throws PreferencesException
	{
		InMemoryPreferenceNode<String> node = new InMemoryPreferenceNode<>(String.class);
		node.putNativeValue("val", "1977-01-09T14:50:00.000Z");
		
		TransformerPreferenceNode xNode = new TransformerPreferenceNode(node);
		xNode.addTransformer(new DateTimeToStringTransformer(), StandardTransformPhase.DATATYPE);
		xNode.addTransformer(new IntToStringTransformer(), StandardTransformPhase.DATATYPE);
		xNode.addTransformer(new StringToByteTransformer(), StandardTransformPhase.DATATYPE);
		
		DateTime result = xNode.getValue("val", new PreferenceKeyMetadata<>(DateTime.class));
		assertEquals("Wrong result.",  new DateTime(1977, 1, 9, 14, 50, DateTimeZone.forID("GMT")).toDate(), result.toDate());
	}
	
	@Test
	public void testRemove()
	throws PreferencesException
	{
		InMemoryPreferenceNode<String> node = new InMemoryPreferenceNode<>(String.class);
		node.putNativeValue("val", "1977-01-09T14:50:00.000Z");
		
		TransformerPreferenceNode xNode = new TransformerPreferenceNode(node);
		xNode.addTransformer(new DateTimeToStringTransformer(), StandardTransformPhase.DATATYPE);
		xNode.addTransformer(new IntToStringTransformer(), StandardTransformPhase.DATATYPE);
		xNode.addTransformer(new StringToByteTransformer(), StandardTransformPhase.DATATYPE);
		
		xNode.removeValue("val", new PreferenceKeyMetadata<>(DateTime.class));
		
		String data = node.getNativeValue("val");
		assertNull("Value should not exist.", data);
	}
	
	@Test
	public void testDataTypeSupport()
	throws PreferencesException
	{
		InMemoryPreferenceNode<String> node = new InMemoryPreferenceNode<>(String.class);
		TransformerPreferenceNode xNode = new TransformerPreferenceNode(node);
		xNode.addTransformer(new DateTimeToStringTransformer(), StandardTransformPhase.DATATYPE);
		xNode.addTransformer(new IntToStringTransformer(), StandardTransformPhase.DATATYPE);
		xNode.addTransformer(new StringToByteTransformer(), StandardTransformPhase.DATATYPE);
		
		boolean support = xNode.isDataTypeSupported(new PreferenceKeyMetadata<>(DateTime.class));
		
		assertTrue("Should have support.", support);
	}
}
