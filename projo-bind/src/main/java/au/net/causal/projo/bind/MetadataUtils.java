package au.net.causal.projo.bind;

import java.lang.annotation.Annotation;
import java.lang.reflect.Array;
import java.lang.reflect.GenericArrayType;
import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.lang.reflect.TypeVariable;
import java.lang.reflect.WildcardType;
import java.util.Collection;
import java.util.Map;

import org.apache.commons.lang3.reflect.TypeUtils;

import com.google.common.base.Defaults;
import com.google.common.collect.ClassToInstanceMap;
import com.google.common.collect.ImmutableClassToInstanceMap;
import com.google.common.collect.MutableClassToInstanceMap;

/**
 * Utility methods for metadata.
 * 
 * @author prunge
 */
public final class MetadataUtils
{
	/**
	 * Private constructor to prevent instantiation.
	 *
	 */
	private MetadataUtils()
	{
	}
	
	/**
	 * Creates a map mapping annotation classes to instances.  The keys in the map will be the annotation classes
	 * themselves (not the implementation classes).  The returned map is immutable.
	 * 
	 * @param annotations the annotations to use.
	 * 
	 * @return a map mapping annotation classes to their implementations.
	 * 
	 * @throws NullPointerException if <code>annotations</code> is null.
	 */
	public static ClassToInstanceMap<Annotation> createConfigurationMap(Collection<? extends Annotation> annotations)
	{
		if (annotations == null)
			throw new NullPointerException("annotations == null");
		
		ClassToInstanceMap<Annotation> map = MutableClassToInstanceMap.create();
		
		for (Annotation annotation : annotations)
		{
			map.put(annotation.annotationType(), annotation);
		}
		
		return(ImmutableClassToInstanceMap.copyOf(map));
	}
	
	/**
	 * Determines the raw type of a generic type.
	 * 
	 * @param type the generic type.
	 * 
	 * @return the raw type.
	 * 
	 * @throws NullPointerException if <code>type</code> is null.
	 */
	public static Class<?> getRawType(Type type)
	{
		if (type == null)
			throw new NullPointerException("type == null");
		
		if (type instanceof Class<?>)
			return((Class<?>)type);
		else if (type instanceof ParameterizedType)
			return((Class<?>)((ParameterizedType)type).getRawType());
		else if (type instanceof WildcardType)
		{
			Type[] upperBounds = ((WildcardType)type).getUpperBounds();
			if (upperBounds == null || upperBounds.length == 0)
				return(Object.class);
			else
				return(getRawType(upperBounds[0]));
		}
		else if (type instanceof TypeVariable<?>)
		{
			Type[] bounds = ((TypeVariable<?>)type).getBounds();
			if (bounds == null || bounds.length == 0)
				return(Object.class);
			else
				return(getRawType(bounds[0]));
		}
		else if (type instanceof GenericArrayType)
		{
			Class<?> rawComponentType = getRawType(((GenericArrayType)type).getGenericComponentType());
			return(Array.newInstance(rawComponentType, 0).getClass());
		}
		else
			return(Object.class);
	}
	
	/**
	 * Drills into a collection or array type and pulls out the first type parameter or array component type.
	 * <p>
	 * 
	 * For collections, the first type parameter of <code>type</code> is returned, or if it is a raw type, 
	 * <code>Object.class</code> is returned.
	 * <p>
	 * 
	 * For arrays, the array component type is returned.
	 * <p>
	 * 
	 * For wildcard types, the first upper bound is returned if it exists. 
	 * 
	 * @param type the type to process.
	 * 
	 * @return the component type.
	 */
	public static Type getCollectionOrArrayComponentGenericType(Type type)
	{
		//Raw types (including arrays
		if (type instanceof Class<?>)
		{
			Class<?> classType = (Class<?>)type;
			if (classType.isArray())
				return(classType.getComponentType());
			else
				return(Object.class); //Raw collection?  Can only guess Object
		}
		else if (type instanceof ParameterizedType)
		{
			ParameterizedType pType = (ParameterizedType)type;
			Type componentType = pType.getActualTypeArguments()[0];
			return(componentType);
		}
		else if (type instanceof TypeVariable<?>)
		{
			TypeVariable<?> typeVar = (TypeVariable<?>)type;
			Type[] bounds = typeVar.getBounds();
			if (bounds == null || bounds.length == 0)
				return(Object.class); //No bounds, assume upper bound is Object
			else
				return(bounds[0]);
		}
		else if (type instanceof WildcardType)
		{
			WildcardType wildcard = (WildcardType)type;
			Type[] bounds = wildcard.getUpperBounds();
			if (bounds == null || bounds.length == 0)
				return(Object.class); //No upper bounds, assume Object
			else
				return(bounds[0]);
		}
		else if (type instanceof GenericArrayType)
			return(((GenericArrayType)type).getGenericComponentType());
		else
			return(Object.class);
	}
	
	/**
	 * Pulls out the key type of a map.  This works for both explicit declarations of maps and subclasses that
	 * specify either the key or value in their declaration.
	 * 
	 * @param type the map type, e.g. <code>Map&lt;String, String&gt;</code>.
	 * 
	 * @return the generic type of the map key, or <code>Object.class</code> if <code>type</code> is not a map
	 * 			or it is a raw type.
	 * 
	 * @see #getMapValueGenericType(Type)
	 */
	public static Type getMapKeyGenericType(Type type)
	{
		Map<TypeVariable<?>, Type> mapTypeArguments = TypeUtils.getTypeArguments(type, Map.class);
		for (Map.Entry<TypeVariable<?>, Type> entry : mapTypeArguments.entrySet())
		{
			if ("K".equals(entry.getKey().getName()))
				return(entry.getValue());
		}
		
		//Not a map, or no K
		return(Object.class);
	}
	
	/**
	 * Pulls out the value type of a map.  This works for both explicit declarations of maps and subclasses that
	 * specify either the key or value in their declaration.
	 * 
	 * @param type the map type, e.g. <code>Map&lt;String, String&gt;</code>.
	 * 
	 * @return the generic type of the map value, or <code>Object.class</code> if <code>type</code> is not a map
	 * 			or it is a raw type.
	 * 
	 * @see #getMapKeyGenericType(Type)
	 */
	public static Type getMapValueGenericType(Type type)
	{
		Map<TypeVariable<?>, Type> mapTypeArguments = TypeUtils.getTypeArguments(type, Map.class);
		for (Map.Entry<TypeVariable<?>, Type> entry : mapTypeArguments.entrySet())
		{
			if ("V".equals(entry.getKey().getName()))
				return(entry.getValue());
		}
		
		//Not a map, or no K
		return(Object.class);
	}
	
	/**
	 * Drills into a collection or array type and pulls out the first type parameter or array component type. returning
	 * its raw type.  This works similarly to {@link #getCollectionOrArrayComponentGenericType(Type)} except a raw
	 * type is returned instead of a generic type.
	 * 
	 * @param type the type to process.
	 * 
	 * @return the component type as a raw type.
	 * 
	 * @see #getCollectionOrArrayComponentGenericType(Type)
	 */
	public static Class<?> getCollectionOrArrayComponentRawType(Type type)
	{
		return(getRawType(getCollectionOrArrayComponentGenericType(type)));
	}
	
	/**
	 * Returns the default value for a primitive type as defined in the Java Language Specification.
	 * <p>
	 * 
	 * The default values for primitive types are zero for number types, <code>false</code> for boolean types and 
	 * '\0' for char types.
	 * 
	 * @param primitiveType the primitive type.
	 * 
	 * @return the default value for the primitive type, or <code>null</code> if <code>primitiveType</code>
	 * 			is not actually a primitive type.
	 */
	public static Object defaultValueForPrimitive(Class<?> primitiveType)
	{
		return(Defaults.defaultValue(primitiveType));
	}
}
