package au.net.causal.projo.bind;

public interface MetadataReader<T extends EntityMetadata<P>, P extends PropertyMetadata>
{
	/**
	 * Reads metadata from an entity type.  Returns <code>null</code> if <code>entityType</code> is not an entity.
	 * 
	 * @param entityType the entity type.
	 * 
	 * @return the read entity metadata.
	 * 
	 * @throws NullPointerException if <code>entityType</code> is null.
	 * @throws EntityMetadataException if there is a configuration problem with the entity's metadata.
	 */
	public T read(Class<?> entityType)
	throws EntityMetadataException;
}
