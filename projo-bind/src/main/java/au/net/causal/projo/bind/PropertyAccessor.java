package au.net.causal.projo.bind;

import java.lang.reflect.InvocationTargetException;

/**
 * Used to access a bean property.
 * 
 * @author prunge
 */
public interface PropertyAccessor
{
	/**
	 * Returns the property value.
	 * 
	 * @param fromObject the object the property exists on.
	 * 
	 * @return the property value.
	 * 
	 * @throws InvocationTargetException if an error occurs calling the getter.
	 * @throws IllegalAccessException if there is an accessibility error calling the getter.
	 * @throws WriteOnlyPropertyException if the property is write-only.
	 */
	public Object get(Object fromObject)
	throws InvocationTargetException, IllegalAccessException, WriteOnlyPropertyException;
	
	/**
	 * Sets the property value.
	 * 
	 * @param fromObject the object the property exists on.
	 * @param value the value to set.
	 * 
	 * @throws InvocationTargetException if an error occurs calling the setter.
	 * @throws IllegalAccessException if there is an accessibility error calling the setter.
	 * @throws ReadOnlyPropertyException if the property is read-only.
	 */
	public void set(Object fromObject, Object value)
	throws InvocationTargetException, IllegalAccessException, ReadOnlyPropertyException;

	/**
	 * Returns the name of the property being accessed.
	 * 
	 * @return the property name.
	 */
	public String getPropertyName();
	
	/**
	 * Returns the type of values.
	 *  
	 * @return the required value type.
	 */
	public Class<?> getRequiredValueType();
	
	/**
	 * Returns whether the property can be read.
	 * 
	 * @return true if the property can be read, false if not.
	 */
	public boolean isReadable();
	
	/**
	 * Returns whether the property can be written.
	 * 
	 * @return true if the property can be written, false if not.
	 */
	public boolean isWritable();
}
