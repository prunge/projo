package au.net.causal.projo.bind;

/**
 * Occurs when there is a problem with an entity's metadata.
 * 
 * @author prunge
 */
public class EntityMetadataException extends Exception
{
	/**
	 * Creates a <code>EntityMetadataException</code>.
	 */
	public EntityMetadataException()
	{
	}

	/**
	 * Creates a <code>EntityMetadataException</code> with a detail message.
	 * 
	 * @param message the detail message.
	 */
	public EntityMetadataException(String message)
	{
		super(message);
	}

	/**
	 * Creates a <code>EntityMetadataException</code> with a cause exception.
	 * 
	 * @param cause the cause exception.
	 */
	public EntityMetadataException(Throwable cause)
	{
		super(cause);
	}

	/**
	 * Creates a <code>EntityMetadataException</code> with a detail message and cause exception.
	 * 
	 * @param message the detail message.
	 * @param cause the cause exception.
	 */
	public EntityMetadataException(String message, Throwable cause)
	{
		super(message, cause);
	}
}
