package au.net.causal.projo.bind;

/**
 * Factory for creating property accessors that read and write object properties.
 * 
 * @author prunge
 */
public interface PropertyAccessorFactory
{
	/**
	 * Creates a property accessor for a given property.
	 * 
	 * @param type the class the property exists on.
	 * @param propertyName the name of the property.
	 * 
	 * @return an accessor that can be used to get and/or set the property, or null if the property does not exist.
	 * 
	 * @throws NullPointerException if <code>type</code> or <code>propertyName</code> is null.
	 */
	public abstract PropertyAccessor createAccessor(Class<?> type, String propertyName);

}