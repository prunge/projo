package au.net.causal.projo.bind;

import java.lang.annotation.Annotation;
import java.util.Collection;
import java.util.Set;

import com.google.common.collect.ClassToInstanceMap;
import com.google.common.collect.ImmutableClassToInstanceMap;

/**
 * Common metadata superclass that has metadata in the form of annotations.
 * 
 * @author prunge
 */
public abstract class AnnotatedMetadata
{
	private final ClassToInstanceMap<Annotation> annotationMap;
	
	/**
	 * Creates an <code>AnnotatedMetadata</code> object.
	 * 
	 * @param annotations the annotations to use.
	 * 
	 * @throws NullPointerException if <code>annotations</code> is null.
	 */
	protected AnnotatedMetadata(Iterable<? extends Annotation> annotations)
	{
		if (annotations == null)
			throw new NullPointerException("annotations == null");
		
		ImmutableClassToInstanceMap.Builder<Annotation> b = ImmutableClassToInstanceMap.<Annotation>builder();
		for (Annotation annotation : annotations)
		{
			builderPut(b, annotation.annotationType(), annotation);
		}
		
		this.annotationMap = b.build();
	}
	
	private static <A extends Annotation> void builderPut(ImmutableClassToInstanceMap.Builder<Annotation> builder, Class<A> annotationType, Annotation annotation)
	{
		builder.put(annotationType, annotationType.cast(annotation));
	}

	/**
	 * Returns the annotations present on the element.  
	 * <p>
	 * 
	 * The returned list is unmodifiable.
	 * 
	 * @return a list of annotations.
	 */
	public Collection<? extends Annotation> getAnnotations()
	{
		return(annotationMap.values());
	}
	
	/**
	 * Returns true if an annotation of the specified type is present.
	 * 
	 * @param annotationType the annotation type.
	 * 
	 * @return true if an annotation of this type exists in the metadata, false if not.
	 * 
	 * @throws NullPointerException if <code>annotationType</code> is null.
	 */
	public boolean isAnnotationPresent(Class<? extends Annotation> annotationType)
	{
		return(annotationMap.containsKey(annotationType));
	}
	
	/**
	 * Returns this element's annotation for the specified type if such an annotation is present, else null.
	 * 
	 * @param annotationType the annotation type.
	 * 
	 * @return the annotation of the specified type present on the element, or null if not found.
	 * 
	 * @throws NullPointerException if <code>annotationType</code> is null.
	 */
	public <A extends Annotation> A getAnnotation(Class<A> annotationType)
	{
		return(annotationMap.getInstance(annotationType));
	}
	
	/**
	 * Returns the set of annotation types present on the element.
	 * 
	 * @return a set of annotation types.  The returned set is unmodifiable.
	 */
	public Set<? extends Class<? extends Annotation>> getAnnotationTypes()
	{
		return(annotationMap.keySet());
	}
}
