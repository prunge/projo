package au.net.causal.projo.bind;

/**
 * Exception that occurs when an attempt is made to read a write-only of a bean.
 * 
 * @author prunge
 */
public class WriteOnlyPropertyException extends InaccessiblePropertyException
{
	/**
	 * Creates a <code>WriteOnlyPropertyException</code> with a default detail message.
	 * 
	 * @param declaringClass the class the property exists on.
	 * @param propertyName the name of the property.
	 */
	public WriteOnlyPropertyException(Class<?> declaringClass, String propertyName)
	{
		super(declaringClass, propertyName);
	}

	/**
	 * Creates a <code>WriteOnlyPropertyException</code> with the specified detail message.
	 * 
	 * @param message the detail message.
	 * @param declaringClass the class the property exists on.
	 * @param propertyName the name of the property.
	 */
	public WriteOnlyPropertyException(String message, Class<?> declaringClass, String propertyName)
	{
		super(message, declaringClass, propertyName);
	}
}
