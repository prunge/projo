package au.net.causal.projo.bind;

/**
 * Exception that occurs when an attempt is made to write to a read-only property of a bean.
 * 
 * @author prunge
 */
public class ReadOnlyPropertyException extends InaccessiblePropertyException
{
	/**
	 * Creates a <code>ReadOnlyPropertyException</code> with a default detail message.
	 * 
	 * @param declaringClass the class the property exists on.
	 * @param propertyName the name of the property.
	 */
	public ReadOnlyPropertyException(Class<?> declaringClass, String propertyName)
	{
		super(declaringClass, propertyName);
	}

	/**
	 * Creates a <code>ReadOnlyPropertyException</code> with the specified detail message.
	 * 
	 * @param message the detail message.
	 * @param declaringClass the class the property exists on.
	 * @param propertyName the name of the property.
	 */
	public ReadOnlyPropertyException(String message, Class<?> declaringClass, String propertyName)
	{
		super(message, declaringClass, propertyName);
	}
}
