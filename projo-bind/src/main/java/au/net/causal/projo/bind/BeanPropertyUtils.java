package au.net.causal.projo.bind;

import java.beans.Introspector;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.util.Set;

import com.google.common.collect.ImmutableSet;

/**
 * Utility methods for dealing with bean properties and reflection.
 * 
 * @author prunge
 */
public final class BeanPropertyUtils
{
	/**
	 * Prefixes for getter methods.
	 */
	private static final Set<String> GETTER_PREFIXES = ImmutableSet.<String>builder().add("get", "is").build(); 

	/**
	 * Prefixes for setter methods.
	 */
	private static final Set<String> SETTER_PREFIXES = ImmutableSet.<String>builder().add("set").build(); 
	
	/**
	 * Private constructor to prevent instantiation.
	 */
	private BeanPropertyUtils()
	{
	}

	/**
	 * Returns the name of the property if a method is a getter method for a property.
	 * 
	 * @param possibleGetterMethod the possible getter method to process.
	 * 
	 * @return the bean property name <code>possibleGetterMethod</code> sets if it is a getter, or <code>null</code>
	 * 			if the method is not a getter.
	 */
	public static String getterMethodToPropertyName(Method possibleGetterMethod)
	{
		//Getters must be public and non-static, non-abstract, be non-void and have no arguments
		
		//Must be void
		if (possibleGetterMethod.getReturnType() == Void.TYPE)
			return(null);
		
		int modifiers = possibleGetterMethod.getModifiers();
		if (Modifier.isAbstract(modifiers))
			return(null);
		if (Modifier.isStatic(modifiers))
			return(null);
		if (!Modifier.isPublic(modifiers))
			return(null);
		
		//Must have no args
		if (possibleGetterMethod.getParameterTypes().length != 0)
			return(null);
		
		//Must have appropriate name
		String methodName = possibleGetterMethod.getName();
		
		for (String getterPrefix : GETTER_PREFIXES)
		{
			if (methodName.startsWith(getterPrefix))
			{
				String remainingName = methodName.substring(getterPrefix.length());
				if (remainingName.length() == 0)
					return(null);
			
				String propertyName = Introspector.decapitalize(remainingName);
				return(propertyName);
			}
		}
		
		//Does not match getter prefix
		return(null);
	}
	
	/**
	 * Returns the name of the property if a method is a setter method for a property.
	 * 
	 * @param possibleSetterMethod the possible setter method to process.
	 * 
	 * @return the bean property name <code>possibleSetterMethod</code> sets if it is a setter, or <code>null</code>
	 * 			if the method is not a setter.
	 */
	public static String setterMethodToPropertyName(Method possibleSetterMethod)
	{
		//Setters must be public and non-static, non-abstract, be void and have one argument
		
		//Must be void
		if (possibleSetterMethod.getReturnType() != Void.TYPE)
			return(null);
		
		int modifiers = possibleSetterMethod.getModifiers();
		if (Modifier.isAbstract(modifiers))
			return(null);
		if (Modifier.isStatic(modifiers))
			return(null);
		if (!Modifier.isPublic(modifiers))
			return(null);
		
		//Must have one arg
		if (possibleSetterMethod.getParameterTypes().length != 1)
			return(null);
		
		//Must have appropriate name
		String methodName = possibleSetterMethod.getName();
		for (String setterPrefix : SETTER_PREFIXES)
		{
			if (methodName.startsWith(setterPrefix))
			{
				String remainingName = methodName.substring(setterPrefix.length());
				if (remainingName.length() == 0)
					return(null);
				
				String propertyName = Introspector.decapitalize(remainingName);
				return(propertyName);
			}
		}
		
		//Not a setter
		return(null);
		
	}
}
