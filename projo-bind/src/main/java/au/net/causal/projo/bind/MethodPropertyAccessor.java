package au.net.causal.projo.bind;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

/**
 * Property accessor implementation that uses getter and setter methods.
 * 
 * @author prunge
 */
public class MethodPropertyAccessor implements PropertyAccessor
{
	private final String propertyName;
	private final Method getter;
	private final Method setter;

	/**
	 * Creates a <code>MethodPropertyAccessor</code>.
	 * 
	 * @param propertyName the name of the property.
	 * @param getter the getter method.
	 * @param setter the setter method.
	 * 
	 * @throws NullPointerException if <code>propertyName</code> is null.
	 * @throws IllegalArgumentException if both the <code>getter</code> and <code>setter</code> is null.
	 */
	public MethodPropertyAccessor(String propertyName, Method getter, Method setter)
	{
		if (propertyName == null)
			throw new NullPointerException("propertyName == null");
		if (getter == null && setter == null)
			throw new IllegalArgumentException("getter and setter cannot both be null.");
		
		this.propertyName = propertyName;
		this.getter = getter;
		this.setter = setter;
	}
	
	@Override
	public Object get(Object fromObject)
	throws InvocationTargetException, IllegalAccessException, WriteOnlyPropertyException
	{
		if (getter == null)
			throw new WriteOnlyPropertyException(setter.getDeclaringClass(), propertyName);
		
		return(getter.invoke(fromObject));
	}

	@Override
	public void set(Object fromObject, Object value)
	throws InvocationTargetException, IllegalAccessException, ReadOnlyPropertyException
	{
		if (setter == null)
			throw new ReadOnlyPropertyException(getter.getDeclaringClass(), propertyName);
		
		//Special case for not setting null values on primitive properties
		if (setter.getParameterTypes()[0].isPrimitive() && value == null)
			value = MetadataUtils.defaultValueForPrimitive(setter.getParameterTypes()[0]);
		
		setter.invoke(fromObject, value);
	}

	@Override
	public String getPropertyName()
	{
		return(propertyName);
	}

	@Override
	public Class<?> getRequiredValueType()
	{
		if (setter != null)
			return(setter.getParameterTypes()[0]);
		else
			return(getter.getReturnType());
	}

	@Override
	public boolean isReadable()
	{
		return(getter != null);
	}

	@Override
	public boolean isWritable()
	{
		return(setter != null);
	}
}
