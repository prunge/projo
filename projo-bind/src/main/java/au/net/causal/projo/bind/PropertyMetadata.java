package au.net.causal.projo.bind;

import java.lang.annotation.Annotation;
import java.lang.reflect.Type;

public abstract class PropertyMetadata extends AnnotatedMetadata
{
	private final String name;
	private final Type propertyType;
	private final EntityMetadata<?> parent;
	private final PropertyAccessor accessor;
	
	protected PropertyMetadata(String name, Type propertyType, EntityMetadata<?> parent, PropertyAccessor accessor, Iterable<? extends Annotation> annotations)
	{
		super(annotations);
		
		if (name == null)
			throw new NullPointerException("name == null");
		if (propertyType == null)
			throw new NullPointerException("propertyType == null");
		if (parent == null)
			throw new NullPointerException("parent == null");
		if (accessor == null)
			throw new NullPointerException("accessor == null");
		
		this.name = name;
		this.propertyType = propertyType;
		this.parent = parent;
		this.accessor = accessor;
	}

	public String getName()
	{
		return(name);
	}

	public Type getPropertyType()
	{
		return(propertyType);
	}

	public EntityMetadata<?> getParent()
	{
		return(parent);
	}

	public PropertyAccessor getAccessor()
	{
		return(accessor);
	}
}
