package au.net.causal.projo.bind;

import java.lang.annotation.Annotation;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public abstract class EntityMetadata<P extends PropertyMetadata> extends AnnotatedMetadata
{
	private final Class<?> entityType;
	
	private final List<P> properties = new ArrayList<>();
	private final Map<String, P> propertyMap = new HashMap<>();
	
	protected EntityMetadata(Class<?> entityType, Iterable<? extends Annotation> annotations)
	{
		super(annotations);
		
		if (entityType == null)
			throw new NullPointerException("entityType == null");
		this.entityType = entityType;
	}
	
	public List<? extends P> getProperties()
	{
		return(Collections.unmodifiableList(properties));
	}
	
	public void addProperty(P property)
	{
		if (property == null)
			throw new NullPointerException("property == null");
		
		properties.add(property);
		propertyMap.put(property.getName(), property);
	}
	
	public void removeProperty(P property)
	{
		if (property == null)
			throw new NullPointerException("property == null");
		
		properties.remove(property);
		propertyMap.remove(property.getName());
	}
	
	public P getProperty(String name)
	{
		if (name == null)
			throw new NullPointerException("name == null");
		
		return(propertyMap.get(name));
	}

	public Class<?> getEntityType()
	{
		return(entityType);
	}

}
