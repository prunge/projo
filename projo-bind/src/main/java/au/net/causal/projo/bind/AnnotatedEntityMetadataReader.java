package au.net.causal.projo.bind;

import java.lang.annotation.Annotation;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.lang.reflect.Type;
import java.util.Arrays;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import com.google.common.collect.ImmutableList;

public abstract class AnnotatedEntityMetadataReader<T extends EntityMetadata<P>, P extends PropertyMetadata> 
implements MetadataReader<T, P>
{	
	private final PropertyAccessorFactory propertyAccessorFactory;
	
	/**
	 * Holds entity metadata as we are building it, so that we don't go into an infinite loop from nested read calls.  The map maps
	 * the entity type to its partially built metadata object.
	 */
	private final ThreadLocal<Map<Class<?>, T>> builtMetadata = new ThreadLocal<>();
	
	protected abstract T createEntityMetadata(Class<?> entityType, Iterable<? extends Annotation> annotations)
	throws EntityMetadataException;
	
	/**
	 * Creates a property from the given metadata.
	 * 
	 * @param prototype prototype metadata information for the property.
	 * 
	 * @return the created property metadata, or null if no property should be created.
	 * 
	 * @throws EntityMetadataException if there is a problem with the property metadata.
	 */
	protected abstract P createPropertyMetadata(PropertyPrototype<T, P> prototype)
	throws EntityMetadataException;
	
	protected AnnotatedEntityMetadataReader()
	{
		this(new StandardPropertyAccessorFactory());
	}
	
	protected AnnotatedEntityMetadataReader(PropertyAccessorFactory propertyAccessorFactory)
	{
		if (propertyAccessorFactory == null)
			throw new NullPointerException("propertyAccessorFactory == null");
		
		this.propertyAccessorFactory = propertyAccessorFactory;
	}
	
	/**
	 * Verifies the configuration of entity metadata after it has been completely constructed.
	 * <p>
	 * 
	 * Subclasses may override this method to add additional validation logic.
	 * 
	 * @param entityMetadata the entity metadata to verify.
	 * 
	 * @throws NullPointerException if <code>entityMetadata</code> is null.
	 * @throws EntityMetadataException if there is a problem with the entity metadata.
	 */
	protected void verifyEntityMetadata(T entityMetadata)
	throws EntityMetadataException
	{
		if (entityMetadata == null)
			throw new NullPointerException("entityMetadata == null");
		
		//No logic required, subclasses will add
	}
	
	@Override
	public T read(Class<?> entityType) 
	throws EntityMetadataException
	{
		if (entityType == null)
			throw new NullPointerException("entityType == null");
	
		Map<Class<?>, T> builtMetadataMap = builtMetadata.get();
		boolean inNestedCall;
		if (builtMetadataMap == null)
		{
			builtMetadataMap = new HashMap<>();
			builtMetadata.set(builtMetadataMap);
			inNestedCall = false;
		}
		else
			inNestedCall = true;
		
		try
		{
			return(readInternal(entityType, builtMetadataMap));
		}
		finally
		{
			if (!inNestedCall)
				builtMetadata.remove();
		}
	}
	
	private T readInternal(Class<?> entityType, Map<Class<?>, T> builtMetadata)
	throws EntityMetadataException
	{
		//If the entity has already been (partially) built, don't build it again to prevent infinite recursion
		T existingMeta = builtMetadata.get(entityType);
		if (existingMeta != null)
			return(existingMeta);
		
		//Create the base entity metadata and put it in the map so it is not built again in this round
		List<Annotation> entityAnnotations = ImmutableList.<Annotation>builder().add(entityType.getAnnotations()).build();
		T metadata = createEntityMetadata(entityType, entityAnnotations);
		builtMetadata.put(entityType, metadata);
		
		if (metadata == null)
			return(null);
		
		//Build property list
		Map<String, P> propertyConfigurations = new LinkedHashMap<>();
		
		//Process fields and methods
		Class<?> curEntityType = entityType;
		while (curEntityType != null && !Object.class.equals(curEntityType))
		{
			processMethods(metadata, propertyConfigurations, curEntityType, builtMetadata);
			processFields(metadata, propertyConfigurations, curEntityType, builtMetadata);
			curEntityType = curEntityType.getSuperclass();
		}
		
		for (P property : propertyConfigurations.values())
		{
			metadata.addProperty(property);
		}
		
		verifyEntityMetadata(metadata);
		
		return(metadata);
	}
	
	/**
	 * Processes getter and setter methods on an entity, scanning for properties.
	 * 
	 * @param entity the entity type.  Will be annotated appropriately.
	 * @param propertyConfigurations a map mapping property names to property metadata configurations.  Will be 
	 * 			added to by this method.
	 * @param entityType the raw entity type being processed.
	 * @param builtMetadata a map of entity metadata already built for other entity types.  Used to prevent 
	 * 			infinite recursion when building entity properties.
	 * 
	 * @throws EntityMetadataException if an error occurs reading metadata.
	 */
	private void processMethods(T entity, Map<String, P> propertyConfigurations, Class<?> entityType, Map<Class<?>, T> builtMetadata)
	throws EntityMetadataException
	{
		for (Method method : entityType.getDeclaredMethods())
		{
			if (Modifier.isPublic(method.getModifiers()))
			{
				String propertyName = BeanPropertyUtils.setterMethodToPropertyName(method);
				if (propertyName != null) //setter
				{
					List<Annotation> annotations = Arrays.asList(method.getAnnotations());
					Type propertyType = method.getGenericParameterTypes()[0];
					PropertyAccessor accessor = propertyAccessorFactory.createAccessor(entityType, propertyName);
					if (accessor != null)
					{
						PropertyPrototype<T, P> prototype = new PropertyPrototype<>(propertyType, propertyName, entity, accessor, annotations);
						P config = createPropertyMetadata(prototype);
						if (config != null)
						{
							P oldConfig = propertyConfigurations.put(propertyName, config);
							if (oldConfig != null)
								throw new EntityMetadataException("Duplicate property definition for " + entityType.getCanonicalName() + "." + propertyName);
						}
					}
				}
				else //getter
				{
					propertyName = BeanPropertyUtils.getterMethodToPropertyName(method);
					if (propertyName != null)
					{
						List<Annotation> annotations = Arrays.asList(method.getAnnotations());
						Type propertyType = method.getGenericReturnType();
						PropertyAccessor accessor = propertyAccessorFactory.createAccessor(entityType, propertyName);
						if (accessor != null)
						{
							PropertyPrototype<T, P> prototype = new PropertyPrototype<>(propertyType, propertyName, entity, accessor, annotations);
							P config = createPropertyMetadata(prototype);
							if (config != null)
							{
								PropertyMetadata oldConfig = propertyConfigurations.put(propertyName, config);
								if (oldConfig != null)
									throw new EntityMetadataException("Duplicate property definition for " + entityType.getCanonicalName() + "." + propertyName);
							}
						}
					}
				}
			}
		}
	}
	
	/**
	 * Processes fields on an entity, scanning for properties.  Private fields are also scanned for metadata, but 
	 * such fields are only considered as properties if they have publicly accessible getter and/or setter methods.
	 * 
	 * @param entity the entity type.  Will be annotated appropriately.
	 * @param propertyConfigurations a map mapping property names to property metadata configurations.  Will be 
	 * 			added to by this method.
	 * @param entityType the raw entity type being processed.
	 * @param builtMetadata a map of entity metadata already built for other entity types.  Used to prevent 
	 * 			infinite recursion when building entity properties.
	 * 
	 * @throws EntityMetadataException if an error occurs reading metadata.
	 */
	private void processFields(T entity, Map<String, P> propertyConfigurations, Class<?> entityType, Map<Class<?>, T> builtMetadata)
	throws EntityMetadataException
	{
		for (Field field : entityType.getDeclaredFields())
		{
			String propertyName = field.getName();
			List<Annotation> annotations = Arrays.asList(field.getAnnotations());
			Type propertyType = field.getGenericType();
			PropertyAccessor accessor = propertyAccessorFactory.createAccessor(entityType, propertyName);
			if (accessor != null)
			{
				PropertyPrototype<T, P> prototype = new PropertyPrototype<>(propertyType, propertyName, entity, accessor, annotations);
				P config = createPropertyMetadata(prototype);
				if (config != null)
				{
					P oldConfig = propertyConfigurations.put(propertyName, config);
					if (oldConfig != null)
						throw new EntityMetadataException("Duplicate property definition for " + entityType.getCanonicalName() + "." + propertyName);
				}
			}
		}
	}
	
	public static class PropertyPrototype<T extends EntityMetadata<P>, P extends PropertyMetadata> extends PropertyMetadata
	{
		protected PropertyPrototype(Type type, String name, T parent, PropertyAccessor accessor, Iterable<? extends Annotation> annotations)
		{
			super(name, type, parent, accessor, annotations);
		}
	}
}
