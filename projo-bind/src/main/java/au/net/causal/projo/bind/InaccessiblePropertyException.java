package au.net.causal.projo.bind;

/**
 * Exception that is thrown when a property cannot be accessed.  This might be because a property is missing a public
 * getter or a setter method.
 * 
 * @author RUNPET
 */
public class InaccessiblePropertyException extends EntityMetadataException
{
	private final Class<?> declaringClass;
	private final String propertyName;
	
	/**
	 * Creates a <code>InaccessiblePropertyException</code> with a default detail message.
	 * 
	 * @param declaringClass the class the property exists on.
	 * @param propertyName the name of the property.
	 */
	public InaccessiblePropertyException(Class<?> declaringClass, String propertyName)
	{
		this(declaringClass.getCanonicalName() + "." + propertyName, declaringClass, propertyName);
	}
	
	/**
	 * Creates a <code>InaccessiblePropertyException</code> with the specified detail message.
	 * 
	 * @param message the detail message.
	 * @param declaringClass the class the property exists on.
	 * @param propertyName the name of the property.
	 */
	public InaccessiblePropertyException(String message, Class<?> declaringClass, String propertyName)
	{
		super(message);
		
		this.declaringClass = declaringClass;
		this.propertyName = propertyName;
	}

	/**
	 * Returns the class the property exists on.
	 * 
	 * @return the class the property exists on.
	 */
	public Class<?> getDeclaringClass()
	{
		return(declaringClass);
	}

	/**
	 * Returns the name of the property.
	 * 
	 * @return the name of the property.
	 */
	public String getPropertyName()
	{
		return(propertyName);
	}
}
