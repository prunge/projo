package au.net.causal.projo.bind;

import java.lang.reflect.Method;

/**
 * Factory for creating property accessors using standard public field and method access.
 * 
 * @author prunge
 */
public class StandardPropertyAccessorFactory implements PropertyAccessorFactory
{
	/**
	 * Creates a property accessor for a given property.
	 * 
	 * @param type the class the property exists on.
	 * @param propertyName the name of the property.
	 * 
	 * @return an accessor that can be used to get and/or set the property, or null if the property does not exist.
	 * 
	 * @throws NullPointerException if <code>type</code> or <code>propertyName</code> is null.
	 */
	@Override
	public PropertyAccessor createAccessor(Class<?> type, String propertyName)
	{
		if (type == null)
			throw new NullPointerException("type == null");
		if (propertyName == null)
			throw new NullPointerException("propertyName == null");
		
		//Find getter and setter
		Method getterMethod = null;
		Method setterMethod = null;
		for (Method method : type.getMethods())
		{
			String getterMethodPropertyName = BeanPropertyUtils.getterMethodToPropertyName(method);
			if (getterMethodPropertyName != null && propertyName.equals(getterMethodPropertyName))
				getterMethod = method;

			String setterMethodPropertyName = BeanPropertyUtils.setterMethodToPropertyName(method);
			if (setterMethodPropertyName != null && propertyName.equals(setterMethodPropertyName))
				setterMethod = method;
		}
		
		if (getterMethod != null || setterMethod != null)
			return(new MethodPropertyAccessor(propertyName, getterMethod, setterMethod));
		
		//TODO field acccessors
		
		//If we get here we could not access the property
		return(null);
		
	}
}
