package au.net.causal.projo.bind;

import static org.junit.Assert.*;

import java.lang.annotation.Annotation;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.lang.reflect.WildcardType;
import java.net.URI;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import java.util.Map;

import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.common.collect.ClassToInstanceMap;

/**
 * Unit tests for the {@link MetadataUtils} class.
 * 
 * @author prunge
 */
public class TestMetadataUtils
{
	private static final Logger log = LoggerFactory.getLogger(TestMetadataUtils.class);
	
	/**
	 * Tests the {@link MetadataUtils#createConfigurationMap(java.util.Collection)} method.
	 */
	@Test
	public void testCreateConfigurationMap()
	{
		List<Annotation> annotations = Arrays.asList(AnnotationHolder.class.getAnnotations());
		
		ClassToInstanceMap<Annotation> map = MetadataUtils.createConfigurationMap(annotations);
		assertNotNull("Null map.", map);
		
		log.debug("Map: " + map);
		assertEquals("Wrong size.", 2, map.size());
		
		Entity entityAnnotation = map.getInstance(Entity.class);
		assertNotNull("No entity annotation.", entityAnnotation);
		DummyAnnotation dummy = map.getInstance(DummyAnnotation.class);
		assertNotNull("No dummy annotation.", dummy);
	}
	
	/**
	 * Tests getting the raw type of an already-raw type.
	 */
	@Test
	public void testGetRawTypeClass()
	{
		Class<?> result = MetadataUtils.getRawType(String.class);
		assertEquals("Wrong result.", String.class, result);
	}
	
	/**
	 * Tests getting the raw type of a parameterized type.
	 */
	@Test
	public void testGetRawTypeParameterized()
	{
		Class<?> result = MetadataUtils.getRawType(Parameterized.class.getGenericInterfaces()[0]);
		assertEquals("Wrong result.", List.class, result);
	}
	
	/**
	 * Tests getting the raw type of a type variable.
	 * 
	 * @throws Exception if an error occurs.
	 */
	@Test
	public void testGetRawTypeTypeVariable()
	throws Exception
	{
		Class<?> result = MetadataUtils.getRawType(MyType.class.getMethod("doSomething").getGenericReturnType());
		assertEquals("Wrong result.", Map.class, result);
	}
	
	/**
	 * Tests getting the raw type of a wildcard type.
	 * 
	 * @throws Exception if an error occurs.
	 */
	@Test
	public void testGetRawTypeWildcard()
	throws Exception
	{
		Class<?> result = MetadataUtils.getRawType(WildOne.class.getMethod("doSomething").getGenericReturnType());
		assertEquals("Wrong result.", Collection.class, result);
	}
	
	/**
	 * Tests getting the raw type of a generic array.
	 * 
	 * @throws Exception if an error occurs.
	 */
	@Test
	public void testGetRawTypeGenericArray()
	throws Exception
	{
		Class<?> result = MetadataUtils.getRawType(GenericArrayOne.class.getMethod("doSomething").getGenericReturnType());
		assertEquals("Wrong result.", Comparable[].class, result);
	}
	
	/**
	 * Tests getting the component type with a wildcard type.
	 * 
	 * @throws Exception if an error occurs.
	 */
	@Test
	public void testGetCollectionOrArrayComponentGenericTypeWildcard()
	throws Exception
	{
		Type componentType = MetadataUtils.getCollectionOrArrayComponentGenericType(WildOne.class.getMethod("doSomething").getGenericReturnType());
		assertTrue("Not wildcard type.", componentType instanceof WildcardType);
		WildcardType wildType = (WildcardType)componentType;
		assertArrayEquals("Wrong result.", new Type[] {Number.class}, wildType.getUpperBounds());
	}
	
	/**
	 * Tests getting the component type with a parameterized type.
	 * 
	 * @throws Exception if an error occurs.
	 */
	@Test
	public void testGetCollectionOrArrayComponentGenericTypeParameterized()
	throws Exception
	{
		Type componentType = MetadataUtils.getCollectionOrArrayComponentGenericType(Parameterized.class.getGenericInterfaces()[0]);
		assertEquals("Wrong result.", String.class, componentType);
	}
	
	/**
	 * Tests getting the component type with a generic array type.
	 * 
	 * @throws Exception if an error occurs.
	 */
	@Test
	public void testGetCollectionOrArrayComponentGenericTypeGenericArray()
	throws Exception
	{
		Type componentType = MetadataUtils.getCollectionOrArrayComponentGenericType(GenericArrayOne.class.getMethod("doSomething").getGenericReturnType());
		assertTrue("Wrong type.", componentType instanceof ParameterizedType);
		ParameterizedType pType = (ParameterizedType)componentType;
		assertEquals("Wrong result.", Comparable.class, pType.getRawType());
	}
	
	/**
	 * Tests getting the component type with a normal array type.
	 * 
	 * @throws Exception if an error occurs.
	 */
	@Test
	public void testGetCollectionOrArrayComponentGenericTypeNormalArray()
	throws Exception
	{
		Type componentType = MetadataUtils.getCollectionOrArrayComponentGenericType(URI[].class);
		assertEquals("Wrong result.", URI.class, componentType);
	}
	
	/**
	 * Tests getting the component type with a wildcard type.
	 * 
	 * @throws Exception if an error occurs.
	 */
	@Test
	public void testGetCollectionOrArrayComponentRawTypeWildcard()
	throws Exception
	{
		Class<?> componentType = MetadataUtils.getCollectionOrArrayComponentRawType(WildOne.class.getMethod("doSomething").getGenericReturnType());
		assertEquals("Wrong result.", Number.class, componentType);
	}
	
	/**
	 * Tests getting the component type with a parameterized type.
	 * 
	 * @throws Exception if an error occurs.
	 */
	@Test
	public void testGetCollectionOrArrayComponentRawTypeParameterized()
	throws Exception
	{
		Class<?> componentType = MetadataUtils.getCollectionOrArrayComponentRawType(Parameterized.class.getGenericInterfaces()[0]);
		assertEquals("Wrong result.", String.class, componentType);
	}
	
	/**
	 * Tests getting the component type with a generic array type.
	 * 
	 * @throws Exception if an error occurs.
	 */
	@Test
	public void testGetCollectionOrArrayComponentRawTypeGenericArray()
	throws Exception
	{
		Class<?> componentType = MetadataUtils.getCollectionOrArrayComponentRawType(GenericArrayOne.class.getMethod("doSomething").getGenericReturnType());
		assertEquals("Wrong result.", Comparable.class, componentType);
	}
	
	/**
	 * Tests getting the component type with a normal array type.
	 * 
	 * @throws Exception if an error occurs.
	 */
	@Test
	public void testGetCollectionOrArrayComponentRawTypeNormalArray()
	throws Exception
	{
		Class<?> componentType = MetadataUtils.getCollectionOrArrayComponentRawType(URI[].class);
		assertEquals("Wrong result.", URI.class, componentType);
	}
	
	/**
	 * Tests getting the key type of a declared map.
	 * 
	 * @throws Exception if an error occurs.
	 */
	@Test
	public void testGetMapKeyGenericType()
	throws Exception
	{
		Type type = MapHolder1.class.getMethod("doSomething").getGenericReturnType();
		Type keyType = MetadataUtils.getMapKeyGenericType(type);
		assertEquals("Wrong key type.", String.class, keyType);
	}
	
	/**
	 * Tests getting the value type of a declared map.
	 * 
	 * @throws Exception if an error occurs.
	 */
	@Test
	public void testGetMapValueGenericType()
	throws Exception
	{
		Type type = MapHolder1.class.getMethod("doSomething").getGenericReturnType();
		Type valueType = MetadataUtils.getMapValueGenericType(type);
		assertEquals("Wrong value type.", Number.class, valueType);
	}
	
	/**
	 * Tests {@link MetadataUtils#defaultValueForPrimitive(Class)}.
	 */
	@Test
	public void testDefaultValueForPrimitive()
	{
		assertEquals("Wrong value.", Integer.valueOf(0), MetadataUtils.defaultValueForPrimitive(int.class));
	}

	/**
	 * Tests {@link MetadataUtils#defaultValueForPrimitive(Class)} for a non-primitive type.
	 */
	@Test
	public void testDefaultValueForPrimitiveNonPrimitive()
	{
		assertNull("Wrong value.", MetadataUtils.defaultValueForPrimitive(String.class));
	}
	
	@Retention(RetentionPolicy.RUNTIME)
	private static @interface Entity
	{
		
	}

	/**
	 * Used just so we can get instances of annotations.
	 * 
	 * @author RUNPET
	 */
	@Entity
	@DummyAnnotation
	private static class AnnotationHolder
	{
		
	}
	
	@Retention(RetentionPolicy.RUNTIME)
	private static @interface DummyAnnotation
	{
		
	}
	
	private static abstract class Parameterized implements List<String>
	{
		
	}
	
	private static abstract class MyType<T extends Map<Number, String>>
	{
		public abstract T doSomething();
	}
	
	private static abstract class WildOne
	{
		public abstract Collection<? extends Number> doSomething();
	}
	
	private static abstract class GenericArrayOne
	{
		public abstract Comparable<?>[] doSomething();
	}
	
	private static abstract class MapHolder1
	{
		public abstract Map<String, Number> doSomething();
	}
}
