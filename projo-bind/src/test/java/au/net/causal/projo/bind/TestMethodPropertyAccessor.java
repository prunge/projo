package au.net.causal.projo.bind;

import static org.junit.Assert.*;

import java.lang.reflect.Method;

import org.junit.Test;

/**
 * Unit tests for the {@link MethodPropertyAccessor} class.
 * 
 * @author prunge
 */
public class TestMethodPropertyAccessor
{
	@Test
	public void testNormalGetter()
	throws Exception
	{
		Person p = new Person("John Galah", 20);
		
		Method getter = Person.class.getMethod("getName");
		Method setter = Person.class.getMethod("setName", String.class);
		MethodPropertyAccessor accessor = new MethodPropertyAccessor("name", getter, setter);
		
		Object value = accessor.get(p);
		assertEquals("Wrong value.", "John Galah", value);
	}
	
	@Test
	public void testNormalSetter()
	throws Exception
	{
		Person p = new Person("John Galah", 20);
		
		Method getter = Person.class.getMethod("getName");
		Method setter = Person.class.getMethod("setName", String.class);
		MethodPropertyAccessor accessor = new MethodPropertyAccessor("name", getter, setter);
		
		accessor.set(p, "Jane Cockatoo");
		assertEquals("Wrong value.", "Jane Cockatoo", p.getName());
	}
	
	@Test
	public void testPrimitiveGetter()
	throws Exception
	{
		Person p = new Person("John Galah", 20);
		
		Method getter = Person.class.getMethod("getAge");
		Method setter = Person.class.getMethod("setAge", int.class);
		MethodPropertyAccessor accessor = new MethodPropertyAccessor("age", getter, setter);
		
		Object value = accessor.get(p);
		assertEquals("Wrong value.", 20, value);
	}
	
	@Test
	public void testPrimitiveSetter()
	throws Exception
	{
		Person p = new Person("John Galah", 20);
		
		Method getter = Person.class.getMethod("getAge");
		Method setter = Person.class.getMethod("setAge", int.class);
		MethodPropertyAccessor accessor = new MethodPropertyAccessor("age", getter, setter);
		
		accessor.set(p, 30);
		assertEquals("Wrong value.", 30, p.getAge());
	}
	
	/**
	 * When setting a null value to a primitive property, the default value for that primitive type should be used.
	 * e.g. int: 0
	 * 
	 * @throws Exception if an error occurs.
	 */
	@Test
	public void testPrimitiveSetterWithNull()
	throws Exception
	{
		Person p = new Person("John Galah", 20);
		
		Method getter = Person.class.getMethod("getAge");
		Method setter = Person.class.getMethod("setAge", int.class);
		MethodPropertyAccessor accessor = new MethodPropertyAccessor("age", getter, setter);
		
		accessor.set(p, null);
		assertEquals("Wrong value.", 0, p.getAge());
	}
	
	@Test
	public void testReadOnlyProperty()
	throws Exception
	{
		Method getter = Person.class.getMethod("getName");
		Method setter = null;
		MethodPropertyAccessor accessor = new MethodPropertyAccessor("name", getter, setter);
		
		assertTrue("Should be read-only.", accessor.isReadable());
		assertFalse("Should be read-only.", accessor.isWritable());
	}
	
	@Test
	public void testWriteOnlyProperty()
	throws Exception
	{
		Method getter = null;
		Method setter = Person.class.getMethod("setName", String.class);
		MethodPropertyAccessor accessor = new MethodPropertyAccessor("name", getter, setter);
		
		assertFalse("Should be write-only.", accessor.isReadable());
		assertTrue("Should be write-only.", accessor.isWritable());
	}
	
	@Test
	public void testReadWriteProperty()
	throws Exception
	{
		Method getter = Person.class.getMethod("getName");
		Method setter = Person.class.getMethod("setName", String.class);
		MethodPropertyAccessor accessor = new MethodPropertyAccessor("name", getter, setter);
		
		assertTrue("Should be read-write.", accessor.isReadable());
		assertTrue("Should be read-write.", accessor.isWritable());
	}
	
	public static class Person
	{
		private String name;
		private int age;
		
		public Person(String name, int age)
		{
			this.name = name;
			this.age = age;
		}
		
		public String getName()
		{
			return(name);
		}
		
		public void setName(String name)
		{
			this.name = name;
		}
		
		public int getAge()
		{
			return(age);
		}
		
		public void setAge(int age)
		{
			this.age = age;
		}
	}

}
