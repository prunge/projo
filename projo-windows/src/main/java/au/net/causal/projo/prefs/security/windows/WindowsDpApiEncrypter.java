package au.net.causal.projo.prefs.security.windows;

import org.jasypt.encryption.ByteEncryptor;
import org.jasypt.exceptions.EncryptionOperationNotPossibleException;

import com.sun.jna.platform.win32.Crypt32Util;
import com.sun.jna.platform.win32.Win32Exception;

/**
 * Uses the Windows API to encrypt data using the local user's account's credentials.
 * 
 * @author prunge
 */
public class WindowsDpApiEncrypter implements ByteEncryptor
{
	@Override
	public byte[] encrypt(byte[] binary)
	{
		if (binary == null)
			throw new NullPointerException("binary == null");
		
		try
		{
			byte[] encryptedBinary = Crypt32Util.cryptProtectData(binary);
			return(encryptedBinary);
		}
		catch (Win32Exception e)
		{
			throw new EncryptionOperationNotPossibleException(e);
		}
	}

	@Override
	public byte[] decrypt(byte[] encryptedBinary)
	{
		if (encryptedBinary == null)
			throw new NullPointerException("encryptedBinary == null");
		
		try
		{
			byte[] binary = Crypt32Util.cryptUnprotectData(encryptedBinary);
			return(binary);
		}
		catch (Win32Exception e)
		{
			throw new EncryptionOperationNotPossibleException(e);
		}
	}
}
