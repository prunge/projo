package au.net.causal.projo.prefs.security.windows;

import java.nio.charset.StandardCharsets;
import java.util.Arrays;

import org.junit.Ignore;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class TestWindowsEncryption
{
	private static final Logger log = LoggerFactory.getLogger(TestWindowsEncryption.class);

	@Test
	@Ignore
	public void test()
	{
		WindowsDpApiEncrypter encrypter = new WindowsDpApiEncrypter();
		byte[] data = "Good morning".getBytes(StandardCharsets.UTF_8);
		byte[] encrypted = encrypter.encrypt(data);
		log.info("Encrypted: " + Arrays.toString(encrypted));
		
		byte[] decrypted = encrypter.decrypt(encrypted);
		String s = new String(decrypted, StandardCharsets.UTF_8);
		
		log.info("String: " + s);
	}

}
