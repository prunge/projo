package au.net.causal.projo.prefs.windows;

import static org.junit.Assert.*;

import java.nio.charset.StandardCharsets;
import java.util.Collections;
import java.util.List;
import java.util.Random;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import au.net.causal.projo.prefs.PreferenceKeyMetadata;
import au.net.causal.projo.prefs.PreferenceNode;
import au.net.causal.projo.prefs.PreferencesException;

import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableSet;
import com.google.common.reflect.TypeToken;
import com.sun.jna.platform.win32.Advapi32Util;
import com.sun.jna.platform.win32.WinReg;
import com.sun.jna.platform.win32.WinReg.HKEY;

public class TestWindowsRegistry
{
	private static final Logger log = LoggerFactory.getLogger(TestWindowsRegistry.class); 
	
	private static final HKEY TEST_ROOT = WinReg.HKEY_CURRENT_USER;
	private static final String TEST_KEY_PARENT = "Software\\JavaSoft\\prefs";
	private static final String TEST_KEY_PARENT_SUFFIX = "projotest";
	private static final String TEST_KEY_PREFIX = TEST_KEY_PARENT + RegistryUtils.NODE_SEPARATOR + TEST_KEY_PARENT_SUFFIX;
	
	private String testKeySuffix;
	private String testKey;
	
	@Before
	public void preTestGenerate()
	throws PreferencesException
	{
		Random rand = new Random();
		testKeySuffix = String.valueOf(Math.abs(rand.nextInt()));
		testKey = TEST_KEY_PREFIX + RegistryUtils.NODE_SEPARATOR + testKeySuffix;
		RegistryUtils.createKeyPath(TEST_ROOT, testKey);
		
		log.info("Registry key for this test: " + testKey);
	}
	
	@After
	public void postTestClean()
	{
		if (testKey != null)
		{
			cleanRegistryArea();
			testKeySuffix = null;
			testKey = null;
		}
	}
	
	/**
	 * Clean up the registry area before and after the tests.
	 */
	private void cleanRegistryArea()
	{
		log.info("Cleaning up registry key: " + testKey);
		RegistryUtils.deleteKeyWithChildren(TEST_ROOT, testKey);
	}
	
	/**
	 * Tests retrieving a string value.
	 * 
	 * @throws PreferencesException if an error occurs.
	 */
	@Test
	public void testGetString()
	throws PreferencesException
	{
		Advapi32Util.registrySetStringValue(TEST_ROOT, testKey, "greeting", "Good morning");
		
		WindowsRegistryNode node = new WindowsRegistryNode(TEST_ROOT, testKey);
		String value = node.getValue("greeting", new PreferenceKeyMetadata<>(String.class));
		
		assertEquals("Wrong value.", "Good morning", value);
	}
	
	/**
	 * Tests retrieving a string value that does not exist.
	 * 
	 * @throws PreferencesException if an error occurs.
	 */
	@Test
	public void testGetStringMissing()
	throws PreferencesException
	{
		WindowsRegistryNode node = new WindowsRegistryNode(TEST_ROOT, testKey);
		String value = node.getValue("greeting", new PreferenceKeyMetadata<>(String.class));
		
		assertNull("Should have no value.", value);
	}
	
	/**
	 * Tests retrieving an integer value.
	 * 
	 * @throws PreferencesException if an error occurs.
	 */
	@Test
	public void testGetInt()
	throws PreferencesException
	{
		Advapi32Util.registrySetIntValue(TEST_ROOT, testKey, "number", 75);
		
		WindowsRegistryNode node = new WindowsRegistryNode(TEST_ROOT, testKey);
		Integer value = node.getValue("number", new PreferenceKeyMetadata<>(Integer.class));
		
		assertEquals("Wrong value.", Integer.valueOf(75), value);
	}
	
	/**
	 * Tests retrieving an integer value that does not exist.
	 * 
	 * @throws PreferencesException if an error occurs.
	 */
	@Test
	public void testGetIntMissing()
	throws PreferencesException
	{
		WindowsRegistryNode node = new WindowsRegistryNode(TEST_ROOT, testKey);
		Integer value = node.getValue("number", new PreferenceKeyMetadata<>(Integer.class));
		
		assertNull("Should have no value.", value);
	}
	
	/**
	 * Tests retrieving a long value.
	 * 
	 * @throws PreferencesException if an error occurs.
	 */
	@Test
	public void testGetLong()
	throws PreferencesException
	{
		Advapi32Util.registrySetLongValue(TEST_ROOT, testKey, "number", 75L);
		
		WindowsRegistryNode node = new WindowsRegistryNode(TEST_ROOT, testKey);
		Long value = node.getValue("number", new PreferenceKeyMetadata<>(Long.class));
		
		assertEquals("Wrong value.", Long.valueOf(75L), value);
	}
	
	/**
	 * Tests retrieving a long value that does not exist.
	 * 
	 * @throws PreferencesException if an error occurs.
	 */
	@Test
	public void testGetLongMissing()
	throws PreferencesException
	{
		WindowsRegistryNode node = new WindowsRegistryNode(TEST_ROOT, testKey);
		Long value = node.getValue("number", new PreferenceKeyMetadata<>(Long.class));
		
		assertNull("Should have no value.", value);
	}
	
	/**
	 * Tests retrieving a byte array value.
	 * 
	 * @throws PreferencesException if an error occurs.
	 */
	@Test
	public void testGetByteArray()
	throws PreferencesException
	{
		Advapi32Util.registrySetBinaryValue(TEST_ROOT, testKey, "data", new byte[] {1, 2, 3, 5, 7, 11});
		
		WindowsRegistryNode node = new WindowsRegistryNode(TEST_ROOT, testKey);
		byte[] value = node.getValue("data", new PreferenceKeyMetadata<>(byte[].class));
		
		assertArrayEquals("Wrong value.",new byte[] {1, 2, 3, 5, 7, 11}, value);
	}
	
	/**
	 * Tests retrieving a byte array value that does not exist.
	 * 
	 * @throws PreferencesException if an error occurs.
	 */
	@Test
	public void testGetByteArrayMissing()
	throws PreferencesException
	{
		WindowsRegistryNode node = new WindowsRegistryNode(TEST_ROOT, testKey);
		byte[] value = node.getValue("data", new PreferenceKeyMetadata<>(byte[].class));
		
		assertNull("Should have no value.", value);
	}
	
	/**
	 * Tests retrieving a string list value.
	 * 
	 * @throws PreferencesException if an error occurs.
	 */
	@Test
	public void testGetStringList()
	throws PreferencesException
	{
		Advapi32Util.registrySetStringArray(TEST_ROOT, testKey, "items", new String[] {"one", "two", "three"});
		
		WindowsRegistryNode node = new WindowsRegistryNode(TEST_ROOT, testKey);
		List<String> value = node.getValue("items", new PreferenceKeyMetadata<>(new TypeToken<List<String>>(){}));
		
		assertEquals("Wrong value.", ImmutableList.of("one", "two", "three"), value);
	}
	
	/**
	 * Tests retrieving a long value that does not exist.
	 * 
	 * @throws PreferencesException if an error occurs.
	 */
	@Test
	public void testGetStringListMissing()
	throws PreferencesException
	{
		WindowsRegistryNode node = new WindowsRegistryNode(TEST_ROOT, testKey);
		List<String> value = node.getValue("items", new PreferenceKeyMetadata<>(new TypeToken<List<String>>(){}));
		
		assertNull("Should have no value.", value);
	}
	
	/**
	 * Tests data conversion: int to string.
	 * 
	 * @throws PreferencesException if an error occurs.
	 */
	@Test
	public void testGetDataConversionIntToString()
	throws PreferencesException
	{
		Advapi32Util.registrySetIntValue(TEST_ROOT, testKey, "number", 75);
		
		WindowsRegistryNode node = new WindowsRegistryNode(TEST_ROOT, testKey);
		String value = node.getValue("number", new PreferenceKeyMetadata<>(String.class));
		
		assertEquals("Wrong value.", "75", value);
	}
	
	/**
	 * Tests data conversion: int to long.
	 * 
	 * @throws PreferencesException if an error occurs.
	 */
	@Test
	public void testGetDataConversionIntToLong()
	throws PreferencesException
	{
		Advapi32Util.registrySetIntValue(TEST_ROOT, testKey, "number", 75);
		
		WindowsRegistryNode node = new WindowsRegistryNode(TEST_ROOT, testKey);
		Long value = node.getValue("number", new PreferenceKeyMetadata<>(Long.class));
		
		assertEquals("Wrong value.", Long.valueOf(75L), value);
	}
	
	/**
	 * Tests data conversion: int to byte[].
	 * 
	 * @throws PreferencesException if an error occurs.
	 */
	@Test
	public void testGetDataConversionIntToByteArray()
	throws PreferencesException
	{
		Advapi32Util.registrySetIntValue(TEST_ROOT, testKey, "number", 75);
		
		WindowsRegistryNode node = new WindowsRegistryNode(TEST_ROOT, testKey);
		byte[] value = node.getValue("number", new PreferenceKeyMetadata<>(byte[].class));
		
		assertArrayEquals("Wrong value.", new byte[] {75, 0, 0, 0}, value);
	}
	
	/**
	 * Tests data conversion: int to string list.
	 * 
	 * @throws PreferencesException if an error occurs.
	 */
	@Test
	public void testGetDataConversionIntToStringList()
	throws PreferencesException
	{
		Advapi32Util.registrySetIntValue(TEST_ROOT, testKey, "number", 75);
		
		WindowsRegistryNode node = new WindowsRegistryNode(TEST_ROOT, testKey);
		List<String> value = node.getValue("number", new PreferenceKeyMetadata<>(new TypeToken<List<String>>() {}));
		
		assertEquals("Wrong value.", ImmutableList.of("75"), value);
	}
	
	/**
	 * Tests data conversion: long to string.
	 * 
	 * @throws PreferencesException if an error occurs.
	 */
	@Test
	public void testGetDataConversionLongToString()
	throws PreferencesException
	{
		Advapi32Util.registrySetLongValue(TEST_ROOT, testKey, "number", 75L);
		
		WindowsRegistryNode node = new WindowsRegistryNode(TEST_ROOT, testKey);
		String value = node.getValue("number", new PreferenceKeyMetadata<>(String.class));
		
		assertEquals("Wrong value.", "75", value);
	}
	
	/**
	 * Tests data conversion: long to int.
	 * 
	 * @throws PreferencesException if an error occurs.
	 */
	@Test
	public void testGetDataConversionLongToInt()
	throws PreferencesException
	{
		Advapi32Util.registrySetLongValue(TEST_ROOT, testKey, "number", 75L);
		
		WindowsRegistryNode node = new WindowsRegistryNode(TEST_ROOT, testKey);
		Integer value = node.getValue("number", new PreferenceKeyMetadata<>(Integer.class));
		
		assertEquals("Wrong value.", Integer.valueOf(75), value);
	}
	
	/**
	 * Tests data conversion: long to byte[].
	 * 
	 * @throws PreferencesException if an error occurs.
	 */
	@Test
	public void testGetDataConversionLongToByteArray()
	throws PreferencesException
	{
		Advapi32Util.registrySetLongValue(TEST_ROOT, testKey, "number", 75L);
		
		WindowsRegistryNode node = new WindowsRegistryNode(TEST_ROOT, testKey);
		byte[] value = node.getValue("number", new PreferenceKeyMetadata<>(byte[].class));
		
		assertArrayEquals("Wrong value.", new byte[] {75, 0, 0, 0, 0, 0, 0, 0}, value);
	}
	
	/**
	 * Tests data conversion: int to string list.
	 * 
	 * @throws PreferencesException if an error occurs.
	 */
	@Test
	public void testGetDataConversionLongToStringList()
	throws PreferencesException
	{
		Advapi32Util.registrySetLongValue(TEST_ROOT, testKey, "number", 75L);
		
		WindowsRegistryNode node = new WindowsRegistryNode(TEST_ROOT, testKey);
		List<String> value = node.getValue("number", new PreferenceKeyMetadata<>(new TypeToken<List<String>>() {}));
		
		assertEquals("Wrong value.", ImmutableList.of("75"), value);
	}
	
	/**
	 * Tests data conversion: string to int.
	 * 
	 * @throws PreferencesException if an error occurs.
	 */
	@Test
	public void testGetDataConversionStringToInt()
	throws PreferencesException
	{
		Advapi32Util.registrySetStringValue(TEST_ROOT, testKey, "number", "75");
		
		WindowsRegistryNode node = new WindowsRegistryNode(TEST_ROOT, testKey);
		Integer value = node.getValue("number", new PreferenceKeyMetadata<>(Integer.class));
		
		assertEquals("Wrong value.", Integer.valueOf(75), value);
	}
	
	/**
	 * Tests data conversion: string to int with unparseable number.
	 * 
	 * @throws PreferencesException if an error occurs.
	 */
	@Test
	public void testGetDataConversionStringToIntUnparseable()
	throws PreferencesException
	{
		Advapi32Util.registrySetStringValue(TEST_ROOT, testKey, "number", "galah");
		
		WindowsRegistryNode node = new WindowsRegistryNode(TEST_ROOT, testKey);
		Integer value = node.getValue("number", new PreferenceKeyMetadata<>(Integer.class));
		
		assertNull("Wrong value.", value);
	}
	
	/**
	 * Tests data conversion: string to long.
	 * 
	 * @throws PreferencesException if an error occurs.
	 */
	@Test
	public void testGetDataConversionStringToLong()
	throws PreferencesException
	{
		Advapi32Util.registrySetStringValue(TEST_ROOT, testKey, "number", "75");
		
		WindowsRegistryNode node = new WindowsRegistryNode(TEST_ROOT, testKey);
		Long value = node.getValue("number", new PreferenceKeyMetadata<>(Long.class));
		
		assertEquals("Wrong value.", Long.valueOf(75L), value);
	}
	
	/**
	 * Tests data conversion: string to byte[].
	 * 
	 * @throws PreferencesException if an error occurs.
	 */
	@Test
	public void testGetDataConversionStringToByteArray()
	throws PreferencesException
	{
		Advapi32Util.registrySetStringValue(TEST_ROOT, testKey, "number", "75");
		
		WindowsRegistryNode node = new WindowsRegistryNode(TEST_ROOT, testKey);
		byte[] value = node.getValue("number", new PreferenceKeyMetadata<>(byte[].class));
		
		assertArrayEquals("Wrong value.", "75".getBytes(StandardCharsets.ISO_8859_1), value);
	}
	
	/**
	 * Tests data conversion: string to string list.
	 * 
	 * @throws PreferencesException if an error occurs.
	 */
	@Test
	public void testGetDataConversionStringToStringList()
	throws PreferencesException
	{
		Advapi32Util.registrySetStringValue(TEST_ROOT, testKey, "number", "75");
		
		WindowsRegistryNode node = new WindowsRegistryNode(TEST_ROOT, testKey);
		List<String> value = node.getValue("number", new PreferenceKeyMetadata<>(new TypeToken<List<String>>() {}));
		
		assertEquals("Wrong value.", ImmutableList.of("75"), value);
	}
	
	/**
	 * Tests storage: put string.
	 * 
	 * @throws PreferencesException if an error occurs.
	 */
	@Test
	public void testPutString()
	throws PreferencesException
	{
		WindowsRegistryNode node = new WindowsRegistryNode(TEST_ROOT, testKey);
		node.putValue("name", "galah", new PreferenceKeyMetadata<>(String.class));
		
		String result = Advapi32Util.registryGetStringValue(TEST_ROOT, testKey, "name");
		assertEquals("Wrong result.", "galah", result);
	}
	
	/**
	 * Tests storage: put int.
	 * 
	 * @throws PreferencesException if an error occurs.
	 */
	@Test
	public void testPutInt()
	throws PreferencesException
	{
		WindowsRegistryNode node = new WindowsRegistryNode(TEST_ROOT, testKey);
		node.putValue("number", 75, new PreferenceKeyMetadata<>(Integer.class));
		
		int result = Advapi32Util.registryGetIntValue(TEST_ROOT, testKey, "number");
		assertEquals("Wrong result.", 75, result);
	}
	
	/**
	 * Tests storage: put long.
	 * 
	 * @throws PreferencesException if an error occurs.
	 */
	@Test
	public void testPutLong()
	throws PreferencesException
	{
		WindowsRegistryNode node = new WindowsRegistryNode(TEST_ROOT, testKey);
		node.putValue("number", 75L, new PreferenceKeyMetadata<>(Long.class));
		
		long result = Advapi32Util.registryGetLongValue(TEST_ROOT, testKey, "number");
		assertEquals("Wrong result.", 75L, result);
	}
	
	/**
	 * Tests storage: put byte[].
	 * 
	 * @throws PreferencesException if an error occurs.
	 */
	@Test
	public void testPutByteArray()
	throws PreferencesException
	{
		WindowsRegistryNode node = new WindowsRegistryNode(TEST_ROOT, testKey);
		node.putValue("data", new byte[] {1, 2, 3, 5, 7, 11}, new PreferenceKeyMetadata<>(byte[].class));
		
		byte[] result = Advapi32Util.registryGetBinaryValue(TEST_ROOT, testKey, "data");
		assertArrayEquals("Wrong result.", new byte[] {1, 2, 3, 5, 7, 11}, result);
	}
	
	/**
	 * Tests storage: put string list.
	 * 
	 * @throws PreferencesException if an error occurs.
	 */
	@Test
	public void testPutStringList()
	throws PreferencesException
	{
		WindowsRegistryNode node = new WindowsRegistryNode(TEST_ROOT, testKey);
		node.putValue("items", ImmutableList.of("one", "two", "three"), new PreferenceKeyMetadata<>(new TypeToken<List<String>>(){}));
		
		String[] result = Advapi32Util.registryGetStringArray(TEST_ROOT, testKey, "items");
		assertArrayEquals("Wrong result.", new String[] {"one", "two", "three"}, result);
	}
	
	/**
	 * Tests removing all values from a node.
	 * 
	 * @throws PreferencesException if an error occurs.
	 */
	@Test
	public void testRemoveAllValues()
	throws PreferencesException
	{
		Advapi32Util.registrySetStringValue(TEST_ROOT, testKey, "name", "galah");
		Advapi32Util.registrySetIntValue(TEST_ROOT, testKey, "number", 75);
		
		WindowsRegistryNode node = new WindowsRegistryNode(TEST_ROOT, testKey);
		node.removeAllValues();
		
		assertFalse("Value should not exist.", Advapi32Util.registryValueExists(TEST_ROOT, testKey, "name"));
		assertFalse("Value should not exist.", Advapi32Util.registryValueExists(TEST_ROOT, testKey, "number"));
	}
	
	/**
	 * Tests removing a value from a node.
	 * 
	 * @throws PreferencesException if an error occurs.
	 */
	@Test
	public void testRemoveValue()
	throws PreferencesException
	{
		Advapi32Util.registrySetStringValue(TEST_ROOT, testKey, "name", "galah");
		Advapi32Util.registrySetIntValue(TEST_ROOT, testKey, "number", 75);
		
		WindowsRegistryNode node = new WindowsRegistryNode(TEST_ROOT, testKey);
		node.removeValue("name", new PreferenceKeyMetadata<>(String.class));
		
		assertFalse("Value should not exist.", Advapi32Util.registryValueExists(TEST_ROOT, testKey, "name"));
		assertTrue("Value should not exist.", Advapi32Util.registryValueExists(TEST_ROOT, testKey, "number"));
	}
	
	/**
	 * Tests using a child node for getting values.
	 * 
	 * @throws PreferencesException if an error occurs.
	 */
	@Test
	public void testChildNodeGet()
	throws PreferencesException
	{
		Advapi32Util.registryCreateKey(TEST_ROOT, testKey, "child");
		String childKey = testKey + RegistryUtils.NODE_SEPARATOR + "child";
		Advapi32Util.registrySetStringValue(TEST_ROOT, childKey, "name", "galah");
		
		WindowsRegistryNode parentNode = new WindowsRegistryNode(TEST_ROOT, testKey);
		PreferenceNode childNode = parentNode.getChildNode("child");
		
		String result = childNode.getValue("name", new PreferenceKeyMetadata<>(String.class));
		assertEquals("Wrong result.", "galah", result);
	}
	
	/**
	 * Tests using a child node for getting values when the key does not exist.
	 * 
	 * @throws PreferencesException if an error occurs.
	 */
	@Test
	public void testChildNodeGetKeyNotExist()
	throws PreferencesException
	{
		WindowsRegistryNode parentNode = new WindowsRegistryNode(TEST_ROOT, testKey);
		PreferenceNode childNode = parentNode.getChildNode("child");
		
		String result = childNode.getValue("name", new PreferenceKeyMetadata<>(String.class));
		assertNull("Should not have value.", result);
	}
	
	/**
	 * Tests using a child node for putting values when the key exists.
	 * 
	 * @throws PreferencesException if an error occurs.
	 */
	@Test
	public void testChildNodePutKeyExists()
	throws PreferencesException
	{
		Advapi32Util.registryCreateKey(TEST_ROOT, testKey, "child");
		String childKey = testKey + RegistryUtils.NODE_SEPARATOR + "child";		
		
		WindowsRegistryNode parentNode = new WindowsRegistryNode(TEST_ROOT, testKey);
		PreferenceNode childNode = parentNode.getChildNode("child");
		
		childNode.putValue("name", "galah", new PreferenceKeyMetadata<>(String.class));
		String result = Advapi32Util.registryGetStringValue(TEST_ROOT, childKey, "name");
		
		assertEquals("Wrong result.", "galah", result);
	}
	
	/**
	 * Tests using a child node for putting values when the key does not exist.
	 * 
	 * @throws PreferencesException if an error occurs.
	 */
	@Test
	public void testChildNodePutKeyNeedsCreating()
	throws PreferencesException
	{
		String childKey = testKey + RegistryUtils.NODE_SEPARATOR + "child";		
		
		WindowsRegistryNode parentNode = new WindowsRegistryNode(TEST_ROOT, testKey);
		PreferenceNode childNode = parentNode.getChildNode("child");
		
		childNode.putValue("name", "galah", new PreferenceKeyMetadata<>(String.class));
		String result = Advapi32Util.registryGetStringValue(TEST_ROOT, childKey, "name");
		
		assertEquals("Wrong result.", "galah", result);
	}
	
	/**
	 * Tests using a child node removing values when the key does not exist.
	 * 
	 * @throws PreferencesException if an error occurs.
	 */
	@Test
	public void testRemoveValuesFromChildKeyNotExist()
	throws PreferencesException
	{
		WindowsRegistryNode parentNode = new WindowsRegistryNode(TEST_ROOT, testKey);
		PreferenceNode childNode = parentNode.getChildNode("child");
		
		//Should not crash
		childNode.removeValue("name", new PreferenceKeyMetadata<>(String.class));
	}
	
	/**
	 * Tests removing a child node.
	 * 
	 * @throws PreferencesException if an error occurs.
	 */
	@Test
	public void testRemoveChildNode()
	throws PreferencesException
	{
		Advapi32Util.registryCreateKey(TEST_ROOT, testKey, "child");
		String childKey = testKey + RegistryUtils.NODE_SEPARATOR + "child";		
		Advapi32Util.registrySetStringValue(TEST_ROOT, childKey, "name", "galah");
		
		WindowsRegistryNode parentNode = new WindowsRegistryNode(TEST_ROOT, testKey);
		
		parentNode.removeChildNode("child");
		
		assertFalse("Should not exist.", Advapi32Util.registryKeyExists(TEST_ROOT, childKey));
	}
	
	/**
	 * Tests removing a child node whose key does not exist.
	 * 
	 * @throws PreferencesException if an error occurs.
	 */
	@Test
	public void testRemoveChildNodeThatDoesNotExist()
	throws PreferencesException
	{
		WindowsRegistryNode parentNode = new WindowsRegistryNode(TEST_ROOT, testKey);
		parentNode.removeChildNode("child");
		
		//Just should not crash
	}
	
	/**
	 * Tests getting child node names.
	 * 
	 * @throws PreferencesException if an error occurs.
	 */
	@Test
	public void testGetNodeNames()
	throws PreferencesException
	{
		Advapi32Util.registryCreateKey(TEST_ROOT, testKey, "child");
		String childKey = testKey + RegistryUtils.NODE_SEPARATOR + "child";
		Advapi32Util.registrySetStringValue(TEST_ROOT, childKey, "name", "galah");
		
		WindowsRegistryNode node = new WindowsRegistryNode(TEST_ROOT, testKey);
		assertEquals("Wrong child names.", ImmutableSet.of("child"), node.getNodeNames());
	}
	
	/**
	 * Tests getting child node names when registry key does not exist.
	 * 
	 * @throws PreferencesException if an error occurs.
	 */
	@Test
	public void testGetNodeNamesKeyNotExists()
	throws PreferencesException
	{
		WindowsRegistryNode node = new WindowsRegistryNode(TEST_ROOT, testKey);
		PreferenceNode child = node.getChildNode("child");
		assertEquals("Wrong child names.", Collections.emptySet(), child.getNodeNames());
	}
	
	/**
	 * Tests getting key names.
	 * 
	 * @throws PreferencesException if an error occurs.
	 */
	@Test
	public void testGetKeyNames()
	throws PreferencesException
	{
		Advapi32Util.registrySetStringValue(TEST_ROOT, testKey, "name", "galah");
		Advapi32Util.registrySetIntValue(TEST_ROOT, testKey, "number", 75);
		
		WindowsRegistryNode node = new WindowsRegistryNode(TEST_ROOT, testKey);
		assertEquals("Wrong key names.", ImmutableSet.of("name", "number"), node.getKeyNames());
	}
	
	/**
	 * Tests getting key names when registry key does not exist.
	 * 
	 * @throws PreferencesException if an error occurs.
	 */
	@Test
	public void testGetKeyNamesKeyNotExists()
	throws PreferencesException
	{
		WindowsRegistryNode node = new WindowsRegistryNode(TEST_ROOT, testKey);
		PreferenceNode child = node.getChildNode("child");
		assertEquals("Wrong key names.", Collections.emptySet(), child.getKeyNames());
	}
}
