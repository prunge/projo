package au.net.causal.projo.prefs;

import java.util.Properties;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import org.junit.Ignore;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import au.net.causal.projo.annotation.Preference;
import au.net.causal.projo.annotation.Projo;
import au.net.causal.projo.annotation.Secure;
import au.net.causal.projo.prefs.properties.PropertiesPreferenceNode;
import au.net.causal.projo.prefs.security.ConsoleUiPasswordSource;
import au.net.causal.projo.prefs.security.SwingUiPasswordSource;

/**
 * Tests the builder when we have all the Windows stuff available.
 * 
 * @author prunge
 */
public class TestBuilder
{
	private static final Logger log = LoggerFactory.getLogger(TestBuilder.class);
		
	@Ignore
	@Test
	public void test()
	throws Exception
	{
		Properties props = new Properties();
		PropertiesPreferenceNode node = new PropertiesPreferenceNode(props);
		
		ProjoStore store = ProjoStoreBuilder.builder().defaults()
							.configureSecurityForNativeSystem(new ConsoleUiPasswordSource())
							//.configureSecurityForMasterPassword(new SwingUiPasswordSource())
							.node(node)
							.build();
		
		MyConfig config = new MyConfig("john", "galah");
		store.save(config);
		
		log.info(props.toString());
		
		MyConfig readConfig = store.read(MyConfig.class);
		
		log.info(ToStringBuilder.reflectionToString(readConfig, ToStringStyle.SHORT_PREFIX_STYLE));
	}
	
	@Test
	public void testDefaultsPut()
	throws Exception
	{
		try (ProjoStore store = ProjoStoreBuilder.builder().defaults().configureSecurityForNativeSystem(new SwingUiPasswordSource()).build())
		{
			MyConfig obj = new MyConfig("galah", "12345");
			store.save(obj);
		}
	}
	
	@Test
	@Ignore
	public void testDefaultsGet()
	throws Exception
	{
		try (ProjoStore store = ProjoStoreBuilder.builder().defaults().configureSecurityForNativeSystem(new SwingUiPasswordSource()).build())
		{
			MyConfig obj = store.read(MyConfig.class);
			log.info(ToStringBuilder.reflectionToString(obj, ToStringStyle.SHORT_PREFIX_STYLE));
		}
	}
	
	
	@Projo
	public static class MyConfig
	{
		@Preference
		private String userId;
		
		@Preference
		@Secure
		private String password;
		
		public MyConfig()
		{
			this(null, null);
		}
		
		public MyConfig(String userId, String password)
		{
			this.userId = userId;
			this.password = password;
		}

		public String getUserId()
		{
			return(userId);
		}

		public void setUserId(String userId)
		{
			this.userId = userId;
		}

		public String getPassword()
		{
			return(password);
		}

		public void setPassword(String password)
		{
			this.password = password;
		}
	}

}
