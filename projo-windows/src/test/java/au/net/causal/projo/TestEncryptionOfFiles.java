package au.net.causal.projo;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.attribute.BasicFileAttributeView;
import java.nio.file.attribute.BasicFileAttributes;
import java.nio.file.attribute.UserDefinedFileAttributeView;

import org.junit.Ignore;
import org.junit.Test;

import com.sun.jna.Native;
import com.sun.jna.platform.win32.Advapi32;
import com.sun.jna.platform.win32.Kernel32;
import com.sun.jna.platform.win32.Kernel32Util;
import com.sun.jna.platform.win32.Win32Exception;
import com.sun.jna.platform.win32.WinDef.DWORD;
import com.sun.jna.win32.W32APIOptions;

/**
 * Can I encrypt files with Windows EFS using NIO2 alone?
 * 
 * @author prunge
 */
public class TestEncryptionOfFiles
{
	@Test
	@Ignore
	public void test()
	throws IOException
	{
		Path path = Paths.get("c:\\temp\\hello.txt");
		System.out.println("Path: " + path);
		
		//Hmm, apparently not, need to use JNA
		UserDefinedFileAttributeView view = Files.getFileAttributeView(path, UserDefinedFileAttributeView.class);
		System.out.println(view.list());
		
		BasicFileAttributeView basic = Files.getFileAttributeView(path, BasicFileAttributeView.class);
		BasicFileAttributes basicAtts = basic.readAttributes();
		System.out.println("Basic: " + basicAtts);
		
		String windowsFilePath = "\\\\?\\" + path.toRealPath().toString();
		int atts = Kernel32Util.getFileAttributes(windowsFilePath);
		if (atts == 0)
			throw new Win32Exception(Kernel32.INSTANCE.GetLastError());
		
		System.out.println("Atts: " + atts);
		
		//Toggle encryption
		boolean encrypted = ((atts & Kernel32.FILE_ATTRIBUTE_ENCRYPTED) != 0);
		System.out.println("Encrypted: " + encrypted);
		
		encrypted = !encrypted;

		boolean ok;
		if (encrypted)
			ok = Advapi32X.INSTANCE.EncryptFile(windowsFilePath);
		else
			ok = Advapi32X.INSTANCE.DecryptFile(windowsFilePath, new DWORD());
		
		if (!ok)
			throw new Win32Exception(Kernel32.INSTANCE.GetLastError());
		
			
		
		/*
		if (encrypted)
			atts = atts | Kernel32.FILE_ATTRIBUTE_ENCRYPTED;
		else
			atts = atts & ~Kernel32.FILE_ATTRIBUTE_ENCRYPTED;

		System.out.println("New atts: " + atts);
		if (atts == 0)
			atts = Kernel32.FILE_ATTRIBUTE_NORMAL;
		
		boolean ok = Kernel32.INSTANCE.SetFileAttributes(windowsFilePath, new DWORD(atts));
		if (!ok)
			throw new Win32Exception(Kernel32.INSTANCE.GetLastError());
		*/
	}
	
	public static interface Advapi32X extends Advapi32
	{
		public Advapi32X INSTANCE = (Advapi32X)Native.loadLibrary("Advapi32", Advapi32X.class, W32APIOptions.UNICODE_OPTIONS);
		
		public boolean EncryptFile(String lpFileName);
		public boolean DecryptFile(String lpFileName, DWORD reserved);
	}

}
