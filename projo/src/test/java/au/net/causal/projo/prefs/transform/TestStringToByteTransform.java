package au.net.causal.projo.prefs.transform;

import static org.junit.Assert.*;

import java.nio.charset.StandardCharsets;

import org.junit.Test;

import au.net.causal.projo.prefs.PreferenceKeyMetadata;
import au.net.causal.projo.prefs.PreferenceNode;
import au.net.causal.projo.prefs.PreferencesException;
import au.net.causal.projo.prefs.StandardTransformPhase;
import au.net.causal.projo.prefs.TransformerPreferenceNode;
import au.net.causal.projo.prefs.memory.InMemoryPreferenceNode;
import au.net.causal.projo.prefs.transform.IntToStringTransformer;
import au.net.causal.projo.prefs.transform.StringToByteTransformer;

public class TestStringToByteTransform
{
	@Test
	public void testPut()
	throws PreferencesException
	{
		PreferenceNode node = new InMemoryPreferenceNode<>(byte[].class);
		TransformerPreferenceNode xNode = new TransformerPreferenceNode(node);
		xNode.addTransformer(new IntToStringTransformer(), StandardTransformPhase.DATATYPE);
		xNode.addTransformer(new StringToByteTransformer(), StandardTransformPhase.DATATYPE);
		
		xNode.putValue("name", "John Galah", new PreferenceKeyMetadata<>(String.class));
		
		byte[] data = node.getValue("name", new PreferenceKeyMetadata<>(byte[].class));
		assertArrayEquals("Wrong result.", "John Galah".getBytes(StandardCharsets.UTF_8), data);
	}
	
	@Test
	public void testGet()
	throws PreferencesException
	{
		InMemoryPreferenceNode<byte[]> node = new InMemoryPreferenceNode<>(byte[].class);
		node.putValue("name", "Jenny Galah".getBytes(StandardCharsets.UTF_8), new PreferenceKeyMetadata<>(byte[].class));
		
		TransformerPreferenceNode xNode = new TransformerPreferenceNode(node);
		xNode.addTransformer(new IntToStringTransformer(), StandardTransformPhase.DATATYPE);
		xNode.addTransformer(new StringToByteTransformer(), StandardTransformPhase.DATATYPE);
		
		String result = xNode.getValue("name", new PreferenceKeyMetadata<>(String.class));
		assertEquals("Wrong result.", "Jenny Galah", result);
	}
	
	@Test
	public void testRemove()
	throws PreferencesException
	{
		InMemoryPreferenceNode<byte[]> node = new InMemoryPreferenceNode<>(byte[].class);
		node.putValue("name", "Jenny Galah".getBytes(StandardCharsets.UTF_8), new PreferenceKeyMetadata<>(byte[].class));
		
		TransformerPreferenceNode xNode = new TransformerPreferenceNode(node);
		xNode.addTransformer(new IntToStringTransformer(), StandardTransformPhase.DATATYPE);
		xNode.addTransformer(new StringToByteTransformer(), StandardTransformPhase.DATATYPE);
		
		xNode.removeValue("name", new PreferenceKeyMetadata<>(String.class));
		
		byte[] data = node.getValue("name", new PreferenceKeyMetadata<>(byte[].class));
		assertNull("Value should not exist.", data);
	}
	
	@Test
	public void testDataTypeSupport()
	throws PreferencesException
	{
		InMemoryPreferenceNode<byte[]> node = new InMemoryPreferenceNode<>(byte[].class);
		TransformerPreferenceNode xNode = new TransformerPreferenceNode(node);
		xNode.addTransformer(new IntToStringTransformer(), StandardTransformPhase.DATATYPE);
		xNode.addTransformer(new StringToByteTransformer(), StandardTransformPhase.DATATYPE);
		
		boolean stringSupport = xNode.isDataTypeSupported(new PreferenceKeyMetadata<>(String.class));
		
		assertTrue("Should have string support.", stringSupport);
	}

}
