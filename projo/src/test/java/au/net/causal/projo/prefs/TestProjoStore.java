package au.net.causal.projo.prefs;

import static org.junit.Assert.*;

import java.util.Properties;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import au.net.causal.projo.annotation.PartialPreferences;
import au.net.causal.projo.annotation.Preference;
import au.net.causal.projo.annotation.PreferenceRoot;
import au.net.causal.projo.annotation.Projo;
import au.net.causal.projo.prefs.properties.PropertiesPreferenceNode;
import au.net.causal.projo.prefs.transform.Base64Transformer;
import au.net.causal.projo.prefs.transform.IntToStringTransformer;
import au.net.causal.projo.prefs.transform.ListTransformer;
import au.net.causal.projo.prefs.transform.RectangleTransformer;
import au.net.causal.projo.prefs.transform.StringToByteTransformer;

import com.google.common.collect.ImmutableMap;

public class TestProjoStore
{
	private static final Logger log = LoggerFactory.getLogger(TestProjoStore.class);
	
	private PreferenceNode createPreferenceNode(Properties props)
	{
		PreferenceNode node = new PropertiesPreferenceNode(props);
		TransformerPreferenceNode xNode = new TransformerPreferenceNode(node);
		
		xNode.addTransformer(new IntToStringTransformer(), StandardTransformPhase.DATATYPE);
		xNode.addTransformer(new StringToByteTransformer(), StandardTransformPhase.DATATYPE);
		xNode.addTransformer(new Base64Transformer(), StandardTransformPhase.DATATYPE);
		xNode.addTransformer(new RectangleTransformer(), StandardTransformPhase.DATATYPE);
		xNode.addTransformer(new ListTransformer(), StandardTransformPhase.DATATYPE);
		//xNode.addTransformer(new EncryptedValueTransformer(), StandardTransformPhase.DATATYPE);
		
		return(xNode);
	}
	
	@Test
	public void testSave()
	throws Exception
	{
		Properties props = new Properties();
		PreferenceNode node = createPreferenceNode(props);
		ProjoStore store = new ProjoStore(node);
		store.setPreferenceRootUsed(false);
		
		MyPrefObj projo = new MyPrefObj("John Galah", 22);
		store.save(projo);
		
		log.info("Props: " + props);
		
		assertEquals("Wrong properties.",
				ImmutableMap.of("name", "John Galah", "age", "22"),
				props);
	}
	
	@Test
	public void testReadWithType()
	throws Exception
	{
		Properties props = new Properties();
		PreferenceNode node = createPreferenceNode(props);
		ProjoStore store = new ProjoStore(node);
		store.setPreferenceRootUsed(false);
		
		props.setProperty("name", "John Galah");
		props.setProperty("age", "22");
		
		MyPrefObj projo = store.read(MyPrefObj.class);
		
		log.info("Projo: " + projo);
		
		assertEquals("Wrong values.", "John Galah", projo.getName());
		assertEquals("Wrong values.", 22, projo.getAge());
	}
	
	@Test
	public void testRemove()
	throws Exception
	{
		Properties props = new Properties();
		PreferenceNode node = createPreferenceNode(props);
		ProjoStore store = new ProjoStore(node);
		store.setPreferenceRootUsed(false);
		
		props.setProperty("name", "John Galah");
		props.setProperty("age", "22");
		props.setProperty("other", "hello");
		
		MyPrefObj projo =  new MyPrefObj();
		store.remove(projo);
		
		assertEquals("Wrong properties.",
				ImmutableMap.of("other", "hello"),
				props);
	}
	
	@Test
	public void testReadNested()
	throws Exception
	{
		Properties props = new Properties();
		PreferenceNode node = createPreferenceNode(props);
		ProjoStore store = new ProjoStore(node);
		store.setPreferenceRootUsed(false);
		
		props.setProperty("child.name", "John Galah");
		props.setProperty("child.age", "22");
		
		MyParentObj projo = store.read(MyParentObj.class);
		
		log.info("Projo: " + projo);
		
		assertEquals("Wrong values.", "John Galah", projo.getChild().getName());
		assertEquals("Wrong values.", 22, projo.getChild().getAge());
	}
	
	@Test
	public void testSaveNested()
	throws Exception
	{
		Properties props = new Properties();
		PreferenceNode node = createPreferenceNode(props);
		ProjoStore store = new ProjoStore(node);
		store.setPreferenceRootUsed(false);
		
		MyParentObj projo = new MyParentObj();
		projo.setChild(new MyPrefObj("John Galah", 22));
		store.save(projo);
		
		log.info("Props: " + props);
		
		assertEquals("Wrong properties.",
				ImmutableMap.of("child.name", "John Galah", "child.age", "22"),
				props);
	}
	
	@Test
	public void testRemoveNested()
	throws Exception
	{
		Properties props = new Properties();
		PreferenceNode node = createPreferenceNode(props);
		ProjoStore store = new ProjoStore(node);
		store.setPreferenceRootUsed(false);
		
		props.setProperty("child.name", "John Galah");
		props.setProperty("child.age", "22");
		props.setProperty("child.other", "hello");
		props.setProperty("other", "hello");
		
		store.remove(MyParentObj.class);
		
		assertEquals("Wrong properties.",
				ImmutableMap.of("other", "hello", "child.other", "hello"),
				props);
	}
	
	@Test
	public void testReadUsingPreferenceRootConfiguration()
	throws Exception
	{
		Properties props = new Properties();
		PreferenceNode node = createPreferenceNode(props);
		ProjoStore store = new ProjoStore(node);		
		
		props.setProperty("au.net.causal.projo.prefs.TestProjoStore.MyPrefObj.name", "John Galah");
		props.setProperty("au.net.causal.projo.prefs.TestProjoStore.MyPrefObj.age", "22");
		
		MyPrefObj projo = store.read(MyPrefObj.class);
		
		log.info("Projo: " + projo);
		
		assertEquals("Wrong values.", "John Galah", projo.getName());
		assertEquals("Wrong values.", 22, projo.getAge());
	}
	
	@Test
	public void testReadUsingCustomPreferenceRootConfiguration()
	throws Exception
	{
		Properties props = new Properties();
		PreferenceNode node = createPreferenceNode(props);
		ProjoStore store = new ProjoStore(node);		
		
		props.setProperty("person.name", "John Galah");
		props.setProperty("person.age", "22");
		
		MyPrefObjCustomRoot projo = store.read(MyPrefObjCustomRoot.class);
		
		log.info("Projo: " + projo);
		
		assertEquals("Wrong values.", "John Galah", projo.getName());
		assertEquals("Wrong values.", 22, projo.getAge());
	}
	
	@Test
	public void testWithNonPreferenceProperty()
	throws Exception
	{
		Properties props = new Properties();
		PreferenceNode node = createPreferenceNode(props);
		ProjoStore store = new ProjoStore(node);		
		
		props.setProperty("person.name", "John Galah");
		
		MyPrefObjWithNonPreferenceProperty projo = store.read(MyPrefObjWithNonPreferenceProperty.class);
		
		log.info("Projo: " + projo);
		
		assertEquals("Wrong values.", "John Galah", projo.getName());
		assertEquals("Wrong values.", 0, projo.getAge());
	}
	
	@Projo
	@PartialPreferences
	public static class MyParentObj
	{
		@Preference
		private MyPrefObj child;

		public MyPrefObj getChild()
		{
			return(child);
		}

		public void setChild(MyPrefObj child)
		{
			this.child = child;
		}
		
		@Override
		public String toString()
		{
			return(ToStringBuilder.reflectionToString(this, ToStringStyle.SHORT_PREFIX_STYLE));
		}
	}
	
	@Projo
	@PartialPreferences
	public static class MyPrefObj
	{
		@Preference
		private String name;
		
		@Preference
		private int age;

		public MyPrefObj()
		{
			this(null, 0);
		}
		
		public MyPrefObj(String name, int age)
		{
			super();
			this.name = name;
			this.age = age;
		}

		public String getName()
		{
			return(name);
		}

		public void setName(String name)
		{
			this.name = name;
		}

		public int getAge()
		{
			return(age);
		}

		public void setAge(int age)
		{
			this.age = age;
		}
		
		@Override
		public String toString()
		{
			return(ToStringBuilder.reflectionToString(this, ToStringStyle.SHORT_PREFIX_STYLE));
		}
	}
	
	@Projo
	@PartialPreferences
	@PreferenceRoot(name="person")
	public static class MyPrefObjCustomRoot
	{
		@Preference
		private String name;
		
		@Preference
		private int age;

		public MyPrefObjCustomRoot()
		{
			this(null, 0);
		}
		
		public MyPrefObjCustomRoot(String name, int age)
		{
			super();
			this.name = name;
			this.age = age;
		}

		public String getName()
		{
			return(name);
		}

		public void setName(String name)
		{
			this.name = name;
		}

		public int getAge()
		{
			return(age);
		}

		public void setAge(int age)
		{
			this.age = age;
		}
		
		@Override
		public String toString()
		{
			return(ToStringBuilder.reflectionToString(this, ToStringStyle.SHORT_PREFIX_STYLE));
		}
	}

	@Projo
	@PartialPreferences
	@PreferenceRoot(name="person")
	public static class MyPrefObjWithNonPreferenceProperty
	{
		@Preference
		private String name;
		
		private int age;

		public MyPrefObjWithNonPreferenceProperty()
		{
			this(null, 0);
		}
		
		public MyPrefObjWithNonPreferenceProperty(String name, int age)
		{
			super();
			this.name = name;
			this.age = age;
		}

		public String getName()
		{
			return(name);
		}

		public void setName(String name)
		{
			this.name = name;
		}

		public int getAge()
		{
			return(age);
		}

		public void setAge(int age)
		{
			this.age = age;
		}
		
		@Override
		public String toString()
		{
			return(ToStringBuilder.reflectionToString(this, ToStringStyle.SHORT_PREFIX_STYLE));
		}
	}
}
