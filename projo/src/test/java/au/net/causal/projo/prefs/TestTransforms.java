package au.net.causal.projo.prefs;

import static org.junit.Assert.*;

import java.awt.Rectangle;
import java.util.List;
import java.util.Properties;

import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.common.collect.ImmutableList;
import com.google.common.reflect.TypeToken;

import au.net.causal.projo.prefs.properties.PropertiesPreferenceNode;
import au.net.causal.projo.prefs.transform.IntToStringTransformer;
import au.net.causal.projo.prefs.transform.ListTransformer;
import au.net.causal.projo.prefs.transform.RectangleTransformer;

/**
 * Tests the transform API.
 * 
 * @author prunge
 */
public class TestTransforms
{
	private static final Logger log = LoggerFactory.getLogger(TestTransforms.class);

	/**
	 * Simple data type transform test, transforming integer to string.
	 * 
	 * @throws PreferencesException if an error occurs.
	 */
	@Test
	public void testDataTypeTransformRead()
	throws PreferencesException
	{
		Properties props = new Properties();
		props.setProperty("person.name", "John Galah");
		props.setProperty("person.age", "22");
		PreferenceNode node = new PropertiesPreferenceNode(props);
		TransformerPreferenceNode xNode = new TransformerPreferenceNode(node);
		xNode.addTransformer(new IntToStringTransformer(), StandardTransformPhase.DATATYPE);
		
		PreferenceNode childNode = xNode.getChildNode("person");
		
		Integer age = childNode.getValue("age", new PreferenceKeyMetadata<>(Integer.class));
		
		assertEquals("Wrong age.", Integer.valueOf(22), age);
	}
	
	/**
	 * Simple data type transform test, transforming integer to string with key mutation.
	 * 
	 * @throws PreferencesException if an error occurs.
	 */
	@Test
	public void testDataTypeTransformReadWithKeyMutation()
	throws PreferencesException
	{
		Properties props = new Properties();
		props.setProperty("screen.name", "J1");
		props.setProperty("screen.areaX", "1");
		props.setProperty("screen.areaY", "2");
		props.setProperty("screen.areaWidth", "3");
		props.setProperty("screen.areaHeight", "4");
		PreferenceNode node = new PropertiesPreferenceNode(props);
		TransformerPreferenceNode xNode = new TransformerPreferenceNode(node);
		xNode.addTransformer(new IntToStringTransformer(), StandardTransformPhase.DATATYPE);
		xNode.addTransformer(new RectangleTransformer(), StandardTransformPhase.DATATYPE);
		
		PreferenceNode childNode = xNode.getChildNode("screen");
		
		Rectangle area = childNode.getValue("area", new PreferenceKeyMetadata<>(Rectangle.class));
		
		assertEquals("Wrong area.", new Rectangle(1, 2, 3, 4), area);
	}

	/**
	 * Simple data type transform test, transforming integer to string.
	 * 
	 * @throws PreferencesException if an error occurs.
	 */
	@Test
	public void testDataTypeTransformWrite()
	throws PreferencesException
	{
		Properties props = new Properties();
		props.setProperty("person.name", "John Galah");
		props.setProperty("person.age", "22");
		PreferenceNode node = new PropertiesPreferenceNode(props);
		TransformerPreferenceNode xNode = new TransformerPreferenceNode(node);
		xNode.addTransformer(new IntToStringTransformer(), StandardTransformPhase.DATATYPE);
		
		PreferenceNode childNode = xNode.getChildNode("person");
		
		childNode.putValue("age", 20, new PreferenceKeyMetadata<>(Integer.class));
		
		String ageStr = props.getProperty("person.age");
		
		assertEquals("Wrong age.", "20", ageStr);
	}
	
	/**
	 * Simple data type transform test, transforming integer to string with key mutation.
	 * 
	 * @throws PreferencesException if an error occurs.
	 */
	@Test
	public void testDataTypeTransformWriteWithKeyMutation()
	throws PreferencesException
	{
		Properties props = new Properties();
		props.setProperty("screen.name", "J1");
		props.setProperty("screen.areaX", "1");
		props.setProperty("screen.areaY", "2");
		props.setProperty("screen.areaWidth", "3");
		props.setProperty("screen.areaHeight", "4");
		PreferenceNode node = new PropertiesPreferenceNode(props);
		TransformerPreferenceNode xNode = new TransformerPreferenceNode(node);
		xNode.addTransformer(new IntToStringTransformer(), StandardTransformPhase.DATATYPE);
		xNode.addTransformer(new RectangleTransformer(), StandardTransformPhase.DATATYPE);
		
		PreferenceNode childNode = xNode.getChildNode("screen");
		
		childNode.putValue("area", new Rectangle(5, 6, 7, 8), new PreferenceKeyMetadata<>(Rectangle.class));
		
		assertEquals("Wrong value.", "5", props.getProperty("screen.areaX"));
		assertEquals("Wrong value.", "6", props.getProperty("screen.areaY"));
		assertEquals("Wrong value.", "7", props.getProperty("screen.areaWidth"));
		assertEquals("Wrong value.", "8", props.getProperty("screen.areaHeight"));
	}
	
	/**
	 * Tests removal without key mutation.
	 * 
	 * @throws PreferencesException if an error occurs.
	 */
	@Test
	public void testDataTypeTransformRemove()
	throws PreferencesException
	{
		Properties props = new Properties();
		props.setProperty("person.name", "John Galah");
		props.setProperty("person.age", "22");
		PreferenceNode node = new PropertiesPreferenceNode(props);
		TransformerPreferenceNode xNode = new TransformerPreferenceNode(node);
		xNode.addTransformer(new IntToStringTransformer(), StandardTransformPhase.DATATYPE);
		
		PreferenceNode childNode = xNode.getChildNode("person");
		
		childNode.removeValue("age", new PreferenceKeyMetadata<>(Integer.class));
		
		String ageStr = props.getProperty("person.age");
		
		assertNull("Should not have value.", ageStr);
	}
	
	/**
	 * Tests removal with key mutation.
	 * 
	 * @throws PreferencesException if an error occurs.
	 */
	@Test
	public void testDataTypeTransformRemoveWithKeyMutation()
	throws PreferencesException
	{
		Properties props = new Properties();
		props.setProperty("screen.name", "J1");
		props.setProperty("screen.areaX", "1");
		props.setProperty("screen.areaY", "2");
		props.setProperty("screen.areaWidth", "3");
		props.setProperty("screen.areaHeight", "4");
		PreferenceNode node = new PropertiesPreferenceNode(props);
		TransformerPreferenceNode xNode = new TransformerPreferenceNode(node);
		xNode.addTransformer(new IntToStringTransformer(), StandardTransformPhase.DATATYPE);
		xNode.addTransformer(new RectangleTransformer(), StandardTransformPhase.DATATYPE);
		
		PreferenceNode childNode = xNode.getChildNode("screen");
		
		childNode.removeValue("area", new PreferenceKeyMetadata<>(Rectangle.class));
		
		assertNull("Should not have value.", props.getProperty("screen.areaX"));
		assertNull("Should not have value.", props.getProperty("screen.areaY"));
		assertNull("Should not have value.", props.getProperty("screen.areaWidth"));
		assertNull("Should not have value.", props.getProperty("screen.areaHeight"));
	}
	
	@Test
	public void testListPut()
	throws PreferencesException
	{
		Properties props = new Properties();
		props.setProperty("screen.name", "J1");
		props.setProperty("screen.areaX", "1");
		props.setProperty("screen.areaY", "2");
		props.setProperty("screen.areaWidth", "3");
		props.setProperty("screen.areaHeight", "4");
		PreferenceNode node = new PropertiesPreferenceNode(props);
		TransformerPreferenceNode xNode = new TransformerPreferenceNode(node);
		xNode.addTransformer(new IntToStringTransformer(), StandardTransformPhase.DATATYPE);
		xNode.addTransformer(new RectangleTransformer(), StandardTransformPhase.DATATYPE);
		xNode.addTransformer(new ListTransformer(), StandardTransformPhase.DATATYPE);
		
		PreferenceNode childNode = xNode.getChildNode("screen");
		
		List<Rectangle> list = ImmutableList.of(new Rectangle(1, 2, 3, 4), new Rectangle(5, 6, 7, 8));
		childNode.putValue("area", list, new PreferenceKeyMetadata<>(new TypeToken<List<Rectangle>>(){}));
		
		log.info("Props: " + props);
		
		assertEquals("Wrong value.", "1,5", props.getProperty("screen.areaX"));
		assertEquals("Wrong value.", "2,6", props.getProperty("screen.areaY"));
		assertEquals("Wrong value.", "3,7", props.getProperty("screen.areaWidth"));
		assertEquals("Wrong value.", "4,8", props.getProperty("screen.areaHeight"));
	}
	
	@Test
	public void testListGet()
	throws PreferencesException
	{
		Properties props = new Properties();
		props.setProperty("screen.name", "J1");
		props.setProperty("screen.areaX", "1,2");
		props.setProperty("screen.areaY", "3,4");
		props.setProperty("screen.areaWidth", "5,6");
		props.setProperty("screen.areaHeight", "7,8");
		PreferenceNode node = new PropertiesPreferenceNode(props);
		TransformerPreferenceNode xNode = new TransformerPreferenceNode(node);
		xNode.addTransformer(new IntToStringTransformer(), StandardTransformPhase.DATATYPE);
		xNode.addTransformer(new RectangleTransformer(), StandardTransformPhase.DATATYPE);
		xNode.addTransformer(new ListTransformer(), StandardTransformPhase.DATATYPE);
		
		PreferenceNode childNode = xNode.getChildNode("screen");
		
		List<Rectangle> result = childNode.getValue("area", new PreferenceKeyMetadata<>(new TypeToken<List<Rectangle>>(){}));
		
		log.info("Result: " + result);
		
		assertEquals("Wrong result.", 
				ImmutableList.of(new Rectangle(1, 3, 5, 7), new Rectangle(2, 4, 6, 8)),
				result);
	}
	
	@Test
	public void testListRemove()
	throws PreferencesException
	{
		Properties props = new Properties();
		props.setProperty("screen.name", "J1");
		props.setProperty("screen.areaX", "1,2");
		props.setProperty("screen.areaY", "3,4");
		props.setProperty("screen.areaWidth", "5,6");
		props.setProperty("screen.areaHeight", "7,8");
		PreferenceNode node = new PropertiesPreferenceNode(props);
		TransformerPreferenceNode xNode = new TransformerPreferenceNode(node);
		xNode.addTransformer(new IntToStringTransformer(), StandardTransformPhase.DATATYPE);
		xNode.addTransformer(new RectangleTransformer(), StandardTransformPhase.DATATYPE);
		xNode.addTransformer(new ListTransformer(), StandardTransformPhase.DATATYPE);
		
		PreferenceNode childNode = xNode.getChildNode("screen");
		
		childNode.removeValue("area", new PreferenceKeyMetadata<>(Rectangle.class));
		
		log.info("Remaining props: " + props);
		
		assertNull("Should not have property.", props.getProperty("screen.areaX"));
		assertNull("Should not have property.", props.getProperty("screen.areaY"));
		assertNull("Should not have property.", props.getProperty("screen.areaWidth"));
		assertNull("Should not have property.", props.getProperty("screen.areaHeight"));
	}
}
