package au.net.causal.projo.prefs.transform;

import static org.junit.Assert.*;

import java.awt.Color;
import java.awt.SystemColor;

import org.junit.Test;

import au.net.causal.projo.prefs.PreferenceKeyMetadata;
import au.net.causal.projo.prefs.PreferencesException;
import au.net.causal.projo.prefs.StandardTransformPhase;
import au.net.causal.projo.prefs.TransformerPreferenceNode;
import au.net.causal.projo.prefs.memory.InMemoryPreferenceNode;
import au.net.causal.projo.prefs.transform.ColorToStringTransformer;
import au.net.causal.projo.prefs.transform.EnumToStringTransformer;
import au.net.causal.projo.prefs.transform.IntToStringTransformer;
import au.net.causal.projo.prefs.transform.StringToByteTransformer;

public class TestColorToStringTransformer
{
	private static final double DELTA = 1.0E-6;
	
	@Test
	public void testPutNoAlpha()
	throws PreferencesException
	{
		InMemoryPreferenceNode<String> node = new InMemoryPreferenceNode<>(String.class);
		TransformerPreferenceNode xNode = new TransformerPreferenceNode(node);
		xNode.addTransformer(new EnumToStringTransformer(), StandardTransformPhase.DATATYPE);
		xNode.addTransformer(new IntToStringTransformer(), StandardTransformPhase.DATATYPE);
		xNode.addTransformer(new StringToByteTransformer(), StandardTransformPhase.DATATYPE);
		xNode.addTransformer(new ColorToStringTransformer(), StandardTransformPhase.DATATYPE);
		
		xNode.putValue("val", new Color(255, 0, 0), new PreferenceKeyMetadata<>(Color.class));
		
		String data = node.getNativeValue("val");
		assertEquals("Wrong value.", "#ff0000", data);
	}
	
	@Test
	public void testPutWithAlpha()
	throws PreferencesException
	{
		InMemoryPreferenceNode<String> node = new InMemoryPreferenceNode<>(String.class);
		TransformerPreferenceNode xNode = new TransformerPreferenceNode(node);
		xNode.addTransformer(new EnumToStringTransformer(), StandardTransformPhase.DATATYPE);
		xNode.addTransformer(new IntToStringTransformer(), StandardTransformPhase.DATATYPE);
		xNode.addTransformer(new StringToByteTransformer(), StandardTransformPhase.DATATYPE);
		xNode.addTransformer(new ColorToStringTransformer(), StandardTransformPhase.DATATYPE);
		
		xNode.putValue("val", new Color(1.0f, 0.0f, 0.0f, 0.5f), new PreferenceKeyMetadata<>(Color.class));
		
		String data = node.getNativeValue("val");
		assertEquals("Wrong value.", "rgba(1.0, 0.0, 0.0, 0.5)", data);
	}
	
	@Test
	public void testGetDoubleDigitHex()
	throws PreferencesException
	{
		InMemoryPreferenceNode<String> node = new InMemoryPreferenceNode<>(String.class);
		node.putNativeValue("val", "#ff0099");
		
		TransformerPreferenceNode xNode = new TransformerPreferenceNode(node);
		xNode.addTransformer(new EnumToStringTransformer(), StandardTransformPhase.DATATYPE);
		xNode.addTransformer(new IntToStringTransformer(), StandardTransformPhase.DATATYPE);
		xNode.addTransformer(new StringToByteTransformer(), StandardTransformPhase.DATATYPE);
		xNode.addTransformer(new ColorToStringTransformer(), StandardTransformPhase.DATATYPE);
		
		Color result = xNode.getValue("val", new PreferenceKeyMetadata<>(Color.class));
		assertEquals("Wrong result.", new Color(0xff, 0x00, 0x99), result);
	}
	
	@Test
	public void testGetSingleDigitHex()
	throws PreferencesException
	{
		InMemoryPreferenceNode<String> node = new InMemoryPreferenceNode<>(String.class);
		node.putNativeValue("val", "#f09");
		
		TransformerPreferenceNode xNode = new TransformerPreferenceNode(node);
		xNode.addTransformer(new EnumToStringTransformer(), StandardTransformPhase.DATATYPE);
		xNode.addTransformer(new IntToStringTransformer(), StandardTransformPhase.DATATYPE);
		xNode.addTransformer(new StringToByteTransformer(), StandardTransformPhase.DATATYPE);
		xNode.addTransformer(new ColorToStringTransformer(), StandardTransformPhase.DATATYPE);
		
		Color result = xNode.getValue("val", new PreferenceKeyMetadata<>(Color.class));
		assertEquals("Wrong result.", new Color(0xff, 0x00, 0x99), result);
	}
	
	@Test
	public void testGetRgb()
	throws PreferencesException
	{
		InMemoryPreferenceNode<String> node = new InMemoryPreferenceNode<>(String.class);
		node.putNativeValue("val", "rgb(255, 0, 100)");
		
		TransformerPreferenceNode xNode = new TransformerPreferenceNode(node);
		xNode.addTransformer(new EnumToStringTransformer(), StandardTransformPhase.DATATYPE);
		xNode.addTransformer(new IntToStringTransformer(), StandardTransformPhase.DATATYPE);
		xNode.addTransformer(new StringToByteTransformer(), StandardTransformPhase.DATATYPE);
		xNode.addTransformer(new ColorToStringTransformer(), StandardTransformPhase.DATATYPE);
		
		Color result = xNode.getValue("val", new PreferenceKeyMetadata<>(Color.class));
		assertEquals("Wrong result.", new Color(255, 0, 100), result);
	}
	
	@Test
	public void testGetRgbFloat()
	throws PreferencesException
	{
		InMemoryPreferenceNode<String> node = new InMemoryPreferenceNode<>(String.class);
		node.putNativeValue("val", "rgb(1.0, 0.0, 0.5)");
		
		TransformerPreferenceNode xNode = new TransformerPreferenceNode(node);
		xNode.addTransformer(new EnumToStringTransformer(), StandardTransformPhase.DATATYPE);
		xNode.addTransformer(new IntToStringTransformer(), StandardTransformPhase.DATATYPE);
		xNode.addTransformer(new StringToByteTransformer(), StandardTransformPhase.DATATYPE);
		xNode.addTransformer(new ColorToStringTransformer(), StandardTransformPhase.DATATYPE);
		
		Color result = xNode.getValue("val", new PreferenceKeyMetadata<>(Color.class));
		assertEquals("Wrong result.", new Color(1.0f, 0.0f, 0.5f), result);
	}
	
	@Test
	public void testGetRgbPercent()
	throws PreferencesException
	{
		InMemoryPreferenceNode<String> node = new InMemoryPreferenceNode<>(String.class);
		node.putNativeValue("val", "rgb(100%, 50%, 0)");
		
		TransformerPreferenceNode xNode = new TransformerPreferenceNode(node);
		xNode.addTransformer(new EnumToStringTransformer(), StandardTransformPhase.DATATYPE);
		xNode.addTransformer(new IntToStringTransformer(), StandardTransformPhase.DATATYPE);
		xNode.addTransformer(new StringToByteTransformer(), StandardTransformPhase.DATATYPE);
		xNode.addTransformer(new ColorToStringTransformer(), StandardTransformPhase.DATATYPE);
		
		Color result = xNode.getValue("val", new PreferenceKeyMetadata<>(Color.class));
		assertEquals("Wrong result.", new Color(1.0f, 0.5f, 0.0f), result);
	}
	
	@Test
	public void testGetArgb()
	throws PreferencesException
	{
		InMemoryPreferenceNode<String> node = new InMemoryPreferenceNode<>(String.class);
		node.putNativeValue("val", "rgba(255, 255, 0, 0.5)");
		
		TransformerPreferenceNode xNode = new TransformerPreferenceNode(node);
		xNode.addTransformer(new EnumToStringTransformer(), StandardTransformPhase.DATATYPE);
		xNode.addTransformer(new IntToStringTransformer(), StandardTransformPhase.DATATYPE);
		xNode.addTransformer(new StringToByteTransformer(), StandardTransformPhase.DATATYPE);
		xNode.addTransformer(new ColorToStringTransformer(), StandardTransformPhase.DATATYPE);
		
		Color result = xNode.getValue("val", new PreferenceKeyMetadata<>(Color.class));
		assertEquals("Wrong result.", new Color(1.0f, 1.0f, 0.0f, 0.5f), result);
		assertEquals("Wrong alpha.", result.getRGBComponents(null)[3], 0.5f, DELTA);
	}
	
	@Test
	public void testRemove()
	throws PreferencesException
	{
		InMemoryPreferenceNode<String> node = new InMemoryPreferenceNode<>(String.class);
		node.putNativeValue("val", "#aabbcc");
		
		TransformerPreferenceNode xNode = new TransformerPreferenceNode(node);
		xNode.addTransformer(new EnumToStringTransformer(), StandardTransformPhase.DATATYPE);
		xNode.addTransformer(new IntToStringTransformer(), StandardTransformPhase.DATATYPE);
		xNode.addTransformer(new StringToByteTransformer(), StandardTransformPhase.DATATYPE);
		xNode.addTransformer(new ColorToStringTransformer(), StandardTransformPhase.DATATYPE);
		
		xNode.removeValue("val", new PreferenceKeyMetadata<>(Color.class));
		
		String data = node.getNativeValue("val");
		assertNull("Value should not exist.", data);
	}
	
	@Test
	public void testDataTypeSupport()
	throws PreferencesException
	{
		InMemoryPreferenceNode<String> node = new InMemoryPreferenceNode<>(String.class);
		TransformerPreferenceNode xNode = new TransformerPreferenceNode(node);
		xNode.addTransformer(new EnumToStringTransformer(), StandardTransformPhase.DATATYPE);
		xNode.addTransformer(new IntToStringTransformer(), StandardTransformPhase.DATATYPE);
		xNode.addTransformer(new StringToByteTransformer(), StandardTransformPhase.DATATYPE);
		xNode.addTransformer(new ColorToStringTransformer(), StandardTransformPhase.DATATYPE);
		
		boolean support = xNode.isDataTypeSupported(new PreferenceKeyMetadata<>(Color.class));
		
		assertTrue("Should have support.", support);
	}
	
	@Test
	public void testDataTypeSupportSubclass()
	throws PreferencesException
	{
		InMemoryPreferenceNode<String> node = new InMemoryPreferenceNode<>(String.class);
		TransformerPreferenceNode xNode = new TransformerPreferenceNode(node);
		xNode.addTransformer(new EnumToStringTransformer(), StandardTransformPhase.DATATYPE);
		xNode.addTransformer(new IntToStringTransformer(), StandardTransformPhase.DATATYPE);
		xNode.addTransformer(new StringToByteTransformer(), StandardTransformPhase.DATATYPE);
		xNode.addTransformer(new ColorToStringTransformer(), StandardTransformPhase.DATATYPE);
		
		boolean support = xNode.isDataTypeSupported(new PreferenceKeyMetadata<>(SystemColor.class));
		
		assertTrue("Should have support.", support);
	}

}
