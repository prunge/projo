package au.net.causal.projo.prefs.transform;

import static org.junit.Assert.*;

import org.junit.Test;

import au.net.causal.projo.prefs.PreferenceKeyMetadata;
import au.net.causal.projo.prefs.PreferencesException;
import au.net.causal.projo.prefs.StandardTransformPhase;
import au.net.causal.projo.prefs.TransformerPreferenceNode;
import au.net.causal.projo.prefs.memory.InMemoryPreferenceNode;
import au.net.causal.projo.prefs.transform.CharacterToStringTransformer;
import au.net.causal.projo.prefs.transform.IntToStringTransformer;
import au.net.causal.projo.prefs.transform.StringToByteTransformer;

public class TestCharacterToStringTransformer
{
	@Test
	public void testPut()
	throws PreferencesException
	{
		InMemoryPreferenceNode<String> node = new InMemoryPreferenceNode<>(String.class);
		TransformerPreferenceNode xNode = new TransformerPreferenceNode(node);
		xNode.addTransformer(new CharacterToStringTransformer(), StandardTransformPhase.DATATYPE);
		xNode.addTransformer(new IntToStringTransformer(), StandardTransformPhase.DATATYPE);
		xNode.addTransformer(new StringToByteTransformer(), StandardTransformPhase.DATATYPE);
		
		xNode.putValue("val", '1', new PreferenceKeyMetadata<>(Character.class));
		
		String data = node.getNativeValue("val");
		assertEquals("Wrong value.", "1", data);
	}
	
	@Test
	public void testGet()
	throws PreferencesException
	{
		InMemoryPreferenceNode<String> node = new InMemoryPreferenceNode<>(String.class);
		node.putNativeValue("val", "1");
		
		TransformerPreferenceNode xNode = new TransformerPreferenceNode(node);
		xNode.addTransformer(new CharacterToStringTransformer(), StandardTransformPhase.DATATYPE);
		xNode.addTransformer(new IntToStringTransformer(), StandardTransformPhase.DATATYPE);
		xNode.addTransformer(new StringToByteTransformer(), StandardTransformPhase.DATATYPE);
		
		char result = xNode.getValue("val", new PreferenceKeyMetadata<>(Character.class));
		assertEquals("Wrong result.", '1', result);
	}
	
	@Test
	public void testRemove()
	throws PreferencesException
	{
		InMemoryPreferenceNode<String> node = new InMemoryPreferenceNode<>(String.class);
		node.putNativeValue("val", "1");
		
		TransformerPreferenceNode xNode = new TransformerPreferenceNode(node);
		xNode.addTransformer(new CharacterToStringTransformer(), StandardTransformPhase.DATATYPE);
		xNode.addTransformer(new IntToStringTransformer(), StandardTransformPhase.DATATYPE);
		xNode.addTransformer(new StringToByteTransformer(), StandardTransformPhase.DATATYPE);
		
		xNode.removeValue("val", new PreferenceKeyMetadata<>(Character.class));
		
		String data = node.getNativeValue("val");
		assertNull("Value should not exist.", data);
	}
	
	@Test
	public void testDataTypeSupport()
	throws PreferencesException
	{
		InMemoryPreferenceNode<String> node = new InMemoryPreferenceNode<>(String.class);
		TransformerPreferenceNode xNode = new TransformerPreferenceNode(node);
		xNode.addTransformer(new CharacterToStringTransformer(), StandardTransformPhase.DATATYPE);
		xNode.addTransformer(new IntToStringTransformer(), StandardTransformPhase.DATATYPE);
		xNode.addTransformer(new StringToByteTransformer(), StandardTransformPhase.DATATYPE);
		
		boolean support = xNode.isDataTypeSupported(new PreferenceKeyMetadata<>(Character.class));
		
		assertTrue("Should have support.", support);
	}
}
