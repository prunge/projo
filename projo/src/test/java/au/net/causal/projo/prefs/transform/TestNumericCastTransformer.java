package au.net.causal.projo.prefs.transform;

import static org.junit.Assert.*;

import java.math.BigInteger;

import org.junit.Test;

import au.net.causal.projo.prefs.PreferenceKeyMetadata;
import au.net.causal.projo.prefs.PreferencesException;
import au.net.causal.projo.prefs.StandardTransformPhase;
import au.net.causal.projo.prefs.TransformerPreferenceNode;
import au.net.causal.projo.prefs.memory.InMemoryPreferenceNode;
import au.net.causal.projo.prefs.transform.Base64Transformer;
import au.net.causal.projo.prefs.transform.IntToStringTransformer;
import au.net.causal.projo.prefs.transform.ListTransformer;
import au.net.causal.projo.prefs.transform.NumericCastTransformer;
import au.net.causal.projo.prefs.transform.RectangleTransformer;
import au.net.causal.projo.prefs.transform.StringToByteTransformer;

public class TestNumericCastTransformer
{
	@Test
	public void testPut()
	throws PreferencesException
	{
		InMemoryPreferenceNode<Long> node = new InMemoryPreferenceNode<>(Long.class);
		TransformerPreferenceNode xNode = new TransformerPreferenceNode(node);
		xNode.addTransformer(new NumericCastTransformer(), StandardTransformPhase.DATATYPE);
		xNode.addTransformer(new IntToStringTransformer(), StandardTransformPhase.DATATYPE);
		xNode.addTransformer(new StringToByteTransformer(), StandardTransformPhase.DATATYPE);
		xNode.addTransformer(new Base64Transformer(), StandardTransformPhase.DATATYPE);
		xNode.addTransformer(new RectangleTransformer(), StandardTransformPhase.DATATYPE);
		xNode.addTransformer(new ListTransformer(), StandardTransformPhase.DATATYPE);
		
		xNode.putValue("num", 22, new PreferenceKeyMetadata<>(Integer.class));
		
		Long nativeValue = node.getNativeValue("num");
		assertEquals("Wrong native value.", Long.valueOf(22L), nativeValue);
	}
	
	@Test
	public void testGet()
	throws PreferencesException
	{
		InMemoryPreferenceNode<Long> node = new InMemoryPreferenceNode<>(Long.class);
		node.putNativeValue("num", 22L);
		
		TransformerPreferenceNode xNode = new TransformerPreferenceNode(node);
		xNode.addTransformer(new NumericCastTransformer(), StandardTransformPhase.DATATYPE);
		xNode.addTransformer(new IntToStringTransformer(), StandardTransformPhase.DATATYPE);
		xNode.addTransformer(new StringToByteTransformer(), StandardTransformPhase.DATATYPE);
		xNode.addTransformer(new Base64Transformer(), StandardTransformPhase.DATATYPE);
		xNode.addTransformer(new RectangleTransformer(), StandardTransformPhase.DATATYPE);
		xNode.addTransformer(new ListTransformer(), StandardTransformPhase.DATATYPE);
		
		Integer result = xNode.getValue("num", new PreferenceKeyMetadata<>(Integer.class));
		assertEquals("Wrong result.", Integer.valueOf(22), result);
	}
	
	@Test
	public void testRemove()
	throws PreferencesException
	{
		InMemoryPreferenceNode<Long> node = new InMemoryPreferenceNode<>(Long.class);
		node.putNativeValue("num", 22L);
		
		TransformerPreferenceNode xNode = new TransformerPreferenceNode(node);
		xNode.addTransformer(new NumericCastTransformer(), StandardTransformPhase.DATATYPE);
		xNode.addTransformer(new IntToStringTransformer(), StandardTransformPhase.DATATYPE);
		xNode.addTransformer(new StringToByteTransformer(), StandardTransformPhase.DATATYPE);
		xNode.addTransformer(new Base64Transformer(), StandardTransformPhase.DATATYPE);
		xNode.addTransformer(new RectangleTransformer(), StandardTransformPhase.DATATYPE);
		xNode.addTransformer(new ListTransformer(), StandardTransformPhase.DATATYPE);
		
		xNode.removeValue("num", new PreferenceKeyMetadata<>(Integer.class));
		
		Long data = node.getNativeValue("num");
		assertNull("Value should not exist.", data);
	}
	
	@Test
	public void testDataTypeSupport()
	throws PreferencesException
	{
		InMemoryPreferenceNode<Long> node = new InMemoryPreferenceNode<>(Long.class);
		TransformerPreferenceNode xNode = new TransformerPreferenceNode(node);
		xNode.addTransformer(new NumericCastTransformer(), StandardTransformPhase.DATATYPE);
		xNode.addTransformer(new IntToStringTransformer(), StandardTransformPhase.DATATYPE);
		xNode.addTransformer(new StringToByteTransformer(), StandardTransformPhase.DATATYPE);
		xNode.addTransformer(new Base64Transformer(), StandardTransformPhase.DATATYPE);
		xNode.addTransformer(new RectangleTransformer(), StandardTransformPhase.DATATYPE);
		xNode.addTransformer(new ListTransformer(), StandardTransformPhase.DATATYPE);
		
		boolean bigIntSupport = xNode.isDataTypeSupported(new PreferenceKeyMetadata<>(BigInteger.class));
		boolean longSupport = xNode.isDataTypeSupported(new PreferenceKeyMetadata<>(Long.class));
		boolean intSupport = xNode.isDataTypeSupported(new PreferenceKeyMetadata<>(Integer.class));
		boolean helmutSupport = xNode.isDataTypeSupported(new PreferenceKeyMetadata<>(Short.class));
		boolean byteSupport = xNode.isDataTypeSupported(new PreferenceKeyMetadata<>(Byte.class));
		
		//Should support longs and smaller
		assertFalse("Wrong support.", bigIntSupport);
		assertTrue("Wrong support.", longSupport);
		assertTrue("Wrong support.", intSupport);
		assertTrue("Wrong support.", helmutSupport);
		assertTrue("Wrong support.", byteSupport);
	}

}
