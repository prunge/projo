package au.net.causal.projo.sample;

import au.net.causal.projo.annotation.Preference;
import au.net.causal.projo.annotation.PreferenceRoot;

@PreferenceRoot
public class MyPref
{
	@Preference
	private String name;
	
	@Preference
	private int age;
}
