package au.net.causal.projo.prefs.transform;

import static org.junit.Assert.*;

import java.io.File;

import org.junit.Test;

import au.net.causal.projo.prefs.PreferenceKeyMetadata;
import au.net.causal.projo.prefs.PreferencesException;
import au.net.causal.projo.prefs.StandardTransformPhase;
import au.net.causal.projo.prefs.TransformerPreferenceNode;
import au.net.causal.projo.prefs.memory.InMemoryPreferenceNode;
import au.net.causal.projo.prefs.transform.Base64Transformer;
import au.net.causal.projo.prefs.transform.FileToPathTransformer;
import au.net.causal.projo.prefs.transform.IntToStringTransformer;
import au.net.causal.projo.prefs.transform.ListTransformer;
import au.net.causal.projo.prefs.transform.NumericCastTransformer;
import au.net.causal.projo.prefs.transform.PathToStringTransformer;
import au.net.causal.projo.prefs.transform.RectangleTransformer;
import au.net.causal.projo.prefs.transform.StringToByteTransformer;

public class TestFileTransformer
{
	@Test
	public void testPutFile()
	throws PreferencesException
	{
		InMemoryPreferenceNode<String> node = new InMemoryPreferenceNode<>(String.class);
		TransformerPreferenceNode xNode = new TransformerPreferenceNode(node);
		xNode.addTransformer(new NumericCastTransformer(), StandardTransformPhase.DATATYPE);
		xNode.addTransformer(new IntToStringTransformer(), StandardTransformPhase.DATATYPE);
		xNode.addTransformer(new StringToByteTransformer(), StandardTransformPhase.DATATYPE);
		xNode.addTransformer(new Base64Transformer(), StandardTransformPhase.DATATYPE);
		xNode.addTransformer(new RectangleTransformer(), StandardTransformPhase.DATATYPE);
		xNode.addTransformer(new ListTransformer(), StandardTransformPhase.DATATYPE);
		xNode.addTransformer(new PathToStringTransformer(), StandardTransformPhase.DATATYPE);
		xNode.addTransformer(new FileToPathTransformer(), StandardTransformPhase.DATATYPE);
		
		File file = new File(File.separator + "tmp");
		xNode.putValue("dir", file, new PreferenceKeyMetadata<>(File.class));
		
		String nativeValue = node.getNativeValue("dir");
		assertEquals("Wrong native value.", File.separator + "tmp", nativeValue);
	}
	
	@Test
	public void testGetFile()
	throws PreferencesException
	{
		InMemoryPreferenceNode<String> node = new InMemoryPreferenceNode<>(String.class);
		node.putNativeValue("dir", File.separator + "tmp");
		
		TransformerPreferenceNode xNode = new TransformerPreferenceNode(node);
		xNode.addTransformer(new NumericCastTransformer(), StandardTransformPhase.DATATYPE);
		xNode.addTransformer(new IntToStringTransformer(), StandardTransformPhase.DATATYPE);
		xNode.addTransformer(new StringToByteTransformer(), StandardTransformPhase.DATATYPE);
		xNode.addTransformer(new Base64Transformer(), StandardTransformPhase.DATATYPE);
		xNode.addTransformer(new RectangleTransformer(), StandardTransformPhase.DATATYPE);
		xNode.addTransformer(new ListTransformer(), StandardTransformPhase.DATATYPE);
		xNode.addTransformer(new PathToStringTransformer(), StandardTransformPhase.DATATYPE);
		xNode.addTransformer(new FileToPathTransformer(), StandardTransformPhase.DATATYPE);
		
		File result = xNode.getValue("dir", new PreferenceKeyMetadata<>(File.class));
		assertEquals("Wrong result.", new File(File.separator + "tmp"), result);
	}
	
	@Test
	public void testRemove()
	throws PreferencesException
	{
		InMemoryPreferenceNode<String> node = new InMemoryPreferenceNode<>(String.class);
		node.putNativeValue("dir", File.separator + "tmp");
		
		TransformerPreferenceNode xNode = new TransformerPreferenceNode(node);
		xNode.addTransformer(new NumericCastTransformer(), StandardTransformPhase.DATATYPE);
		xNode.addTransformer(new IntToStringTransformer(), StandardTransformPhase.DATATYPE);
		xNode.addTransformer(new StringToByteTransformer(), StandardTransformPhase.DATATYPE);
		xNode.addTransformer(new Base64Transformer(), StandardTransformPhase.DATATYPE);
		xNode.addTransformer(new RectangleTransformer(), StandardTransformPhase.DATATYPE);
		xNode.addTransformer(new ListTransformer(), StandardTransformPhase.DATATYPE);
		xNode.addTransformer(new PathToStringTransformer(), StandardTransformPhase.DATATYPE);
		xNode.addTransformer(new FileToPathTransformer(), StandardTransformPhase.DATATYPE);
		
		xNode.removeValue("dir", new PreferenceKeyMetadata<>(File.class));
		
		String data = node.getNativeValue("dir");
		assertNull("Value should not exist.", data);
	}
	
	@Test
	public void testDataTypeSupport()
	throws PreferencesException
	{
		InMemoryPreferenceNode<String> node = new InMemoryPreferenceNode<>(String.class);
		TransformerPreferenceNode xNode = new TransformerPreferenceNode(node);
		xNode.addTransformer(new NumericCastTransformer(), StandardTransformPhase.DATATYPE);
		xNode.addTransformer(new IntToStringTransformer(), StandardTransformPhase.DATATYPE);
		xNode.addTransformer(new StringToByteTransformer(), StandardTransformPhase.DATATYPE);
		xNode.addTransformer(new Base64Transformer(), StandardTransformPhase.DATATYPE);
		xNode.addTransformer(new RectangleTransformer(), StandardTransformPhase.DATATYPE);
		xNode.addTransformer(new ListTransformer(), StandardTransformPhase.DATATYPE);
		xNode.addTransformer(new PathToStringTransformer(), StandardTransformPhase.DATATYPE);
		xNode.addTransformer(new FileToPathTransformer(), StandardTransformPhase.DATATYPE);
		
		boolean fileSupport = xNode.isDataTypeSupported(new PreferenceKeyMetadata<>(File.class));
		assertTrue("Wrong support.", fileSupport);
	}

}
