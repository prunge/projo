package au.net.causal.projo.prefs.transform;

import static org.junit.Assert.*;

import java.math.BigDecimal;

import org.junit.Test;

import au.net.causal.projo.prefs.PreferenceKeyMetadata;
import au.net.causal.projo.prefs.PreferencesException;
import au.net.causal.projo.prefs.StandardTransformPhase;
import au.net.causal.projo.prefs.TransformerPreferenceNode;
import au.net.causal.projo.prefs.memory.InMemoryPreferenceNode;
import au.net.causal.projo.prefs.transform.Base64Transformer;
import au.net.causal.projo.prefs.transform.FloatingPointCastTransformer;
import au.net.causal.projo.prefs.transform.IntToStringTransformer;
import au.net.causal.projo.prefs.transform.ListTransformer;
import au.net.causal.projo.prefs.transform.RectangleTransformer;
import au.net.causal.projo.prefs.transform.StringToByteTransformer;

public class TestFloatingPointCastTransformer
{
	private static final double DELTA = 1.0E-6;
	
	@Test
	public void testPut()
	throws PreferencesException
	{
		InMemoryPreferenceNode<Double> node = new InMemoryPreferenceNode<>(Double.class);
		TransformerPreferenceNode xNode = new TransformerPreferenceNode(node);
		xNode.addTransformer(new FloatingPointCastTransformer(), StandardTransformPhase.DATATYPE);
		xNode.addTransformer(new IntToStringTransformer(), StandardTransformPhase.DATATYPE);
		xNode.addTransformer(new StringToByteTransformer(), StandardTransformPhase.DATATYPE);
		xNode.addTransformer(new Base64Transformer(), StandardTransformPhase.DATATYPE);
		xNode.addTransformer(new RectangleTransformer(), StandardTransformPhase.DATATYPE);
		xNode.addTransformer(new ListTransformer(), StandardTransformPhase.DATATYPE);
		
		xNode.putValue("num", 22.0f, new PreferenceKeyMetadata<>(Float.class));
		
		Double nativeValue = node.getNativeValue("num");
		assertEquals("Wrong native value.", 22.0, nativeValue.doubleValue(), DELTA);
	}
	
	@Test
	public void testGet()
	throws PreferencesException
	{
		InMemoryPreferenceNode<Double> node = new InMemoryPreferenceNode<>(Double.class);
		node.putNativeValue("num", 22.0);
		
		TransformerPreferenceNode xNode = new TransformerPreferenceNode(node);
		xNode.addTransformer(new FloatingPointCastTransformer(), StandardTransformPhase.DATATYPE);
		xNode.addTransformer(new IntToStringTransformer(), StandardTransformPhase.DATATYPE);
		xNode.addTransformer(new StringToByteTransformer(), StandardTransformPhase.DATATYPE);
		xNode.addTransformer(new Base64Transformer(), StandardTransformPhase.DATATYPE);
		xNode.addTransformer(new RectangleTransformer(), StandardTransformPhase.DATATYPE);
		xNode.addTransformer(new ListTransformer(), StandardTransformPhase.DATATYPE);
		
		Float result = xNode.getValue("num", new PreferenceKeyMetadata<>(Float.class));
		assertEquals("Wrong result.", 22.0, result.doubleValue(), DELTA);
	}
	
	@Test
	public void testRemove()
	throws PreferencesException
	{
		InMemoryPreferenceNode<Double> node = new InMemoryPreferenceNode<>(Double.class);
		node.putNativeValue("num", 22.0);
		
		TransformerPreferenceNode xNode = new TransformerPreferenceNode(node);
		xNode.addTransformer(new FloatingPointCastTransformer(), StandardTransformPhase.DATATYPE);
		xNode.addTransformer(new IntToStringTransformer(), StandardTransformPhase.DATATYPE);
		xNode.addTransformer(new StringToByteTransformer(), StandardTransformPhase.DATATYPE);
		xNode.addTransformer(new Base64Transformer(), StandardTransformPhase.DATATYPE);
		xNode.addTransformer(new RectangleTransformer(), StandardTransformPhase.DATATYPE);
		xNode.addTransformer(new ListTransformer(), StandardTransformPhase.DATATYPE);
		
		xNode.removeValue("num", new PreferenceKeyMetadata<>(Float.class));
		
		Double data = node.getNativeValue("num");
		assertNull("Value should not exist.", data);
	}
	
	@Test
	public void testDataTypeSupport()
	throws PreferencesException
	{
		InMemoryPreferenceNode<Double> node = new InMemoryPreferenceNode<>(Double.class);
		TransformerPreferenceNode xNode = new TransformerPreferenceNode(node);
		xNode.addTransformer(new FloatingPointCastTransformer(), StandardTransformPhase.DATATYPE);
		xNode.addTransformer(new IntToStringTransformer(), StandardTransformPhase.DATATYPE);
		xNode.addTransformer(new StringToByteTransformer(), StandardTransformPhase.DATATYPE);
		xNode.addTransformer(new Base64Transformer(), StandardTransformPhase.DATATYPE);
		xNode.addTransformer(new RectangleTransformer(), StandardTransformPhase.DATATYPE);
		xNode.addTransformer(new ListTransformer(), StandardTransformPhase.DATATYPE);
		
		boolean bigDecimalSupport = xNode.isDataTypeSupported(new PreferenceKeyMetadata<>(BigDecimal.class));
		boolean doubleSupport = xNode.isDataTypeSupported(new PreferenceKeyMetadata<>(Double.class));
		boolean floatSupport = xNode.isDataTypeSupported(new PreferenceKeyMetadata<>(Float.class));
		
		//Should support longs and smaller
		assertFalse("Wrong support.", bigDecimalSupport);
		assertTrue("Wrong support.", doubleSupport);
		assertTrue("Wrong support.", floatSupport);
	}

}
