package au.net.causal.projo.prefs.properties;

import static org.junit.Assert.*;

import java.util.Properties;
import java.util.Set;

import org.junit.Test;

import com.google.common.collect.ImmutableSet;

import au.net.causal.projo.prefs.PreferenceKeyMetadata;
import au.net.causal.projo.prefs.PreferenceNode;
import au.net.causal.projo.prefs.PreferencesException;

public class TestPropertiesPreferenceNode
{
	@Test
	public void testRead()
	throws PreferencesException
	{
		Properties props = new Properties();
		props.setProperty("name", "John Galah");
		props.setProperty("age", "22");
		props.setProperty("shoeSize", "12");
		
		PropertiesPreferenceNode node = new PropertiesPreferenceNode(props);
		
		String value = node.getValue("name", new PreferenceKeyMetadata<>(String.class));
		
		assertEquals("Wrong value.", "John Galah", value);
	}

	@Test
	public void testWriteNewValue()
	throws PreferencesException
	{
		Properties props = new Properties();
		props.setProperty("name", "John Galah");
		props.setProperty("age", "22");
		props.setProperty("shoeSize", "12");
		
		PropertiesPreferenceNode node = new PropertiesPreferenceNode(props);
		node.putValue("petName", "Squinky", new PreferenceKeyMetadata<>(String.class));
		
		String value = props.getProperty("petName");
		
		assertEquals("Wrong value.", "Squinky", value);
	}
	
	@Test
	public void testReplaceValue()
	throws PreferencesException
	{
		Properties props = new Properties();
		props.setProperty("name", "John Galah");
		props.setProperty("age", "22");
		props.setProperty("shoeSize", "12");
		
		PropertiesPreferenceNode node = new PropertiesPreferenceNode(props);
		node.putValue("name", "Johnny Galah", new PreferenceKeyMetadata<>(String.class));
		
		String value = props.getProperty("name");
		
		assertEquals("Wrong value.", "Johnny Galah", value);
	}
	
	@Test
	public void testNestedRead()
	throws PreferencesException
	{
		Properties props = new Properties();
		props.setProperty("owner.name", "John Galah");
		props.setProperty("owner.age", "22");
		props.setProperty("pet.name", "Squinky");
		
		PreferenceNode node = new PropertiesPreferenceNode(props);
		node = node.getChildNode("owner");
		
		String value = node.getValue("name", new PreferenceKeyMetadata<>(String.class));
		
		assertEquals("Wrong value.", "John Galah", value);
	}
	
	@Test
	public void testNestedWrite()
	throws PreferencesException
	{
		Properties props = new Properties();
		props.setProperty("owner.name", "John Galah");
		props.setProperty("owner.age", "22");
		props.setProperty("pet.name", "Squinky");
		
		PreferenceNode node = new PropertiesPreferenceNode(props);
		node = node.getChildNode("owner");
		
		node.putValue("name", "Johnny Galah", new PreferenceKeyMetadata<>(String.class)); 
		
		String value = props.getProperty("owner.name");
		assertEquals("Wrong value.", "Johnny Galah", value);
	}
	
	@Test
	public void testGetNodeNames()
	throws PreferencesException
	{
		Properties props = new Properties();
		props.setProperty("owner.name", "John Galah");
		props.setProperty("owner.age", "22");
		props.setProperty("pet.name", "Squinky");
		props.setProperty("somethingElse", "blah");
		
		PreferenceNode node = new PropertiesPreferenceNode(props);
		Set<String> nodeNames = node.getNodeNames();
		
		assertEquals("Wrong node names.", ImmutableSet.of("owner", "pet"), nodeNames);
	}
	
	@Test
	public void testGetKeyNames()
	throws PreferencesException
	{
		Properties props = new Properties();
		props.setProperty("owner.name", "John Galah");
		props.setProperty("owner.age", "22");
		props.setProperty("pet.name", "Squinky");
		props.setProperty("somethingElse", "blah");
		
		PreferenceNode node = new PropertiesPreferenceNode(props);
		Set<String> keyNames = node.getKeyNames();
		
		assertEquals("Wrong node names.", ImmutableSet.of("somethingElse"), keyNames);
	}
	
	@Test
	public void testGetKeyNamesNested()
	throws PreferencesException
	{
		Properties props = new Properties();
		props.setProperty("owner.name", "John Galah");
		props.setProperty("owner.age", "22");
		props.setProperty("pet.name", "Squinky");
		props.setProperty("somethingElse", "blah");
		
		PreferenceNode node = new PropertiesPreferenceNode(props);
		node = node.getChildNode("owner");
		Set<String> keyNames = node.getKeyNames();
		
		assertEquals("Wrong node names.", ImmutableSet.of("age", "name"), keyNames);
	}
	
	@Test
	public void testRemoveValue()
	throws PreferencesException
	{
		Properties props = new Properties();
		props.setProperty("name", "John Galah");
		props.setProperty("age", "22");
		props.setProperty("shoeSize", "12");
		
		PropertiesPreferenceNode node = new PropertiesPreferenceNode(props);
		node.removeValue("shoeSize", new PreferenceKeyMetadata<>(String.class));
		
		String value = props.getProperty("shoeSize");
		
		assertNull("Should not have value.", value);
	}
	
	@Test
	public void testRemoveValueNested()
	throws PreferencesException
	{
		Properties props = new Properties();
		props.setProperty("person.name", "John Galah");
		props.setProperty("person.age", "22");
		props.setProperty("pet.name", "Squinky");
		
		PreferenceNode node = new PropertiesPreferenceNode(props);
		node = node.getChildNode("person");
		node.removeValue("name", new PreferenceKeyMetadata<>(String.class));
		
		String value = props.getProperty("person.name");
		
		assertNull("Should not have value.", value);
	}
	
	@Test
	public void testRemoveAllValues()
	throws PreferencesException
	{
		Properties props = new Properties();
		props.setProperty("name", "John Galah");
		props.setProperty("age", "22");
		props.setProperty("shoeSize", "12");
		
		PropertiesPreferenceNode node = new PropertiesPreferenceNode(props);
		node.removeAllValues();
		
		assertTrue("Should have no values.", props.isEmpty());
	}
	
	@Test
	public void testRemoveAllValuesNested()
	throws PreferencesException
	{
		Properties props = new Properties();
		props.setProperty("person.name", "John Galah");
		props.setProperty("person.age", "22");
		props.setProperty("pet.name", "Squinky");
		
		PreferenceNode node = new PropertiesPreferenceNode(props);
		node = node.getChildNode("person");
		node.removeAllValues();
		
		assertEquals("Should have one property.", 1, props.size());
		assertEquals("Property should still exist.", "Squinky", props.getProperty("pet.name"));
	}
	
	@Test
	public void testRemoveAllValuesNested2()
	throws PreferencesException
	{
		Properties props = new Properties();
		props.setProperty("person.name", "John Galah");
		props.setProperty("pet.name", "Squinky");
		props.setProperty("somethingElse", "yes");
		
		PreferenceNode node = new PropertiesPreferenceNode(props);
		node.removeAllValues();
		
		assertEquals("Should have one property.", 2, props.size());
		assertEquals("Property should still exist.", "Squinky", props.getProperty("pet.name"));
		assertEquals("Property should still exist.", "John Galah", props.getProperty("person.name"));
	}
	
	@Test
	public void testRemoveChildNode()
	throws PreferencesException
	{
		Properties props = new Properties();
		props.setProperty("person.name", "John Galah");
		props.setProperty("person.age", "22");
		props.setProperty("pet.name", "Squinky");
		
		PreferenceNode node = new PropertiesPreferenceNode(props);
		node.removeChildNode("person");
		
		assertEquals("Should have one property.", 1, props.size());
		assertEquals("Property should still exist.", "Squinky", props.getProperty("pet.name"));
	}
}
