package au.net.causal.projo.prefs.transform;

import static org.junit.Assert.*;

import java.lang.annotation.Annotation;

import org.jasypt.encryption.pbe.StandardPBEByteEncryptor;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import au.net.causal.projo.annotation.Secure;
import au.net.causal.projo.prefs.PreferenceKeyMetadata;
import au.net.causal.projo.prefs.PreferencesException;
import au.net.causal.projo.prefs.StandardTransformPhase;
import au.net.causal.projo.prefs.TransformerPreferenceNode;
import au.net.causal.projo.prefs.memory.InMemoryPreferenceNode;
import au.net.causal.projo.prefs.transform.Base64Transformer;
import au.net.causal.projo.prefs.transform.EncryptedValueTransformer;
import au.net.causal.projo.prefs.transform.IntToStringTransformer;
import au.net.causal.projo.prefs.transform.ListTransformer;
import au.net.causal.projo.prefs.transform.RectangleTransformer;
import au.net.causal.projo.prefs.transform.StringToByteTransformer;

import com.google.common.collect.ImmutableList;

public class TestEncryptedValueTransform
{
	private static final Logger log = LoggerFactory.getLogger(TestEncryptedValueTransform.class);
	
	@Test
	public void testEncryptAndDecrypt()
	throws PreferencesException
	{
		StandardPBEByteEncryptor encrypter = new StandardPBEByteEncryptor();
		encrypter.setPassword("secret");
		
		InMemoryPreferenceNode<String> node = new InMemoryPreferenceNode<>(String.class);
		TransformerPreferenceNode xNode = new TransformerPreferenceNode(node);
		xNode.addTransformer(new IntToStringTransformer(), StandardTransformPhase.DATATYPE);
		xNode.addTransformer(new StringToByteTransformer(), StandardTransformPhase.DATATYPE);
		xNode.addTransformer(new Base64Transformer(), StandardTransformPhase.DATATYPE);
		xNode.addTransformer(new RectangleTransformer(), StandardTransformPhase.DATATYPE);
		xNode.addTransformer(new ListTransformer(), StandardTransformPhase.DATATYPE);
		xNode.addTransformer(new EncryptedValueTransformer(encrypter), StandardTransformPhase.DATATYPE);
		
		xNode.putValue("name", "John Galah", new PreferenceKeyMetadata<>(ImmutableList.of(new SecureImpl()), String.class));
		
		log.info("Stored values: " + node);
		
		String value = xNode.getValue("name", new PreferenceKeyMetadata<>(ImmutableList.of(new SecureImpl()), String.class));
		assertEquals("Wrong value.", "John Galah", value);
	}
	
	@Test
	public void testDecryptWithWrongKey()
	throws PreferencesException
	{
		InMemoryPreferenceNode<String> node = new InMemoryPreferenceNode<>(String.class);

		//Store with the password 'secret'
		{
			StandardPBEByteEncryptor encrypter = new StandardPBEByteEncryptor();
			encrypter.setPassword("secret");
			
			TransformerPreferenceNode xNode = new TransformerPreferenceNode(node);
			xNode.addTransformer(new IntToStringTransformer(), StandardTransformPhase.DATATYPE);
			xNode.addTransformer(new StringToByteTransformer(), StandardTransformPhase.DATATYPE);
			xNode.addTransformer(new Base64Transformer(), StandardTransformPhase.DATATYPE);
			xNode.addTransformer(new RectangleTransformer(), StandardTransformPhase.DATATYPE);
			xNode.addTransformer(new ListTransformer(), StandardTransformPhase.DATATYPE);
			xNode.addTransformer(new EncryptedValueTransformer(encrypter), StandardTransformPhase.DATATYPE);
			xNode.putValue("name", "John Galah", new PreferenceKeyMetadata<>(ImmutableList.of(new SecureImpl()), String.class));
		}	
		
		//Attempt to read back with a different password
		{
			StandardPBEByteEncryptor encrypter = new StandardPBEByteEncryptor();
			encrypter.setPassword("notcorrect");
			
			TransformerPreferenceNode xNode = new TransformerPreferenceNode(node);
			xNode.addTransformer(new IntToStringTransformer(), StandardTransformPhase.DATATYPE);
			xNode.addTransformer(new StringToByteTransformer(), StandardTransformPhase.DATATYPE);
			xNode.addTransformer(new Base64Transformer(), StandardTransformPhase.DATATYPE);
			xNode.addTransformer(new RectangleTransformer(), StandardTransformPhase.DATATYPE);
			xNode.addTransformer(new ListTransformer(), StandardTransformPhase.DATATYPE);
			xNode.addTransformer(new EncryptedValueTransformer(encrypter), StandardTransformPhase.DATATYPE);
			try
			{
				xNode.getValue("name", new PreferenceKeyMetadata<>(ImmutableList.of(new SecureImpl()), String.class));
				fail("Should fail decryption due to bad password.");
			}
			catch (PreferencesException e)
			{
				//Should occur because decryption failed
			}
		}
	}
	
	//TODO there HAS to be a better way of creating annotation implementations on the fly
	private static class SecureImpl implements Secure
	{
		@Override
		public Class<? extends Annotation> annotationType()
		{
			return(Secure.class);
		}
	}
	
}
