package au.net.causal.projo.prefs.transform;

import static org.junit.Assert.*;

import java.awt.Font;
import java.awt.font.TextAttribute;
import java.util.Collections;

import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import au.net.causal.projo.prefs.PreferenceKeyMetadata;
import au.net.causal.projo.prefs.PreferencesException;
import au.net.causal.projo.prefs.StandardTransformPhase;
import au.net.causal.projo.prefs.TransformerPreferenceNode;
import au.net.causal.projo.prefs.memory.InMemoryPreferenceNode;
import au.net.causal.projo.prefs.transform.BooleanToStringTransformer;
import au.net.causal.projo.prefs.transform.FloatToStringTransformer;
import au.net.causal.projo.prefs.transform.FontTransformer;
import au.net.causal.projo.prefs.transform.IntToStringTransformer;
import au.net.causal.projo.prefs.transform.StringToByteTransformer;

public class TestFontTransformer
{
	private static final Logger log = LoggerFactory.getLogger(TestFontTransformer.class);
	
	private static final double DELTA = 1.0E-6;
	
	@Test
	public void testPut()
	throws PreferencesException
	{
		InMemoryPreferenceNode<String> node = new InMemoryPreferenceNode<>(String.class);
		TransformerPreferenceNode xNode = new TransformerPreferenceNode(node);
		xNode.addTransformer(FontTransformer.createSimpleFontTransformer(), StandardTransformPhase.DATATYPE);
		xNode.addTransformer(new IntToStringTransformer(), StandardTransformPhase.DATATYPE);
		xNode.addTransformer(new FloatToStringTransformer(), StandardTransformPhase.DATATYPE);
		xNode.addTransformer(new StringToByteTransformer(), StandardTransformPhase.DATATYPE);
		xNode.addTransformer(new BooleanToStringTransformer(), StandardTransformPhase.DATATYPE);
		
		Font font = new Font("Dialog", Font.PLAIN, 10);
		xNode.putValue("val", font, new PreferenceKeyMetadata<>(Font.class));
		
		log.info("Store: " + node);
		
		assertEquals("Wrong value.", "Dialog", node.getNativeValue("val.family"));
		assertEquals("Wrong value.", "10.0", node.getNativeValue("val.size"));
	}
	
	@Test
	public void testPutStyled()
	throws PreferencesException
	{
		InMemoryPreferenceNode<String> node = new InMemoryPreferenceNode<>(String.class);
		TransformerPreferenceNode xNode = new TransformerPreferenceNode(node);
		xNode.addTransformer(FontTransformer.createSimpleFontTransformer(), StandardTransformPhase.DATATYPE);
		xNode.addTransformer(new IntToStringTransformer(), StandardTransformPhase.DATATYPE);
		xNode.addTransformer(new FloatToStringTransformer(), StandardTransformPhase.DATATYPE);
		xNode.addTransformer(new StringToByteTransformer(), StandardTransformPhase.DATATYPE);
		xNode.addTransformer(new BooleanToStringTransformer(), StandardTransformPhase.DATATYPE);
		
		Font font = new Font("Dialog", Font.BOLD | Font.ITALIC, 10);
		xNode.putValue("val", font, new PreferenceKeyMetadata<>(Font.class));
		
		log.info("Store: " + node);
		
		assertEquals("Wrong value.", "Dialog", node.getNativeValue("val.family"));
		assertEquals("Wrong value.", "10.0", node.getNativeValue("val.size"));
		assertEquals("Wrong value.", String.valueOf(TextAttribute.WEIGHT_BOLD), node.getNativeValue("val.weight"));
		assertEquals("Wrong value.", String.valueOf(TextAttribute.POSTURE_OBLIQUE), node.getNativeValue("val.posture"));
	}
	
	@Test
	public void testPutStyled2()
	throws PreferencesException
	{
		InMemoryPreferenceNode<String> node = new InMemoryPreferenceNode<>(String.class);
		TransformerPreferenceNode xNode = new TransformerPreferenceNode(node);
		xNode.addTransformer(FontTransformer.createStandardFontTransformer(), StandardTransformPhase.DATATYPE);
		xNode.addTransformer(new IntToStringTransformer(), StandardTransformPhase.DATATYPE);
		xNode.addTransformer(new FloatToStringTransformer(), StandardTransformPhase.DATATYPE);
		xNode.addTransformer(new StringToByteTransformer(), StandardTransformPhase.DATATYPE);
		xNode.addTransformer(new BooleanToStringTransformer(), StandardTransformPhase.DATATYPE);
		
		Font font = new Font("Dialog", Font.BOLD | Font.ITALIC, 10);
		font = font.deriveFont(Collections.singletonMap(TextAttribute.UNDERLINE, TextAttribute.UNDERLINE_ON));
		font = font.deriveFont(Collections.singletonMap(TextAttribute.STRIKETHROUGH, TextAttribute.STRIKETHROUGH_ON));
		xNode.putValue("val", font, new PreferenceKeyMetadata<>(Font.class));
		
		log.info("Store: " + node);
		
		assertEquals("Wrong value.", "Dialog", node.getNativeValue("val.family"));
		assertEquals("Wrong value.", "10.0", node.getNativeValue("val.size"));
		assertEquals("Wrong value.", String.valueOf(TextAttribute.WEIGHT_BOLD), node.getNativeValue("val.weight"));
		assertEquals("Wrong value.", String.valueOf(TextAttribute.POSTURE_OBLIQUE), node.getNativeValue("val.posture"));
		assertEquals("Wrong value.", String.valueOf(TextAttribute.UNDERLINE_ON), node.getNativeValue("val.underline"));
		assertEquals("Wrong value.", "true", node.getNativeValue("val.strikethrough"));
	}
	
	@Test
	public void testGet()
	throws PreferencesException
	{
		InMemoryPreferenceNode<String> node = new InMemoryPreferenceNode<>(String.class);
		node.putNativeValue("val.family", "Dialog");
		node.putNativeValue("val.size", "11.0");
		
		TransformerPreferenceNode xNode = new TransformerPreferenceNode(node);
		xNode.addTransformer(FontTransformer.createSimpleFontTransformer(), StandardTransformPhase.DATATYPE);
		xNode.addTransformer(new IntToStringTransformer(), StandardTransformPhase.DATATYPE);
		xNode.addTransformer(new FloatToStringTransformer(), StandardTransformPhase.DATATYPE);
		xNode.addTransformer(new StringToByteTransformer(), StandardTransformPhase.DATATYPE);
		xNode.addTransformer(new BooleanToStringTransformer(), StandardTransformPhase.DATATYPE);
		
		Font result = xNode.getValue("val", new PreferenceKeyMetadata<>(Font.class));
		
		assertEquals("Wrong name.", "Dialog", result.getName());
		assertEquals("Wrong size.", 11.0, result.getSize2D(), DELTA);
	}
	
	@Test
	public void testRemove()
	throws PreferencesException
	{
		InMemoryPreferenceNode<String> node = new InMemoryPreferenceNode<>(String.class);
		node.putNativeValue("val.family", "Dialog");
		
		TransformerPreferenceNode xNode = new TransformerPreferenceNode(node);
		xNode.addTransformer(FontTransformer.createSimpleFontTransformer(), StandardTransformPhase.DATATYPE);
		xNode.addTransformer(new IntToStringTransformer(), StandardTransformPhase.DATATYPE);
		xNode.addTransformer(new FloatToStringTransformer(), StandardTransformPhase.DATATYPE);
		xNode.addTransformer(new StringToByteTransformer(), StandardTransformPhase.DATATYPE);
		xNode.addTransformer(new BooleanToStringTransformer(), StandardTransformPhase.DATATYPE);
		
		xNode.removeValue("val", new PreferenceKeyMetadata<>(Font.class));
		
		String data = node.getNativeValue("val.family");
		assertNull("Value should not exist.", data);
	}
	
	@Test
	public void testDataTypeSupport()
	throws PreferencesException
	{
		InMemoryPreferenceNode<String> node = new InMemoryPreferenceNode<>(String.class);
		TransformerPreferenceNode xNode = new TransformerPreferenceNode(node);
		xNode.addTransformer(FontTransformer.createSimpleFontTransformer(), StandardTransformPhase.DATATYPE);
		xNode.addTransformer(new IntToStringTransformer(), StandardTransformPhase.DATATYPE);
		xNode.addTransformer(new FloatToStringTransformer(), StandardTransformPhase.DATATYPE);
		xNode.addTransformer(new StringToByteTransformer(), StandardTransformPhase.DATATYPE);
		xNode.addTransformer(new BooleanToStringTransformer(), StandardTransformPhase.DATATYPE);
		
		boolean support = xNode.isDataTypeSupported(new PreferenceKeyMetadata<>(Font.class));
		
		assertTrue("Should have support.", support);
	}
}
