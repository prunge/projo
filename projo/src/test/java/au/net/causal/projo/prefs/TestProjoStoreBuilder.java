package au.net.causal.projo.prefs;

import static org.junit.Assert.*;

import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.Properties;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import au.net.causal.projo.annotation.Preference;
import au.net.causal.projo.annotation.Projo;
import au.net.causal.projo.prefs.properties.PropertiesPreferenceNode;

import com.google.common.collect.ImmutableMap;

/**
 * Unit tests for the {@link ProjoStoreBuilder} class.
 * 
 * @author prunge
 */
public class TestProjoStoreBuilder
{
	private static final Logger log = LoggerFactory.getLogger(TestProjoStoreBuilder.class);
	
	private static final double DELTA = 1.0E-6;
	
	@Test
	public void testBuildAndSave()
	throws Exception
	{
		Properties props = new Properties();
		PreferenceNode node = new PropertiesPreferenceNode(props);
		ProjoStore store = ProjoStoreBuilder.builder().defaults().node(node).build();
		store.setPreferenceRootUsed(false);
		
		MyPrefObj projo = new MyPrefObj("John Galah", 22, 5.0);
		store.save(projo);
		
		assertEquals("Wrong properties.",
				ImmutableMap.of("name", "John Galah", "age", "22", "rating", "5.0"),
				props);
	}
	
	@Test
	public void testBuildAndRead()
	throws Exception
	{
		Properties props = new Properties();
		props.setProperty("name", "John Galah");
		props.setProperty("age", "22");
		props.setProperty("rating", "5.5");
		
		PreferenceNode node = new PropertiesPreferenceNode(props);
		ProjoStore store = ProjoStoreBuilder.builder().defaults().node(node).build();
		store.setPreferenceRootUsed(false);
		
		MyPrefObj projo = store.read(MyPrefObj.class);
		log.info(ToStringBuilder.reflectionToString(projo, ToStringStyle.SHORT_PREFIX_STYLE));
		
		assertEquals("Wrong value.", "John Galah", projo.getName());
		assertEquals("Wrong value.", 22, projo.getAge());
		assertEquals("Wrong value.", 5.5, projo.getRating(), DELTA);
	}
	
	@Test
	public void testDates()
	throws Exception
	{
		Properties props = new Properties();
		props.setProperty("name", "John Galah");
		props.setProperty("dateOfBirth", "1977-01-09 14:20:02.000");
		
		PreferenceNode node = new PropertiesPreferenceNode(props);
		ProjoStore store = ProjoStoreBuilder.builder().defaults().node(node).build();
		store.setPreferenceRootUsed(false);
		
		MyPrefDateObj projo = store.read(MyPrefDateObj.class);
		log.info(ToStringBuilder.reflectionToString(projo, ToStringStyle.SHORT_PREFIX_STYLE));
		
		assertEquals("Wrong value.", "John Galah", projo.getName());
		assertEquals("Wrong value.", new GregorianCalendar(1977, Calendar.JANUARY, 9, 14, 20, 2).getTime(), projo.getDateOfBirth());
	}
	
	@Test
	public void testCalendars()
	throws Exception
	{
		Properties props = new Properties();
		props.setProperty("name", "John Galah");
		props.setProperty("dateOfBirth", "1977-01-09 14:20:02.000");
		
		PreferenceNode node = new PropertiesPreferenceNode(props);
		ProjoStore store = ProjoStoreBuilder.builder().defaults().node(node).build();
		store.setPreferenceRootUsed(false);
		
		MyPrefCalendarObj projo = store.read(MyPrefCalendarObj.class);
		log.info(ToStringBuilder.reflectionToString(projo, ToStringStyle.SHORT_PREFIX_STYLE));
		
		assertEquals("Wrong value.", "John Galah", projo.getName());
		assertEquals("Wrong value.", new GregorianCalendar(1977, Calendar.JANUARY, 9, 14, 20, 2), projo.getDateOfBirth());
	}
	
	@Projo
	public static class MyPrefObj
	{
		@Preference
		private String name;
		
		@Preference
		private int age;
		
		@Preference
		private double rating;

		public MyPrefObj()
		{
			this(null, 0, Double.NaN);
		}
		
		public MyPrefObj(String name, int age, double rating)
		{
			super();
			this.name = name;
			this.age = age;
			this.rating = rating;
		}

		public String getName()
		{
			return(name);
		}

		public void setName(String name)
		{
			this.name = name;
		}

		public int getAge()
		{
			return(age);
		}

		public void setAge(int age)
		{
			this.age = age;
		}

		public double getRating()
		{
			return(rating);
		}

		public void setRating(double rating)
		{
			this.rating = rating;
		}
	}
	
	@Projo
	public static class MyPrefDateObj
	{
		@Preference
		private String name;
		
		@Preference
		private Date dateOfBirth;
		
		public MyPrefDateObj()
		{
			this(null, null);
		}
		
		public MyPrefDateObj(String name, Date dateOfBirth)
		{
			super();
			this.name = name;
			this.dateOfBirth = dateOfBirth;
		}

		public String getName()
		{
			return(name);
		}

		public void setName(String name)
		{
			this.name = name;
		}

		public Date getDateOfBirth()
		{
			return(dateOfBirth);
		}

		public void setDateOfBirth(Date dateOfBirth)
		{
			this.dateOfBirth = dateOfBirth;
		}
	}
	
	@Projo
	public static class MyPrefCalendarObj
	{
		@Preference
		private String name;
		
		@Preference
		private Calendar dateOfBirth;
		
		public MyPrefCalendarObj()
		{
			this(null, null);
		}
		
		public MyPrefCalendarObj(String name, Calendar dateOfBirth)
		{
			super();
			this.name = name;
			this.dateOfBirth = dateOfBirth;
		}

		public String getName()
		{
			return(name);
		}

		public void setName(String name)
		{
			this.name = name;
		}

		public Calendar getDateOfBirth()
		{
			return(dateOfBirth);
		}

		public void setDateOfBirth(Calendar dateOfBirth)
		{
			this.dateOfBirth = dateOfBirth;
		}
	}

}
