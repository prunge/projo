package au.net.causal.projo.prefs;

/**
 * A chain allows transformers to call other transformers indirectly. A single transformer node call might result in a chain of transformers being called.
 * 
 * @author prunge
 */
public interface TransformPutChain extends TransformDataTypeSupportChain
{
	/**
	 * Asks the native store and other transformers to store a value for a key.
	 * 
	 * @param key the key name.
	 * @param value the value to store.  May be null.
	 * @param keyMetadata the key metadata.
	 * 
	 * @throws NullPointerException if <code>key</code> or <code>keyMetadata</code> is null.
	 * @throws UnsupportedDataTypeException if the key metadata was not supported by the native store and no other transformers could transform it.
	 * @throws PreferencesException if an error occurs in the store or another transform.
	 */
	public <T> void putValue(String key, T value, PreferenceKeyMetadata<T> keyMetadata)
	throws UnsupportedDataTypeException, PreferencesException;

	/**
	 * Asks other transformers to store a value for a key using a custom endpoint in place of the native store.
	 * <p>
	 * 
	 * This can be useful when other keys need to be written to the underlying store, but a transform has to happen first.  In this case, the custom 
	 * implementation of the new endpoint can filter through to the underlying store or original chain, apply any transformation as needed and then 
	 * use other transformers in the chain with this modified data.
	 * 
	 * @param key the key name.
	 * @param value the value to store.  May be null.
	 * @param keyMetadata the key metadata.
	 * @param newEndpoint the custom endpoint to use for other transforms writing data.
	 * 
	 * @throws NullPointerException if <code>key</code>, <code>keyMetadata</code> or <code>newEndpoint</code> is null.
	 * @throws UnsupportedDataTypeException if the key metadata was not supported by the native store and no other transformers could transform it.
	 * @throws PreferencesException if an error occurs in the store or another transform.
	 */
	public <T> void putValueWithRestartedChain(String key, T value, PreferenceKeyMetadata<T> keyMetadata, TransformPutChain newEndpoint)
	throws UnsupportedDataTypeException, PreferencesException;
}
