package au.net.causal.projo.prefs;

/**
 * Defines a phase that preference transforms may be executed under.
 * <p>
 * 
 * Typically, the {@linkplain StandardTransformPhase standard phases} will be adequate for new transforms.  However, it is possible to develop a custom phase by
 * implementing this interface and registering it with a tranform node.
 *  
 * @author prunge
 */
public interface TransformPhase
{
	
}
