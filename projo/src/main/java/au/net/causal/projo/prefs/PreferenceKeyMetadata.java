package au.net.causal.projo.prefs;

import java.lang.annotation.Annotation;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import au.net.causal.projo.annotation.Secure;
import au.net.causal.projo.bind.AnnotatedMetadata;

import com.google.common.reflect.TypeToken;

/**
 * Defines the type of data stored under a key.  Key metadata is used by Projo to determine what transforms to apply to data, and how the data must end up being
 * stored in the preference store.
 * <p>
 * 
 * Typically the data type is the most important information when storing preference data, but additional annotations might alter how data is stored, such as
 * {@literal @}{@link Secure}.
 * <p>
 * 
 * Data type is stored as a {@link TypeToken} to allow generic type information to be stored, such as the component type when a list is used.
 * To get raw type information, use {@link TypeToken#getRawType()}.  When constructing key metadata, use generic information wherever possible, since Projo
 * will not be able to do as much with raw collections as it can with properly typed collections.
 * <p>
 * 
 * Preference key metadata is immutable.  However, new metadata can be derived from this one using the <code>with...</code> family of methods. 
 * 
 * @author prunge
 *
 * @param <T> the data type.
 */
public class PreferenceKeyMetadata<T> extends AnnotatedMetadata
{
	private final TypeToken<T> dataType;
	
	/**
	 * Creates <code>PreferenceKeyMetadata</code> with a raw type and no annotations.
	 * 
	 * @param dataType the raw type.
	 * 
	 * @throws NullPointerException if <code>dataType</code> is null.
	 */
	public PreferenceKeyMetadata(Class<T> dataType)
	{
		this(TypeToken.of(dataType));
	}
	
	/**
	 * Creates <code>PreferenceKeyMetadata</code> with a generic type and no annotations.
	 * 
	 * @param dataType the data type.
	 * 
	 * @throws NullPointerException if <code>dataType</code> is null.
	 */
	public PreferenceKeyMetadata(TypeToken<T> dataType)
	{
		this(Collections.<Annotation>emptyList(), dataType);
	}
	
	/**
	 * Creates <code>PreferenceKeyMetadata</code> with a raw type and the specified annotations.
	 * 
	 * @param annotations annotations for the preference key.
	 * @param dataType the data type.
	 * 
	 * @throws NullPointerException if any parameter is null.
	 */
	public PreferenceKeyMetadata(Iterable<? extends Annotation> annotations, Class<T> dataType)
	{
		this(annotations, TypeToken.of(dataType));
	}
	
	/**
	 * Creates <code>PreferenceKeyMetadata</code> with a generic type and the specified annotations.
	 * 
	 * @param annotations annotations for the preference key.
	 * @param dataType the data type.
	 * 
	 * @throws NullPointerException if any parameter is null.
	 */
	public PreferenceKeyMetadata(Iterable<? extends Annotation> annotations, TypeToken<T> dataType)
	{
		super(annotations);
		
		if (dataType == null)
			throw new NullPointerException("dataType == null");
		
		this.dataType = dataType;
	}

	/**
	 * Returns the data type of the preference key.
	 * <p>
	 * 
	 * This data type contains generic information.  To access the raw type, use {@link TypeToken#getRawType()}.
	 * 
	 * @return the preference key data type.
	 */
	public TypeToken<T> getDataType()
	{
		return(dataType);
	}
	
	/**
	 * Creates new preference key metadata derived from this one but with a different data type.
	 * 
	 * @param newDataType the data type of the new metadata.
	 * 
	 * @return the created key metadata.
	 * 
	 * @throws NullPointerException if <code>newDataType</code> is null.
	 */
	public <U> PreferenceKeyMetadata<U> withDataType(TypeToken<U> newDataType)
	{
		if (newDataType == null)
			throw new NullPointerException("newDataType == null");
		
		return(new PreferenceKeyMetadata<>(getAnnotations(), newDataType));
	}
	
	/**
	 * Creates new preference key metadata derived from this one but with a different raw data type.
	 * 
	 * @param newDataType the data type of the new metadata.
	 * 
	 * @return the created key metadata.
	 * 
	 * @throws NullPointerException if <code>newDataType</code> is null.
	 */
	public <U> PreferenceKeyMetadata<U> withDataType(Class<U> newDataType)
	{
		if (newDataType == null)
			throw new NullPointerException("newDataType == null");
		
		return(withDataType(TypeToken.of(newDataType)));
	}
	
	/**
	 * Creates new preference key metadata derived from this one but with any annotations of type <code>annotationType</code> excluded.
	 * 
	 * @param annotationType the annotation type to exclude.
	 * 
	 * @return the created key metadata.
	 * 
	 * @throws NullPointerException if <code>annotationType</code> is null.
	 */
	public PreferenceKeyMetadata<T> withoutAnnotationType(Class<? extends Annotation> annotationType)
	{
		List<Annotation> annotations = new ArrayList<>();
		for (Annotation annotation : getAnnotations())
		{
			if (!annotationType.isInstance(annotation))
				annotations.add(annotation);
		}
		
		return(new PreferenceKeyMetadata<>(annotations, getDataType()));
	}

	@Override
	public int hashCode()
	{
		final int prime = 31;
		int result = 1;
		result = prime * result + dataType.hashCode();
		result = prime * result + getAnnotations().hashCode();
		return result;
	}

	/**
	 * Compares this key metadata with another object for equality.
	 * <p>
	 * 
	 * Key metadata is equal if its annotations and data types are equal.
	 * 
	 * @param obj the object to compare with.
	 * 
	 * @return true if equal, false if not.
	 */
	@Override
	public boolean equals(Object obj)
	{
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		PreferenceKeyMetadata<?> other = (PreferenceKeyMetadata<?>) obj;
		if (!dataType.equals(other.dataType))
			return false;
		if (!getAnnotations().equals(other.getAnnotations()))
			return false;
		return true;
	}
	
	
}
