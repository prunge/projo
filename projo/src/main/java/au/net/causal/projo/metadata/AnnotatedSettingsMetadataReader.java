package au.net.causal.projo.metadata;

import java.lang.annotation.Annotation;
import java.util.Set;

import au.net.causal.projo.annotation.Preference;
import au.net.causal.projo.annotation.Projo;
import au.net.causal.projo.bind.AnnotatedEntityMetadataReader;
import au.net.causal.projo.bind.EntityMetadataException;

import com.google.common.collect.ImmutableSet;
import com.google.common.collect.Sets;
import com.google.common.reflect.TypeToken;

public class AnnotatedSettingsMetadataReader extends AnnotatedEntityMetadataReader<SettingsEntityMetadata, SettingsPropertyMetadata>
implements SettingsMetadataReader
{
	private static final Set<? extends Class<? extends Annotation>> SETTINGS_ANNOTATION_TYPES = ImmutableSet.<Class<? extends Annotation>>builder()
																									.add(Preference.class)
																									.build();
	
	@Override
	protected SettingsEntityMetadata createEntityMetadata(Class<?> entityType, Iterable<? extends Annotation> annotations) throws EntityMetadataException
	{
		if (!isProjoEntity(annotations))
			return(null);
		
		SettingsEntityMetadata entity = new SettingsEntityMetadata(entityType, annotations);
		
		return(entity);
	}
	
	private boolean isProjoEntity(Iterable<? extends Annotation> annotations)
	{
		//Is this an entity?
		for (Annotation annotation : annotations)
		{
			if (Projo.class.equals(annotation.annotationType()))
				return(true);
		}
		
		return(false);
	}

	@Override
	protected SettingsPropertyMetadata createPropertyMetadata(PropertyPrototype<SettingsEntityMetadata, SettingsPropertyMetadata> prototype)
	throws EntityMetadataException
	{
		//Is this a property we are interested in?
		if (Sets.intersection(prototype.getAnnotationTypes(), SETTINGS_ANNOTATION_TYPES).isEmpty())
			return(null);
			
		//Is this a nested entity?
		Class<?> possibleEntityType = TypeToken.of(prototype.getPropertyType()).getRawType();
		SettingsEntityMetadata nestedProjoMetadata = read(possibleEntityType);		
		SettingsPropertyMetadata property = new SettingsPropertyMetadata(prototype.getName(), 
													prototype.getPropertyType(), 
													prototype.getParent(), 
													prototype.getAccessor(), 
													prototype.getAnnotations(),
													nestedProjoMetadata);
		
		return(property);
	}
}
