package au.net.causal.projo.prefs.transform;

import au.net.causal.projo.annotation.Secure;
import au.net.causal.projo.prefs.DataTypeSupport;
import au.net.causal.projo.prefs.PreferenceKeyMetadata;
import au.net.causal.projo.prefs.PreferencesException;
import au.net.causal.projo.prefs.TransformDataTypeSupportChain;
import au.net.causal.projo.prefs.TransformGetChain;
import au.net.causal.projo.prefs.TransformPutChain;
import au.net.causal.projo.prefs.TransformRemoveChain;
import au.net.causal.projo.prefs.TransformResult;

/**
 * A preference transformer transforms preference calls so that an underlying store may be used to store or retrieve values even if it does not have 
 * native support for a particular data type.  For example, a typical transformer might convert between {@link Integer}s and {@link String}s so that stores
 * that only support string retrieval and storage can support integers as well.
 * <p>
 * 
 * When storing or retrieving values, multiple transformers might be called in a chain.  For example, for a store that only has native support for 
 * byte arrays, to support the integer data type two transforms may be used: one to convert integer to string, and another to convert string to byte array.
 * <p>
 * 
 * Usually the {@linkplain PreferenceKeyMetadata#getDataType() data type} of a key is what determines the transformation.  However, sometimes annotations might
 * affect this as well - a good example is the {@literal @}{@link Secure} annotation.
 * <p>
 * 
 * Transformers should only apply themselves when needed.  For example, a transformer that converts integers to strings should not apply itself if the underlying
 * store has native support for the integer data type.
 * 
 * @author prunge
 */
public interface PreferenceTransformer
{
	/**
	 * Applies this transformer for a preference retrieval operation.
	 * 
	 * @param key the key name.
	 * @param keyMetadata metadata for the key.
	 * @param chain the transform chain.  Can be used to call onto other transforms.
	 * 
	 * @return <code>null</code> if this transform has not applied, or the wrapped retrieved value if it did apply.
	 * 
	 * @throws PreferencesException if an error occurs with the transform.
	 */
	public <T> TransformResult<T> applyGet(String key, PreferenceKeyMetadata<T> keyMetadata, TransformGetChain chain)
	throws PreferencesException;
	
	/**
	 * Applies this transformer for a preference storage operation.
	 * Returns true if the transform has applied.
	 * 
	 * @param key the key name.
	 * @param value the value to store.  May be null.
	 * @param keyMetadata metadata for the key.
	 * @param chain the transform chain.  Can be used to call onto other transforms.
	 * 
	 * @return true if the transform actually applied, false if not.
	 * 
	 * @throws PreferencesException if an error occurs with the transform.
	 */
	public <T> boolean applyPut(String key, T value, PreferenceKeyMetadata<T> keyMetadata, TransformPutChain chain)
	throws PreferencesException;
	
	/**
	 * Applies this transformer for a preference value removal operation.
	 * 
	 * @param key the key name.
	 * @param keyMetadata metadata for the key.
	 * @param chain the transform chain.  Can be used to call onto other transforms.
	 * 
	 * @return true if the transform actually applied, false if not.
	 * 
	 * @throws PreferencesException if an error occurs with the transform.
	 */
	public <T> boolean applyRemove(String key, PreferenceKeyMetadata<T> keyMetadata, TransformRemoveChain chain)
	throws PreferencesException;
	
	/**
	 * Applies this transformer for a data type support query operation.
	 * 
	 * @param keyMetadata metadata for the key.
	 * @param chain the transform chain.  Can be used to call onto other transforms.
	 * 
	 * @return the type of data type support supplied by this transform, or <code>null</code> if the transform did not apply.
	 * 
	 * @throws PreferencesException if an error occurs with the transform.
	 */
	public DataTypeSupport applyDataTypeSupport(PreferenceKeyMetadata<?> keyMetadata, TransformDataTypeSupportChain chain)
	throws PreferencesException;
}
