package au.net.causal.projo.prefs;

/**
 * Defines whether a data type is supported by a transformer, or if a data type's support is blocked by a transformer.
 * 
 * @author prunge
 */
public enum DataTypeSupport
{
	/**
	 * The transformer adds support for the data type.
	 */
	ADD_SUPPORT,
	
	/**
	 * The transformer blocks support for the data type.  This data type will not be supported even if other transformers declare support for it.
	 */
	BLOCK_SUPPORT;
}
