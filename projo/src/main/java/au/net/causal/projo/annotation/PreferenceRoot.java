package au.net.causal.projo.annotation;

import java.lang.annotation.ElementType;
import java.lang.annotation.Inherited;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * Defines a preference POJO as a root.  Preference roots can be loaded from a preference store without any qualifying name.
 * 
 * @author prunge
 */
@Inherited
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.TYPE)
public @interface PreferenceRoot
{
	/**
	 * The name of the preference node to load from.  If not specified, the default is to use the fully qualified name of the preference POJO class.
	 */
	public String name() default "";
}
