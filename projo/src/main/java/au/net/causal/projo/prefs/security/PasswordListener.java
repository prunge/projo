package au.net.causal.projo.prefs.security;

import java.util.EventListener;

/**
 * Listener that is notified of user actions in a {@link JPasswordPane}.
 * 
 * @author prunge
 */
public interface PasswordListener extends EventListener
{
	/**
	 * Called when the user enters and confirms a password.
	 * 
	 * @param event the event.
	 */
	public void passwordEntered(PasswordEnteredEvent event);
	
	/**
	 * Called when the user cancels entering a password.
	 * 
	 * @param event the event.
	 */
	public void passwordCancelled(PasswordEvent event);
}
