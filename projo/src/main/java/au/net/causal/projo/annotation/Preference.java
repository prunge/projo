package au.net.causal.projo.annotation;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * Defines a preference value.  Only properties marked with this annotation will be loaded from and saved to preference stores.
 * 
 * @author prunge
 */
@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.FIELD, ElementType.METHOD})
public @interface Preference
{
	/**
	 * The preference key.  By default, the key name is derived from the property.
	 */
	public String key() default "";
}
