package au.net.causal.projo.prefs.transform;

import java.util.Locale;

import au.net.causal.projo.prefs.PreferencesException;

/**
 * Converts between {@link Locale} values and strings by using the locale's {@linkplain Locale#toLanguageTag() language tag}.
 * 
 * @author prunge
 */
public class LocaleToStringTransformer extends GenericToStringTransformer<Locale>
{
	public LocaleToStringTransformer()
	{
		super(Locale.class);
	}

	@Override
	protected String valueToString(Locale value) throws PreferencesException
	{
		if (value == null)
			return(null);
		
		return(value.toLanguageTag());
	}

	@Override
	protected Locale stringToValue(String s) throws PreferencesException
	{
		if (s == null)
			return(null);
		if (s.isEmpty())
			return(null);
		
		return(Locale.forLanguageTag(s));
	}
}
