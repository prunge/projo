package au.net.causal.projo.prefs.transform;

import au.net.causal.projo.prefs.PreferencesException;

import com.google.common.io.BaseEncoding;

/**
 * Converts byte[] data to String data using base64 (or a customized) encoding.
 * 
 * @author prunge
 */
public class Base64Transformer extends GenericToStringTransformer<byte[]>
{
	private final BaseEncoding encoding;
	
	/**
	 * Creates a <code>Base64Transformer</code> that uses the specified base encoding.
	 * 
	 * @param encoding the base encoding to use.
	 * 
	 * @throws NullPointerException if <code>encoding</code> is null.
	 */
	public Base64Transformer(BaseEncoding encoding)
	{
		super(byte[].class);
		
		if (encoding == null)
			throw new NullPointerException("encoding == null");
		
		this.encoding = encoding;
	}
	
	/**
	 * Creates a <code>Base64Transformer</code> using standard Base64 encoding.
	 */
	public Base64Transformer()
	{
		this(BaseEncoding.base64());
	}
	
	@Override
	protected byte[] stringToValue(String s) throws PreferencesException
	{
		try
		{
			return(encoding.decode(s));
		}
		catch (IllegalArgumentException e)
		{
			throw new PreferencesException("Failed to decode base64 value '" + s + "'.", e);
		}
	}
	
	@Override
	protected String valueToString(byte[] value) throws PreferencesException
	{
		return(encoding.encode(value));
	}
}
