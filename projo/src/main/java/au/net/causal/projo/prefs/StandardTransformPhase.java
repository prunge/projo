package au.net.causal.projo.prefs;

/**
 * Standard transform phases.
 * 
 * @author prunge
 */
public enum StandardTransformPhase implements TransformPhase
{
	/**
	 * Transforms preference datatypes into ones that the backing store or metadata supports.
	 * <p>
	 * 
	 * For example, if the backing store only supports string values, other data types such as Integer will be converted in this phase.
	 */
	DATATYPE,
	
	/**
	 * Executed before the {@link #DATATYPE} phase.
	 */
	PRE_DATATYPE,
	
	/**
	 * Executed after the {@link #DATATYPE} phase.
	 */
	POST_DATATYPE,
	
	/**
	 * Executed after other phases, used to validate values going into the store are valid for that store.
	 */
	STORE_VALIDATION;
}
