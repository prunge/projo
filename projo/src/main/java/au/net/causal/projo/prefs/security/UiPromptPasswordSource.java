package au.net.causal.projo.prefs.security;

/**
 * A password source that displays some kind of UI prompt to the user.  The prompt text can be configured.
 * 
 * @author prunge
 */
public interface UiPromptPasswordSource extends PasswordSource
{
	/**
	 * Returns the password prompt text.
	 * 
	 * @return the password prompt text.
	 */
	public String getPasswordPrompt();
	
	/**
	 * Sets the password prompt text.
	 * 
	 * @param prompt the text to set.
	 */
	public void setPasswordPrompt(String prompt);
}
