package au.net.causal.projo.prefs;

import com.google.common.reflect.TypeToken;

/**
 * Thrown when an attempt is made to read or write a data type to a preference node that the preference node does not support.
 * 
 * @author prunge
 */
public class UnsupportedDataTypeException extends PreferencesException
{
	private final TypeToken<?> unsupportedType;
	
	/**
	 * Creates a <code>UnsupportedDataTypeException</code> with a standard message.
	 * 
	 * @param unsupportedType the type that was not supported by the preference node.
	 */
	public UnsupportedDataTypeException(TypeToken<?> unsupportedType)
	{
		this(unsupportedType, unsupportedType.toString());
	}
	
	/**
	 * Creates a <code>UnsupportedDataTypeException</code> with the specified detail message.
	 * 
	 * @param unsupportedType the type that was not supported by the preference node.
	 * @param message the detail message.
	 */
	public UnsupportedDataTypeException(TypeToken<?> unsupportedType, String message)
	{
		this(unsupportedType, message, null);
	}
	
	/**
	 * Creates a <code>UnsupportedDataTypeException</code> with a standard detail message and the specified cause exception.
	 * 
	 * @param unsupportedType the type that was not supported by the preference node.
	 * @param cause the cause exception.
	 */
	public UnsupportedDataTypeException(TypeToken<?> unsupportedType, Throwable cause)
	{
		this(unsupportedType, unsupportedType.toString(), cause);
	}
	
	/**
	 * Creates a <code>UnsupportedDataTypeException</code> with the specified detail message and cause exception.
	 * 
	 * @param unsupportedType the type that was not supported by the preference node.
	 * @param message the detail message.
	 * @param cause the cause exception.
	 */
	public UnsupportedDataTypeException(TypeToken<?> unsupportedType, String message, Throwable cause)
	{
		super(message, cause);
		this.unsupportedType = unsupportedType;
	}
	
	/**
	 * Returns the type that was not supported by the preference node.
	 * 
	 * @return the unsupported type.
	 */
	public TypeToken<?> getUnsupportedType()
	{
		return(unsupportedType);
	}
}
