/**
 * An implementation of Projo preferences that uses the standard Java Preferences API.
 * 
 * @author prunge
 */
package au.net.causal.projo.prefs.javapreferences;