package au.net.causal.projo.prefs;

import java.util.Set;
import java.util.prefs.Preferences;

/**
 * A node in a tree of preferences that can store preference data under keys.  Preference values are stored under string keys.
 * <p>
 * 
 * <code>PreferenceNode</code> works similarly to the standard {@link Preferences} API of Java, but has the following differences:
 * <ul>
 * 	<li>Arbitrary data types for retrieving and storing data may be supported.  {@link PreferenceKeyMetadata} is used to determine how the data is stored.</li>
 * 	<li>Data type support for a particular preference store can be queried through {@link #isDataTypeSupported(PreferenceKeyMetadata)}</li>
 * </ul>
 * <p>
 * 
 * Data type handling and conversion is implementation specific.  Storing a value, say, as a string and retreiving it as an integer might result in a conversion
 * being performed by the preference store or might result in a null value being returned.  An implementation might even have different spaces for different 
 * data types, allowing the same key name to store multiple values, each of a different data type.
 * 
 * @author prunge
 */
public interface PreferenceNode extends PreferenceStoreMetadata, AutoCloseable
{
	/**
	 * Retrieves a value stored under the specified key.
	 * 
	 * @param key the key.
	 * @param metadata metadata for the key, including data type and annotations.
	 * 
	 * @return the value stored under the specified key, or <code>null</code> if store node does not contain the key or a value.
	 * 
	 * @throws NullPointerException if <code>key</code> or <code>metadata</code> is null.
	 * @throws UnsupportedDataTypeException if the key metadata is not supported by the preference store.
	 * @throws PreferencesException if an error occurs accessing the preference store.
	 */
	public <T> T getValue(String key, PreferenceKeyMetadata<T> metadata)
	throws UnsupportedDataTypeException, PreferencesException;
	
	/**
	 * Stores a value under the specified key.
	 * 
	 * @param key the key.
	 * @param value the value to store under the key.  May be <code>null</code>.
	 * @param metadata metadata for the key, including data type and annotations.
	 * 
	 * @throws NullPointerException if <code>key</code> or <code>metadata</code> is null.
	 * @throws UnsupportedDataTypeException if the key metadata is not supported by the preference store.
	 * @throws PreferencesException if an error occurs accessing the preference store.
	 */
	public <T> void putValue(String key, T value, PreferenceKeyMetadata<T> metadata)
	throws UnsupportedDataTypeException, PreferencesException;
	
	/**
	 * Removes the value stored under the specified key.
	 * 
	 * @param key the key.
	 * @param metadata metadata for the key, including data type and annotations.
	 * 
	 * @throws NullPointerException if <code>key</code> or <code>metadata</code> is null.
	 * @throws UnsupportedDataTypeException if the key metadata is not supported by the preference store.
	 * @throws PreferencesException if an error occurs accessing the preference store.
	 */
	public <T> void removeValue(String key, PreferenceKeyMetadata<T> metadata)
	throws UnsupportedDataTypeException, PreferencesException;
	
	/**
	 * Removes all keys and values from the preference node.  Does not remove child nodes.
	 * 
	 * @throws PreferencesException if an error occurs accessing the preference store.
	 */
	public void removeAllValues()
	throws PreferencesException;
	
	/**
	 * Accesses a child of this node.  If the child node does not exist, a new empty node will be returned.
	 * 
	 * @param name the name of the child node.
	 * 
	 * @return the child node.
	 * 
	 * @throws NullPointerException if <code>name</code> is null.
	 * @throws PreferencesException if an error occurs accessing the preference store.
	 */
	public PreferenceNode getChildNode(String name)
	throws PreferencesException;
	
	/**
	 * Removes a child node, its keys and its descendants.  If the child nodes does not exist then no action is performed.
	 * 
	 * @param name the name of the child node to remove.
	 * 
	 * @throws NullPointerException if <code>name</code> is null.
	 * @throws PreferencesException if an error occurs accessing the preference store.
	 */
	public void removeChildNode(String name)
	throws PreferencesException;
	
	/**
	 * Retrieves the names of all the keys in this node.  The returned set is unmodifiable.
	 * 
	 * @return the names of the keys in this node.
	 * 
	 * @throws PreferencesException if an error occurs accessing the preference store.
	 */
	public Set<String> getKeyNames()
	throws PreferencesException;
	
	/**
	 * Retrieves the names of all the child nodes of this node.  The returned set is unmodifiable.
	 * 
	 * @return the names of the child nodes.
	 * 
	 * @throws PreferencesException if an error occurs accessing the preference store.
	 */
	public Set<String> getNodeNames()
	throws PreferencesException;
	
	/**
	 * Flushes any cached data through to the underlying store.  Some preference stores may cache data in memory.
	 * 
	 * @throws PreferencesException if an error occurs.
	 */
	public void flush()
	throws PreferencesException;
	
	/**
	 * Closes the preference node.  No further action should be performed on it.  This will also cause any cached data to be 
	 * {@linkplain #flush() flushed}.
	 * 
	 * @throws PreferencesException if an error occurs.
	 */
	@Override
	public void close() 
	throws PreferencesException;
}
