package au.net.causal.projo.metadata;

import java.lang.annotation.Annotation;
import java.lang.reflect.Type;

import org.apache.commons.lang3.StringUtils;

import au.net.causal.projo.annotation.Preference;
import au.net.causal.projo.bind.EntityMetadata;
import au.net.causal.projo.bind.PropertyAccessor;
import au.net.causal.projo.bind.PropertyMetadata;

public class SettingsPropertyMetadata extends PropertyMetadata
{
	private final String key;
	private final SettingsEntityMetadata targetEntity;
	
	public SettingsPropertyMetadata(String name, Type propertyType, EntityMetadata<?> parent, PropertyAccessor accessor,
			Iterable<? extends Annotation> annotations, SettingsEntityMetadata targetEntity)
	{
		super(name, propertyType, parent, accessor, annotations);
		
		Preference preference = getAnnotation(Preference.class);
		if (preference == null || StringUtils.isEmpty(preference.key()))
			key = name;
		else
			key = preference.key();
		
		this.targetEntity = targetEntity;
	}
	
	public String getKey()
	{
		return(key);
	}
	
	public SettingsEntityMetadata getTargetEntity()
	{
		return(targetEntity);
	}
}
