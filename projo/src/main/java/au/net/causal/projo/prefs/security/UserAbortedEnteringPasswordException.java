package au.net.causal.projo.prefs.security;

/**
 * Thrown from the {@link SourcedByteEncrypter} when the user aborted entering a password necessary for encryption or decryption.
 * 
 * @author prunge
 */
public class UserAbortedEnteringPasswordException extends RuntimeException
{
	/**
	 * Creates a <code>UserAbortedEnteringPasswordException</code>.
	 */
	public UserAbortedEnteringPasswordException()
	{
	}

	/**
	 * Creates a <code>UserAbortedEnteringPasswordException</code> with a detail message.
	 * 
	 * @param message the detail message.
	 */
	public UserAbortedEnteringPasswordException(String message)
	{
		super(message);
	}
}
