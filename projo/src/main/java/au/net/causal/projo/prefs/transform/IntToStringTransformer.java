package au.net.causal.projo.prefs.transform;

import au.net.causal.projo.prefs.PreferencesException;

/**
 * Converts from {@link Integer} and primitive int data type to string data type.
 * 
 * @author prunge
 */
public class IntToStringTransformer extends GenericToStringTransformer<Integer>
{
	public IntToStringTransformer()
	{
		super(Integer.class);
	}
	
	@Override
	protected Integer stringToValue(String s) throws PreferencesException
	{
		try
		{
			return(Integer.valueOf(s));
		}
		catch (NumberFormatException e)
		{
			throw new PreferencesException("Failed to convert value '" + s + "' to an integer.", e);
		}
	}
	
	@Override
	protected String valueToString(Integer value) throws PreferencesException
	{
		return(value.toString());
	}
}
