package au.net.causal.projo.prefs;

import java.lang.annotation.Annotation;
import java.util.Set;

import au.net.causal.projo.annotation.MustUnderstand;

import com.google.common.collect.ImmutableSet;

/**
 * Superclass of {@link PreferenceNode} to make implementation easier.
 * <p>
 * 
 * Implementations should implement the <code>...Impl</code> family of methods.  The original overridden methods perform the {@literal @}{@link MustUnderstand}
 * annotation checking.
 * 
 * @author prunge
 */
public abstract class AbstractPreferenceNode implements PreferenceNode
{
	private final Set<Class<? extends Annotation>> nativelySupportedAnnotationTypes;
	
	/**
	 * Creates a <code>AbstractPreferenceNode</code>.
	 * 
	 * @param nativelySupportedAnnotationTypes a set of key metadata annotation types that are natively understood by the preference node implementation.
	 * 			May be empty.
	 * 
	 * @throws NullPointerException if <code>nativelySupportedAnnotationTypes</code> is null.
	 */
	protected AbstractPreferenceNode(Set<Class<? extends Annotation>> nativelySupportedAnnotationTypes)
	{
		if (nativelySupportedAnnotationTypes == null)
			throw new NullPointerException("nativelySupportedAnnotationTypes == null");
		
		this.nativelySupportedAnnotationTypes = ImmutableSet.copyOf(nativelySupportedAnnotationTypes);
	}
	
	@Override
	public final boolean isDataTypeSupported(PreferenceKeyMetadata<?> keyType) throws PreferencesException
	{
		if (keyType == null)
			throw new NullPointerException("keyType == null");
		
		if (isUnsupportedAnnotationPresent(keyType))
			return(false);
		
		return(isDataTypeSupportedImpl(keyType));
	}
	
	/**
	 * Performs a check of the annotations present on key metadata, returning <code>true</code> if there is at least one annotation marked with 
	 * {@literal @}{@link MustUnderstand} that is not natively supported by the store.
	 *  
	 * @param keyType the key metadata.
	 * 
	 * @return true if the key is unsupported, false otherwise.
	 * 
	 * @throws NullPointerException if <code>keyType</code> is null.
	 */
	protected boolean isUnsupportedAnnotationPresent(PreferenceKeyMetadata<?> keyType)
	{
		if (keyType == null)
			throw new NullPointerException("keyType == null");
		
		for (Class<? extends Annotation> curAnnotationType : keyType.getAnnotationTypes())
		{
			//If there is a @MustUnderstand annotation present and it is not natively supported, get out now
			if (curAnnotationType.isAnnotationPresent(MustUnderstand.class))
			{
				if (!nativelySupportedAnnotationTypes.contains(curAnnotationType))
					return(true);
			}
		}
		
		return(false);
	}
	
	@Override
	public final <T> T getValue(String key, PreferenceKeyMetadata<T> metadata) 
	throws UnsupportedDataTypeException, PreferencesException
	{
		if (key == null)
			throw new NullPointerException("key == null");
		if (metadata == null)
			throw new NullPointerException("metadata == null");
		
		if (isUnsupportedAnnotationPresent(metadata))
			throw new UnsupportedDataTypeException(metadata.getDataType(), metadata.getDataType() + " unsupported due to a non-understood annotation.");
		
		return(getValueImpl(key, metadata));
	}
	
	@Override
	public final <T> void putValue(String key, T value, PreferenceKeyMetadata<T> metadata) throws UnsupportedDataTypeException, PreferencesException
	{
		if (key == null)
			throw new NullPointerException("key == null");
		if (metadata == null)
			throw new NullPointerException("metadata == null");
		
		if (isUnsupportedAnnotationPresent(metadata))
			throw new UnsupportedDataTypeException(metadata.getDataType(), metadata.getDataType() + " unsupported due to a non-understood annotation.");
		
		putValueImpl(key, value, metadata);
	}
	
	@Override
	public final <T> void removeValue(String key, PreferenceKeyMetadata<T> metadata) throws UnsupportedDataTypeException, PreferencesException
	{
		if (key == null)
			throw new NullPointerException("key == null");
		if (metadata == null)
			throw new NullPointerException("metadata == null");
		
		if (isUnsupportedAnnotationPresent(metadata))
			throw new UnsupportedDataTypeException(metadata.getDataType(), metadata.getDataType() + " unsupported due to a non-understood annotation.");
		
		removeValueImpl(key, metadata);
	}
	
	/**
	 * Implementors should implement this method for retrieving values.  {@link MustUnderstand} annotation checking is already performed by this class.
	 * 
	 * @see PreferenceNode#getValue(String, PreferenceKeyMetadata)
	 */
	protected abstract <T> T getValueImpl(String key, PreferenceKeyMetadata<T> metadata)
	throws UnsupportedDataTypeException, PreferencesException;
	
	/**
	 * Implementors should implement this method for storing values.  {@link MustUnderstand} annotation checking is already performed by this class.
	 * 
	 * @see PreferenceNode#putValue(String, Object, PreferenceKeyMetadata)
	 */
	protected abstract <T> void putValueImpl(String key, T value, PreferenceKeyMetadata<T> metadata)
	throws UnsupportedDataTypeException, PreferencesException;
	
	/**
	 * Implementors should implement this method for removing single values.  {@link MustUnderstand} annotation checking is already performed by this class.
	 * 
	 * @see PreferenceNode#removeValue(String, PreferenceKeyMetadata)
	 */
	protected abstract <T> void removeValueImpl(String key, PreferenceKeyMetadata<T> metadata)
	throws UnsupportedDataTypeException, PreferencesException;
	
	/**
	 * Implementors should implement this method for data type checking.  {@link MustUnderstand} annotation checking is already performed by this class.
	 * 
	 * @see PreferenceNode#isDataTypeSupported(PreferenceKeyMetadata)
	 */
	protected abstract boolean isDataTypeSupportedImpl(PreferenceKeyMetadata<?> keyType) 
	throws PreferencesException;
}
