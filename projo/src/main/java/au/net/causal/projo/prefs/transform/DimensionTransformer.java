package au.net.causal.projo.prefs.transform;

import java.awt.Dimension;

import au.net.causal.projo.prefs.DataTypeSupport;
import au.net.causal.projo.prefs.PreferenceKeyMetadata;
import au.net.causal.projo.prefs.PreferencesException;
import au.net.causal.projo.prefs.TransformDataTypeSupportChain;
import au.net.causal.projo.prefs.TransformGetChain;
import au.net.causal.projo.prefs.TransformPutChain;
import au.net.causal.projo.prefs.TransformRemoveChain;
import au.net.causal.projo.prefs.TransformResult;

import com.google.common.reflect.TypeToken;

/**
 * Allows storage and retrieval of {@link Dimension} values in stores that do not natively support this data type.
 * <p>
 * 
 * The dimension value's width and height components are stored under separate keys in the preference store with {@link Integer} data type.
 * <p>
 * 
 * This transformer will only declare support if:
 * <ul>
 * 	<li>Dimension is not supported natively by the store</li>
 * 	<li>Key is of Dimension type</li>
 * 	<li>Store supports (either directly or indirectly through other transformers) the {@link Integer} data type.</li>
 * </ul>
 * 
 * @author prunge
 */
public class DimensionTransformer implements PreferenceTransformer
{
	private final String widthPropertySuffix = "Width";
	private final String heightPropertySuffix = "Height";
	
	@Override
	public <T> TransformResult<T> applyGet(String key, PreferenceKeyMetadata<T> keyMetadata, TransformGetChain chain)
	throws PreferencesException
	{
		if (!isSupported(keyMetadata, chain))
			return(null);
		
		//Look up the component values
		Integer width = chain.getValue(key + widthPropertySuffix, keyMetadata.withDataType(Integer.class));
		Integer height = chain.getValue(key + heightPropertySuffix, keyMetadata.withDataType(Integer.class));
		
		if (width == null || height == null)
			return(new TransformResult<>(null));
		
		Dimension d = new Dimension(width, height);
		
		@SuppressWarnings("unchecked")
		TransformResult<T> result = new TransformResult<>((T)d);
		
		return(result);
	}

	@Override
	public <T> boolean applyPut(String key, T value, PreferenceKeyMetadata<T> keyMetadata, TransformPutChain chain)
	throws PreferencesException
	{
		if (!isSupported(keyMetadata, chain))
			return(false);
		
		//Store component values
		Dimension dValue = (Dimension)value;
		
		Integer width, height;
		if (dValue == null)
		{
			width = null;
			height = null;
		}
		else
		{
			width = dValue.width;
			height = dValue.height;
		}
		
		PreferenceKeyMetadata<Integer> componentMetadata = keyMetadata.withDataType(Integer.class);
		chain.putValue(key + widthPropertySuffix, width, componentMetadata);
		chain.putValue(key + heightPropertySuffix, height, componentMetadata);
		
		return(true);
	}
	
	@Override
	public <T> boolean applyRemove(String key, PreferenceKeyMetadata<T> keyMetadata, TransformRemoveChain chain)
			throws PreferencesException
	{
		if (!isSupported(keyMetadata, chain))
			return(false);
		
		PreferenceKeyMetadata<Integer> componentMetadata = keyMetadata.withDataType(Integer.class);
		chain.removeValue(key + widthPropertySuffix, componentMetadata);
		chain.removeValue(key + heightPropertySuffix, componentMetadata);
		
		return(true);
	}
	
	@Override
	public DataTypeSupport applyDataTypeSupport(PreferenceKeyMetadata<?> keyMetadata, TransformDataTypeSupportChain chain)
	throws PreferencesException
	{
		//Only for Dimension keys
		if (!keyMetadata.getDataType().equals(TypeToken.of(Dimension.class)))
			return(null);
		
		//If underlying store supports integers then add support, otherwise we can't touch it
		if (!chain.isDataTypeSupported(keyMetadata.withDataType(Integer.class)))
			return(null);
		
		//If we get here the transform will work
		return(DataTypeSupport.ADD_SUPPORT);
	}

	/**
	 * Only use this transformer if:
	 * 
	 * <ul>
	 * 	<li>Store does not support {@link Dimension} data type natively</li>
	 * 	<li>Store supports {@link Integer} data type</li>
	 * 	<li>Key is a Dimension data type</li>
	 * </ul>
	 * 
	 * @throws PreferencesException if the underlying store fails to retrieve data type support information.
	 */
	private boolean isSupported(PreferenceKeyMetadata<?> keyMetadata, TransformDataTypeSupportChain chain)
	throws PreferencesException
	{
		if (!TypeToken.of(Dimension.class).isAssignableFrom(keyMetadata.getDataType()))
			return(false);
		if (chain.isDataTypeSupportedNatively(keyMetadata.withDataType(Dimension.class)))
			return(false);
		if (!chain.isDataTypeSupported(keyMetadata.withDataType(Integer.class)))
			return(false);
		
		return(true);
	}
}
