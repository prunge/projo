package au.net.causal.projo.prefs.transform;

import au.net.causal.projo.prefs.PreferencesException;

/**
 * Converts between short numeric values and strings.
 * 
 * @author prunge
 */
public class ShortToStringTransformer extends GenericToStringTransformer<Short>
{
	public ShortToStringTransformer()
	{
		super(Short.class);
	}
	
	@Override
	protected Short stringToValue(String s) throws PreferencesException
	{
		try
		{
			return(Short.valueOf(s));
		}
		catch (NumberFormatException e)
		{
			throw new PreferencesException("Failed to convert value '" + s + "' to a short.", e);
		}
	}
	
	@Override
	protected String valueToString(Short value) throws PreferencesException
	{
		return(value.toString());
	}
}
