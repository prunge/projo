package au.net.causal.projo.annotation;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * When specified on a Projo annotation, then either a transform or the store itself must consume/understand this annotation or else
 * an error will occur.
 * 
 * @author prunge
 */
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.ANNOTATION_TYPE)
public @interface MustUnderstand
{

}
