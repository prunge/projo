package au.net.causal.projo.prefs.transform;

import au.net.causal.projo.prefs.PreferencesException;

/**
 * Converts between single byte values and strings.
 * 
 * @author prunge
 */
public class ByteToStringTransformer extends GenericToStringTransformer<Byte>
{
	public ByteToStringTransformer()
	{
		super(Byte.class);
	}
	
	@Override
	protected Byte stringToValue(String s) throws PreferencesException
	{
		try
		{
			return(Byte.valueOf(s));
		}
		catch (NumberFormatException e)
		{
			throw new PreferencesException("Failed to convert value '" + s + "' to a byte.", e);
		}
	}
	
	@Override
	protected String valueToString(Byte value) throws PreferencesException
	{
		return(value.toString());
	}
}
