package au.net.causal.projo.metadata;

import java.lang.annotation.Annotation;

import au.net.causal.projo.bind.EntityMetadata;

public class SettingsEntityMetadata extends EntityMetadata<SettingsPropertyMetadata>
{
	public SettingsEntityMetadata(Class<?> entityType, Iterable<? extends Annotation> annotations)
	{
		super(entityType, annotations);
	}
	
	
}
