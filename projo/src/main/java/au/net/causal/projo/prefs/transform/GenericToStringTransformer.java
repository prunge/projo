package au.net.causal.projo.prefs.transform;

import java.math.BigInteger;

import au.net.causal.projo.prefs.DataTypeSupport;
import au.net.causal.projo.prefs.PreferenceKeyMetadata;
import au.net.causal.projo.prefs.PreferencesException;
import au.net.causal.projo.prefs.TransformDataTypeSupportChain;
import au.net.causal.projo.prefs.TransformGetChain;
import au.net.causal.projo.prefs.TransformPutChain;
import au.net.causal.projo.prefs.TransformRemoveChain;
import au.net.causal.projo.prefs.TransformResult;

import com.google.common.primitives.Primitives;
import com.google.common.reflect.TypeToken;

/**
 * Superclass to make it easier to convert from arbitrary data types to the string data type.
 * <p>
 * 
 * This transformer will be supported only if the store has support for {@link String} (which might be through other 
 * transformers).
 * <p>
 * 
 * Typical use of this class involves subclassing this transformer, specifying a specific data type for generic parameter <code>D</code>, and implementing the
 * conversion methods {@link #stringToValue(String)} and {@link #valueToString(Object)}.
 * 
 * @author prunge
 *
 * @param <D> the data type to convert from.
 */
public abstract class GenericToStringTransformer<D> implements PreferenceTransformer
{
	private final Class<D> dataType;
	private final Class<?> primitiveDataType;
	private final boolean allowSubclasses;
	
	protected GenericToStringTransformer(Class<D> dataType)
	{
		this(dataType, false);
	}
	
	protected GenericToStringTransformer(Class<D> dataType, boolean allowSubclasses)
	{
		if (dataType == null)
			throw new NullPointerException("dataType == null");
		
		if (dataType.isPrimitive())
		{
			this.dataType = Primitives.wrap(dataType);
			this.primitiveDataType = dataType;
		}
		else
		{
			this.dataType = dataType;
			if (Primitives.isWrapperType(dataType))
				this.primitiveDataType = Primitives.unwrap(dataType);
			else
				this.primitiveDataType = null;
		}
		
		this.allowSubclasses = allowSubclasses;
	}
	
	@Override
	public <T> TransformResult<T> applyGet(String key, PreferenceKeyMetadata<T> keyMetadata, TransformGetChain chain)
	throws PreferencesException
	{
		if (!isSupported(keyMetadata, chain))
			return(null);
		
		//Read as string from underlying store, convert to Long and pass up
		String storeValue = chain.getValue(key, keyMetadata.withDataType(String.class));
		
		D gValue;
		if (storeValue == null)
			gValue = null;
		else
			gValue = stringToValue(storeValue);
		
		//Safe because isSupported() ensured that T is only of generic type
		@SuppressWarnings("unchecked")
		TransformResult<T> result = new TransformResult<>((T)gValue);
		
		return(result);
	}
	
	@Override
	public <T> boolean applyPut(String key, T value, PreferenceKeyMetadata<T> keyMetadata, TransformPutChain chain)
	throws PreferencesException
	{
		if (!isSupported(keyMetadata, chain))
			return(false);
		
		//The value must be of datatype if we get here
		D gValue = dataType.cast(value);
		
		//Convert to string
		String strValue;
		if (gValue == null)
			strValue = null;
		else
			strValue = valueToString(gValue);
		
		chain.putValue(key, strValue, keyMetadata.withDataType(String.class));
		
		return(true);
	}
	
	/**
	 * Converts from the declared data type to a string value.
	 * 
	 * @param value the value to convert.  Will never be null.
	 * 
	 * @return the resulting value.
	 * 
	 * @throws PreferencesException if a conversion error occurs.
	 */
	protected abstract String valueToString(D value)
	throws PreferencesException;
	
	/**
	 * Converts from string data type to the declared data type.
	 * 
	 * @param value the value to convert.  Will never be null.
	 * 
	 * @return the resulting value.
	 * 
	 * @throws PreferencesException if a conversion error occurs.
	 */
	protected abstract D stringToValue(String s)
	throws PreferencesException;
	
	@Override
	public <T> boolean applyRemove(String key, PreferenceKeyMetadata<T> keyMetadata, TransformRemoveChain chain)
	throws PreferencesException
	{
		if (!isSupported(keyMetadata, chain))
			return(false);
		
		chain.removeValue(key, keyMetadata.withDataType(String.class));
		
		return(true);
	}
	
	@Override
	public DataTypeSupport applyDataTypeSupport(PreferenceKeyMetadata<?> keyMetadata, TransformDataTypeSupportChain chain)
	throws PreferencesException
	{
		//Only for keys for dataType or primitiveDataType
		boolean isApplicableDataType;
		if (keyMetadata.getDataType().equals(TypeToken.of(dataType)))
			isApplicableDataType = true;
		else if (primitiveDataType != null && keyMetadata.getDataType().equals(TypeToken.of(primitiveDataType)))
			isApplicableDataType = true;
		else if (allowSubclasses && TypeToken.of(dataType).isAssignableFrom(keyMetadata.getDataType()))
			isApplicableDataType = true;
		else
			isApplicableDataType = false;
		
		if (!isApplicableDataType)
			return(null);
		
		//If underlying store supports strings then add support, otherwise we can't touch it
		if (!chain.isDataTypeSupported(keyMetadata.withDataType(String.class)))
			return(null);
		
		//If we get here the transform will work
		return(DataTypeSupport.ADD_SUPPORT);
	}

	/**
	 * Only use this transformer if:
	 * 
	 * <ul>
	 * 	<li>Store does not support data type natively</li>
	 * 	<li>Store supports {@link String} data type</li>
	 * 	<li>Key is for data type</li>
	 * </ul>
	 * 
	 * @throws PreferencesException if the underlying store fails to retrieve data type support information.
	 */
	private boolean isSupported(PreferenceKeyMetadata<?> keyMetadata, TransformDataTypeSupportChain chain)
	throws PreferencesException
	{
		//Only for keys for dataType or primitiveDataType
		boolean isApplicableDataType;
		if (keyMetadata.getDataType().equals(TypeToken.of(dataType)))
			isApplicableDataType = true;
		else if (primitiveDataType != null && keyMetadata.getDataType().equals(TypeToken.of(primitiveDataType)))
			isApplicableDataType = true;
		else if (allowSubclasses && TypeToken.of(dataType).isAssignableFrom(keyMetadata.getDataType()))
			isApplicableDataType = true;
		else
			isApplicableDataType = false;
		
		if (!isApplicableDataType)
			return(false);
		if (chain.isDataTypeSupportedNatively(keyMetadata.withDataType(dataType)))
			return(false);
		if (!chain.isDataTypeSupported(keyMetadata.withDataType(String.class)))
			return(false);
		
		return(true);
	}
}
