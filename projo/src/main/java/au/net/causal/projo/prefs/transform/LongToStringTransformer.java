package au.net.causal.projo.prefs.transform;

import au.net.causal.projo.prefs.PreferencesException;

/**
 * Converts between {@link Long} values and string values.
 * 
 * @author prunge
 */
public class LongToStringTransformer extends GenericToStringTransformer<Long>
{
	public LongToStringTransformer()
	{
		super(Long.class);
	}
	
	@Override
	protected Long stringToValue(String s) throws PreferencesException
	{
		try
		{
			return(Long.valueOf(s));
		}
		catch (NumberFormatException e)
		{
			throw new PreferencesException("Failed to convert value '" + s + "' to a long.", e);
		}
	}
	
	@Override
	protected String valueToString(Long value) throws PreferencesException
	{
		return(value.toString());
	}
}
