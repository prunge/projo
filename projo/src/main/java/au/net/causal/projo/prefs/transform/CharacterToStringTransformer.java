package au.net.causal.projo.prefs.transform;

import au.net.causal.projo.prefs.PreferencesException;

/**
 * Converts between single character values and strings.
 * 
 * @author prunge
 */
public class CharacterToStringTransformer extends GenericToStringTransformer<Character>
{
	public CharacterToStringTransformer()
	{
		super(Character.class);
	}

	@Override
	protected String valueToString(Character value) throws PreferencesException
	{
		if (value == null)
			return(null);
		
		return(value.toString());
	}

	@Override
	protected Character stringToValue(String s) throws PreferencesException
	{
		if (s == null)
			return(null);
		if (s.isEmpty())
			return(null);
		
		return(s.charAt(0));
	}
}
