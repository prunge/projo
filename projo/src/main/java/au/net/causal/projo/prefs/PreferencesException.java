package au.net.causal.projo.prefs;

/**
 * A general preferences exception that occurs when something goes wrong reading or writing preferences.
 * 
 * @author prunge
 */
public class PreferencesException extends Exception
{
	/**
	 * Creates a <code>PreferencesException</code>.
	 */
	public PreferencesException()
	{
	}

	/**
	 * Creates a <code>PreferencesException</code> with a detail message.
	 * 
	 * @param message the detail message.
	 */
	public PreferencesException(String message)
	{
		super(message);
	}

	/**
	 * Creates a <code>PreferencesException</code> with a cause exception.
	 * 
	 * @param cause the cause exception.
	 */
	public PreferencesException(Throwable cause)
	{
		super(cause);
	}

	/**
	 * Creates a <code>PreferencesException</code> with a detail message and cause exception.
	 * 
	 * @param message the detail message.
	 * @param cause the cause exception.
	 */
	public PreferencesException(String message, Throwable cause)
	{
		super(message, cause);
	}

	/**
	 * Creates a <code>PreferencesException</code> with a detail message, cause exception and configurable supression and stack trace writability.
	 * 
	 * @param message the detail message.
	 * @param cause the cause exception.
	 * @param enableSuppression whether or not suppression is enabled or disabled.
	 * @param writableStackTrace whether or not the stack trace should be writable.
	 */
	public PreferencesException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace)
	{
		super(message, cause, enableSuppression, writableStackTrace);
	}
}
