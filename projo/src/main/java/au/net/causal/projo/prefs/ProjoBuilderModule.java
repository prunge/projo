package au.net.causal.projo.prefs;

/**
 * Callback interface for configuring {@link ProjoStoreBuilder} using fluent API easily.
 * 
 * @author prunge
 */
public interface ProjoBuilderModule
{
	/**
	 * Configures the builder.
	 * 
	 * @param builder the builder to configure.
	 */
	public void configure(ProjoStoreBuilder builder);
}
