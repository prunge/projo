package au.net.causal.projo.prefs;

/**
 * A chain allows transformers to call other transformers indirectly. A single transformer node call might result in a chain of transformers being called.
 * 
 * @author prunge
 */
public interface TransformGetChain extends TransformDataTypeSupportChain
{
	/**
	 * Asks the native store and other transformers to retrieve a value for a key.
	 * 
	 * @param key the key name.
	 * @param keyMetadata the key metadata.
	 * 
	 * @return the value retrieved, or <code>null</code> if there was none.
	 * 
	 * @throws NullPointerException if <code>key</code> or <code>keyMetadata</code> is null.
	 * @throws UnsupportedDataTypeException if the key metadata was not supported by the native store and no other transformers could transform it.
	 * @throws PreferencesException if an error occurs in the store or another transform.
	 */
	public <T> T getValue(String key, PreferenceKeyMetadata<T> keyMetadata)
	throws UnsupportedDataTypeException, PreferencesException;
	
	/**
	 * Asks other transformers to retrieve a value for a key using a custom endpoint in place of the native store.
	 * <p>
	 * 
	 * This can be useful when other keys need to be read from underlying store, but a transform has to happen first.  In this case, the custom 
	 * implementation of the new endpoint can filter through to the underlying store or original chain, apply any transformation as needed and then 
	 * use other transformers in the chain with this modified data.
	 * 
	 * @param key the key name.
	 * @param keyMetadata the key metadata.
	 * @param newEndpoint the custom endpoint to use for other transforms reading data.
	 * 
	 * @return the value retrieved, or <code>null</code> if there was none.
	 * 
	 * @throws NullPointerException if <code>key</code>, <code>keyMetadata</code> or <code>newEndpoint</code> is null.
	 * @throws UnsupportedDataTypeException if the key metadata was not supported by the native store and no other transformers could transform it.
	 * @throws PreferencesException if an error occurs in the store or another transform.
	 */
	public <T> T getValueWithRestartedChain(String key, PreferenceKeyMetadata<T> keyMetadata, TransformGetChain newEndpoint)
	throws UnsupportedDataTypeException, PreferencesException;

}
