package au.net.causal.projo.prefs;

/**
 * Wrapper for the result of a data transform.
 * <p>
 * 
 * This allows for a null value result (a TransformResult object with null value) to be distinguished from a null TransformResult which indicates that a transform
 * could not be performed.
 *  
 * @author prunge
 *
 * @param <T> the result data type.
 */
public class TransformResult<T>
{
	private final T value;
	
	/**
	 * Creates a <code>TransformResult</code>.
	 * 
	 * @param value the value.  May be null.
	 */
	public TransformResult(T value)
	{
		this.value = value;
	}
	
	/**
	 * Returns the transformed value.  The transformed value may be null.
	 * 
	 * @return the transformed value.
	 */
	public T getValue()
	{
		return(value);
	}
}
