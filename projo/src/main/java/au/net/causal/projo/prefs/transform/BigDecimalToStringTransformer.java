package au.net.causal.projo.prefs.transform;

import java.math.BigDecimal;

import au.net.causal.projo.prefs.PreferencesException;

/**
 * Converts between {@link BigDecimal} values and strings.
 * 
 * @author prunge
 */
public class BigDecimalToStringTransformer extends GenericToStringTransformer<BigDecimal>
{
	public BigDecimalToStringTransformer()
	{
		super(BigDecimal.class);
	}
	
	@Override
	protected BigDecimal stringToValue(String s) throws PreferencesException
	{
		try
		{
			return(new BigDecimal(s));
		}
		catch (NumberFormatException e)
		{
			throw new PreferencesException("Failed to convert value '" + s + "' to a decimal number.", e);
		}
	}
	
	@Override
	protected String valueToString(BigDecimal value) throws PreferencesException
	{
		return(value.toString());
	}
}
