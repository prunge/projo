package au.net.causal.projo.prefs.transform;

import au.net.causal.projo.prefs.PreferencesException;

/**
 * Converts between {@link Double} values and strings using standard double syntax.
 * 
 * @author prunge
 */
public class DoubleToStringTransformer extends GenericToStringTransformer<Double>
{
	public DoubleToStringTransformer()
	{
		super(Double.class);
	}
	
	@Override
	protected Double stringToValue(String s) throws PreferencesException
	{
		try
		{
			return(Double.valueOf(s));
		}
		catch (NumberFormatException e)
		{
			throw new PreferencesException("Failed to convert value '" + s + "' to a decimal number.", e);
		}
	}
	
	@Override
	protected String valueToString(Double value) throws PreferencesException
	{
		return(value.toString());
	}
}
