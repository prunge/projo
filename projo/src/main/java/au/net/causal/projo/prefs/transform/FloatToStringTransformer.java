package au.net.causal.projo.prefs.transform;

import au.net.causal.projo.prefs.PreferencesException;

/**
 * Converts between {@link Float} data type and string.
 * 
 * @author prunge
 */
public class FloatToStringTransformer extends GenericToStringTransformer<Float>
{
	public FloatToStringTransformer()
	{
		super(Float.class);
	}
	
	@Override
	protected Float stringToValue(String s) throws PreferencesException
	{
		try
		{
			return(Float.valueOf(s));
		}
		catch (NumberFormatException e)
		{
			throw new PreferencesException("Failed to convert value '" + s + "' to a decimal number.", e);
		}
	}
	
	@Override
	protected String valueToString(Float value) throws PreferencesException
	{
		return(value.toString());
	}
}
