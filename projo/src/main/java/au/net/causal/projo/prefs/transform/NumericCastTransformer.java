package au.net.causal.projo.prefs.transform;

import java.math.BigInteger;
import java.util.List;

import au.net.causal.projo.prefs.DataTypeSupport;
import au.net.causal.projo.prefs.PreferenceKeyMetadata;
import au.net.causal.projo.prefs.PreferencesException;
import au.net.causal.projo.prefs.TransformDataTypeSupportChain;
import au.net.causal.projo.prefs.TransformGetChain;
import au.net.causal.projo.prefs.TransformPutChain;
import au.net.causal.projo.prefs.TransformRemoveChain;
import au.net.causal.projo.prefs.TransformResult;

import com.google.common.collect.ImmutableList;
import com.google.common.collect.Lists;
import com.google.common.primitives.Primitives;

/**
 * Transforms between integer numeric data types to fit the native store.  This is useful when a store only supports a larger data type but support is needed
 * for the smaller ones.
 * <p>
 * 
 * For example, if the native store supports {@link Long}s, then this transformer will add support for the smaller numerics {@link Integer}, {@link Short} and
 * {@link Byte}, performing numeric conversions where appropriate.
 * 
 * @author prunge
 */
public class NumericCastTransformer implements PreferenceTransformer
{
	private final List<Class<? extends Number>> dataTypes = ImmutableList.<Class<? extends Number>>of(BigInteger.class, Long.class, Integer.class, Short.class, Byte.class);
	
	@Override
	public <T> TransformResult<T> applyGet(String key, PreferenceKeyMetadata<T> keyMetadata, TransformGetChain chain)
	throws PreferencesException
	{
		Class<? extends Number> bestMatch = findClosestNativeSupportForDataType(keyMetadata, chain);
		if (bestMatch == null)
			return(null);
		
		Number nValue = chain.getValue(key, keyMetadata.withDataType(bestMatch));
		if (nValue == null)
			return(new TransformResult<>(null));
		
		Class<? extends Number> keyType = Primitives.wrap(keyMetadata.getDataType().getRawType()).asSubclass(Number.class);
		
		//Need to convert the native value nValue to key's data type
		//For BigInteger, because we know we are doing a conversion it must be long or smaller
		if (BigInteger.class.equals(keyType))
			return(new TransformResult<>((T)BigInteger.valueOf(nValue.longValue())));
		else if (Long.class.equals(keyType))
			return(new TransformResult<>((T)Long.valueOf(nValue.longValue())));
		else if (Integer.class.equals(keyType))
			return(new TransformResult<>((T)Integer.valueOf(nValue.intValue())));
		else if (Short.class.equals(keyType))
			return(new TransformResult<>((T)Short.valueOf(nValue.shortValue())));
		else if (Byte.class.equals(keyType))
			return(new TransformResult<>((T)Byte.valueOf(nValue.byteValue())));
		else ///Should never get here
			throw new Error("Unknokwn bestMatch data type.");
	}
	
	@Override
	public <T> boolean applyPut(String key, T value, PreferenceKeyMetadata<T> keyMetadata, TransformPutChain chain)
	throws PreferencesException
	{
		Class<? extends Number> bestMatch = findClosestNativeSupportForDataType(keyMetadata, chain);
		if (bestMatch == null)
			return(false);

		Number nValue = (Number)value;
		
		if (nValue == null)
			chain.putValue(key, null, keyMetadata.withDataType(bestMatch));
		
		//It must be long or smaller if bestMatch is BigInteger
		else if (BigInteger.class.equals(bestMatch))
			chain.putValue(key, BigInteger.valueOf(nValue.longValue()), keyMetadata.withDataType(BigInteger.class));
		else if (Long.class.equals(bestMatch))
			chain.putValue(key, Long.valueOf(nValue.longValue()), keyMetadata.withDataType(Long.class));
		else if (Integer.class.equals(bestMatch))
			chain.putValue(key, Integer.valueOf(nValue.intValue()), keyMetadata.withDataType(Integer.class));
		else if (Short.class.equals(bestMatch))
			chain.putValue(key, Short.valueOf(nValue.shortValue()), keyMetadata.withDataType(Short.class));
		else if (Byte.class.equals(bestMatch))
			chain.putValue(key, Byte.valueOf(nValue.byteValue()), keyMetadata.withDataType(Byte.class));
		else ///Should never get here
			throw new Error("Unknokwn bestMatch data type.");
		
		return(true);
	}
	
	@Override
	public <T> boolean applyRemove(String key, PreferenceKeyMetadata<T> keyMetadata, TransformRemoveChain chain)
	throws PreferencesException
	{
		Class<? extends Number> bestMatch = findClosestNativeSupportForDataType(keyMetadata, chain);
		if (bestMatch == null)
			return(false);
		
		chain.removeValue(key, keyMetadata.withDataType(bestMatch));
		
		return(true);
	}
	
	@Override
	public DataTypeSupport applyDataTypeSupport(PreferenceKeyMetadata<?> keyMetadata, TransformDataTypeSupportChain chain)
	throws PreferencesException
	{
		//If not a numeric data type then no support from this transformer
		Class<? extends Number> bestMatch = findClosestNativeSupportForDataType(keyMetadata, chain);
		if (bestMatch == null)
			return(null);
				
		//If we get here the transform will work
		return(DataTypeSupport.ADD_SUPPORT);
	}

	private Class<? extends Number> findClosestNativeSupportForDataType(PreferenceKeyMetadata<?> keyMetadata, TransformDataTypeSupportChain chain)
	throws PreferencesException
	{
		Class<?> keyType = keyMetadata.getDataType().getRawType();
		keyType = Primitives.wrap(keyType);
		int dataTypeIndex = dataTypes.indexOf(keyType);
		if (dataTypeIndex < 0)
			return(null);

		//If there is native support for the key type itself then this transform shouldn't touch it
		if (chain.isDataTypeSupportedNatively(keyMetadata.withDataType(keyType))) //Normalized data type
			return(null);
		
		//Find the next biggest data type supported natively
		List<Class<? extends Number>> supportForKey = dataTypes.subList(0, dataTypeIndex + 1);
		supportForKey = Lists.reverse(supportForKey);

		//Now supportForKey contains all data types that can support the key type, ordered from closest match up to largest data type
		//Go through and ask for native support until we find one
		for (Class<? extends Number> curDataType : supportForKey)
		{
			if (chain.isDataTypeSupportedNatively(keyMetadata.withDataType(curDataType)))
				return(curDataType);
		}
		
		return(null);
	}
}
