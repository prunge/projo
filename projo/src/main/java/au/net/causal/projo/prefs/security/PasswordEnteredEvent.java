package au.net.causal.projo.prefs.security;

/**
 * Event that occurs when the user has entered (and optionally confirmed) their password.
 * 
 * @author prunge
 */
public class PasswordEnteredEvent extends PasswordEvent
{
	private final char[] password;
	
	/**
	 * Creates a <code>PasswordEnteredEvent</code>.
	 * 
	 * @param source the source of the event.
	 * @param password the entered password.
	 */
	public PasswordEnteredEvent(JPasswordPane source, char[] password)
	{
		super(source);
		
		this.password = password;
	}
	
	/**
	 * Returns the password entered.
	 * 
	 * @return the password.
	 */
	public char[] getPassword()
	{
		return(password);
	}

}
