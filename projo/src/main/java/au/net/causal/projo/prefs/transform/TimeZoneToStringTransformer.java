package au.net.causal.projo.prefs.transform;

import java.util.TimeZone;

import au.net.causal.projo.prefs.PreferencesException;

/**
 * Converts {@link TimeZone} objects to strings using its ID.
 * 
 * @author prunge
 */
public class TimeZoneToStringTransformer extends GenericToStringTransformer<TimeZone>
{
	public TimeZoneToStringTransformer()
	{
		super(TimeZone.class);
	}
	
	@Override
	protected TimeZone stringToValue(String s) throws PreferencesException
	{
		if (s == null || s.isEmpty())
			return(null);
		
		return(TimeZone.getTimeZone(s));
	}
	
	@Override
	protected String valueToString(TimeZone value) throws PreferencesException
	{
		if (value == null)
			return(null);
		
		return(value.getID());
	}
}
