package au.net.causal.projo.prefs.security;

import javax.swing.JComponent;
import javax.swing.JDialog;
import javax.swing.UIManager;

import org.apache.commons.lang3.StringUtils;

import au.net.causal.projo.prefs.PreferencesException;

/**
 * Password source that reads passwords from a displayed Swing dialog.
 * <p>
 *
 * The {@linkplain #getPasswordPrompt() prompt} is used as the dialog's title.  An optional {@linkplain #getDetailMessage() detail message} can also be 
 * configured to display inside the dialog.
 * 
 * @author prunge
 */
public class SwingUiPasswordSource implements UiPromptPasswordSource
{
	private String prompt = "Configuration master password";
	private String detailMessage;
	private boolean alwaysOnTop = true;
	private JComponent parentComponent;

	public static void main(String... args)
	throws Exception
	{
		UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
		
		SwingUiPasswordSource p = new SwingUiPasswordSource();
		p.setDetailMessage("Please enter the configuration master password:");
		char[] password = p.readPassword(Mode.DECRYPTION);
		
		System.err.println("Password: " + (password == null ? null : String.valueOf(password)));
	}
	
	/**
	 * Returns the parent component the dialog will be shown over.
	 * 
	 * @return the parent component.
	 * 
	 * @see #setParentComponent(JComponent)
	 */
	public JComponent getParentComponent()
	{
		return(parentComponent);
	}

	/**
	 * Sets the parent component that the dialog will be shown over.  If this is set to a non-null value, the dialog will be modal.
	 * 
	 * @param parentComponent the parent component.
	 * 
	 * @see #getParentComponent()
	 */
	public void setParentComponent(JComponent parentComponent)
	{
		this.parentComponent = parentComponent;
	}

	/**
	 * Returns whether the dialog will attempt to be made topmost over all other windows.  
	 * 
	 * @return true if always on top mode is on, false if not.
	 * 
	 * @see #setAlwaysOnTop(boolean)
	 */
	public boolean isAlwaysOnTop()
	{
		return(alwaysOnTop);
	}

	/**
	 * Sets whether the dialog will attempt to be made topmost over all other windows.
	 * <p>
	 * 
	 * Making password prompts topmost can help prevent these types of dialogs getting buried under other windows.
	 * <p>
	 * 
	 * Topmost windows will only be used if the toolkit and system supports it.  If unsupported, this setting has no effect.
	 * 
	 * @param alwaysOnTop true to make always on top dialogs, false to make normal dialogs.
	 * 
	 * @see #isAlwaysOnTop()
	 */
	public void setAlwaysOnTop(boolean alwaysOnTop)
	{
		this.alwaysOnTop = alwaysOnTop;
	}
	
	/**
	 * Returns the detail message displayed inside the dialog.
	 *  
	 * @return the detail message.
	 * 
	 * @see #setDetailMessage(String)
	 */
	public String getDetailMessage()
	{
		return(detailMessage);
	}

	/**
	 * Sets the detail message displayed inside the dialog.  If <code>null</code>, then only a password control is displayed in the dialog.  
	 * 
	 * @param detailMessage the detail message to set.
	 * 
	 * @see #getDetailMessage()
	 */
	public void setDetailMessage(String detailMessage)
	{
		this.detailMessage = detailMessage;
	}

	@Override
	public char[] readPassword(Mode mode) throws PreferencesException
	{
		JPasswordPane pane = new JPasswordPane();
		if (!StringUtils.isEmpty(getDetailMessage()))
			pane.setDetailMessage(getDetailMessage());

		pane.setConfirming(mode == Mode.ENCRYPTION);

		JDialog dialog = pane.createDialog(getParentComponent(), getPasswordPrompt());
		dialog.setAlwaysOnTop(isAlwaysOnTop());
		dialog.setModal(true);
		
		dialog.setVisible(true);
		
		char[] result = pane.getPassword();
		return(result);
		
		/*
		String message = getPasswordPrompt();
		if (message == null)
			message = "";
		
		final JPasswordField passwordField = new JPasswordField(20);
		
		JComponent content;
		if (!StringUtils.isEmpty(getDetailMessage()))
		{
			JPanel uiContentPane = new JPanel(new BorderLayout());
			uiContentPane.add(passwordField, BorderLayout.SOUTH);
			uiContentPane.add(new JLabel(getDetailMessage()), BorderLayout.CENTER);
			content = uiContentPane;
		}
		else
			content = passwordField;
		
		JOptionPane jop = new JOptionPane(content, JOptionPane.PLAIN_MESSAGE, JOptionPane.OK_CANCEL_OPTION);
		JDialog dialog;
		if (getParentComponent() == null)
			dialog = jop.createDialog(message);
		else
		{
			dialog = jop.createDialog(getParentComponent(), message);
			dialog.setModalityType(JDialog.DEFAULT_MODALITY_TYPE);
		}
		
		if (isAlwaysOnTop() && dialog.getToolkit().isAlwaysOnTopSupported())
			dialog.setAlwaysOnTop(true);
		
		//Try our best to focus the password field
		dialog.addWindowListener(new WindowAdapter()
		{
			@Override
			public void windowActivated(WindowEvent e)
			{
				SwingUtilities.invokeLater(new Runnable()
				{
					@Override
					public void run()
					{
						passwordField.requestFocusInWindow();
					}
				});
			}
		});
		
		dialog.addComponentListener(new ComponentAdapter()
		{
			@Override
			public void componentShown(ComponentEvent e)
			{
				SwingUtilities.invokeLater(new Runnable()
				{
					@Override
					public void run()
					{
						passwordField.requestFocusInWindow();
					}
				});
			}
		});
		dialog.setVisible(true);
		
		int result = (Integer)jop.getValue();
		dialog.dispose();
		char[] password = null;
		if(result == JOptionPane.OK_OPTION)
			password = passwordField.getPassword();
		
		return(password);
		*/
	}

	@Override
	public String getPasswordPrompt()
	{
		return(prompt);
	}

	@Override
	public void setPasswordPrompt(String prompt)
	{
		this.prompt = prompt;
	}
}
