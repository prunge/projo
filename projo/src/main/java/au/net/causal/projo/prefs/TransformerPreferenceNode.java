package au.net.causal.projo.prefs;

import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import au.net.causal.projo.prefs.transform.PreferenceTransformer;

import com.google.common.collect.ArrayListMultimap;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.ListMultimap;

/**
 * A preference node that can perform data type transformation, backed by another preference store.
 * <p>
 * 
 * Preference node implementations typically support only a few data types.  Other data types can be converted into these supported data types, and this is 
 * what the transformer preference node does with the help of its registered transformers.
 * <p>
 * 
 * Transformers are registered through the {@link #addTransformer(PreferenceTransformer, TransformPhase)} method.  Each transformer typically converts one
 * data type into another data type.  It is also possible that multiple transformers may be executed in order to get to the underlying store.  For example, 
 * a transformer might support converting URIs into strings, and another might support converting strings into byte arrays, which is the only data type
 * the real store might support.
 * <p>
 * 
 * Transformers are organized into phases, and may only call onto other transformers in their phase.  Usually most transformers will be registered with
 * the {@link StandardTransformPhase#DATATYPE} phase.  However sometimes it might be required to run a transform either after or before this standard 
 * conversion phase, in which case one of the other {@link StandardTransformPhase}s may be used.
 * <p>
 * 
 * <h3>Note about retrieving {@linkplain #getKeyNames() key names}</h3>
 * 
 * Due to how transformers work, there is no reliable way to enumerate transformed key names that exist in a store.  Just because a key does not exist in 
 * the returned set of key names from {@link #getKeyNames()} does not mean that retrieving a value with that key will not produce a value; a transform might
 * transform key names on the way to the underlying store.
 * 
 * @author prunge
 */
public class TransformerPreferenceNode implements PreferenceNode
{
	private static final Logger log = LoggerFactory.getLogger(TransformerPreferenceNode.class);
	
	/**
	 * Transformers are grouped into their phases.
	 */
	private final ListMultimap<TransformPhase, PreferenceTransformer> transformers;
	
	private List<? extends TransformPhase> phaseOrder = ImmutableList.<TransformPhase>builder()
														.add(StandardTransformPhase.PRE_DATATYPE, StandardTransformPhase.DATATYPE, StandardTransformPhase.POST_DATATYPE, StandardTransformPhase.STORE_VALIDATION)
														.build();
	
	private final PreferenceNode node;
	
	/**
	 * Creates a <code>TransformerPreferenceNode</code> that uses an underlying preference node for storage.
	 * <p>
	 * 
	 * Transformers can be registered through {@link #addTransformer(PreferenceTransformer, TransformPhase)}.
	 * 
	 * @param node the underlying preference node to use for storage and retrieval of values.
	 * 
	 * @throws NullPointerException if <code>node</code> is null.
	 */
	public TransformerPreferenceNode(PreferenceNode node)
	{
		this(node, ArrayListMultimap.<TransformPhase, PreferenceTransformer>create());
		
		if (node == null)
			throw new NullPointerException("node == null");
	}
	
	/**
	 * Creates a <code>TransformerPreferenceNode</code> that uses an existing transformer map.
	 * 
	 * @param node the preference node to use for storage and retrieval of values.
	 * @param transformers the transformers to use.  This preference node will use this reference and if additional transformers are registered this map will
	 * 			be modified.
	 */
	private TransformerPreferenceNode(PreferenceNode node, ListMultimap<TransformPhase, PreferenceTransformer> transformers)
	{
		this.node = node;
		
		//Intentionally copying the multimap reference so that all transformed nodes spawned from this one are updated when transformers change
		this.transformers = transformers;
	}
	
	/**
	 * Registeres a transformer.
	 * 
	 * @param transformer the transformer to register.
	 * @param phase the phase to register the transformer in.
	 * 
	 * @throws NullPointerException if <code>transformer</code> or <code>phase</code> is null.
	 */
	public void addTransformer(PreferenceTransformer transformer, TransformPhase phase)
	{
		if (transformer == null)
			throw new NullPointerException("transformer == null");
		if (phase == null)
			throw new NullPointerException("phase == null");
		
		transformers.put(phase, transformer);
	}

	@Override
	public boolean isDataTypeSupported(PreferenceKeyMetadata<?> keyType) throws PreferencesException
	{
		return(isDataTypeSupportedCustomEndpoint(keyType, new NativeStoreEndpoint()));
	}
	
	private boolean isDataTypeSupportedCustomEndpoint(PreferenceKeyMetadata<?> keyType, TransformDataTypeSupportChain endpoint) 
	throws PreferencesException
	{
		//Start with all the phases
		CurrentTransformDataTypeSupportChain chain = new CurrentTransformDataTypeSupportChain(phaseOrder, endpoint);
		return(chain.isDataTypeSupported(keyType));
	}

	@Override
	public <T> T getValue(String key, PreferenceKeyMetadata<T> keyMetadata) throws UnsupportedDataTypeException, PreferencesException
	{
		return(getValueWithRestartedChain(key, keyMetadata, new NativeStoreEndpoint()));
	}
	
	private <T> T getValueWithRestartedChain(String key, PreferenceKeyMetadata<T> keyMetadata, TransformGetChain endpoint) 
	throws UnsupportedDataTypeException, PreferencesException
	{
		//Start with all the phases
		CurrentTransformGetChain chain = new CurrentTransformGetChain(phaseOrder, endpoint);
		return(chain.getValue(key, keyMetadata));
	}

	@Override
	public <T> void putValue(String key, T value, PreferenceKeyMetadata<T> keyMetadata) throws UnsupportedDataTypeException, PreferencesException
	{
		putValueWithRestartedChain(key, value, keyMetadata, new NativeStoreEndpoint());
	}
	
	private <T> void putValueWithRestartedChain(String key, T value, PreferenceKeyMetadata<T> keyMetadata, TransformPutChain endpoint) 
	throws UnsupportedDataTypeException, PreferencesException
	{
		//Start with all the phases
		CurrentTransformPutChain chain = new CurrentTransformPutChain(phaseOrder, endpoint);
		chain.putValue(key, value, keyMetadata);
	}

	@Override
	public <T> void removeValue(String key, PreferenceKeyMetadata<T> keyMetadata) throws UnsupportedDataTypeException, PreferencesException
	{
		removeValueWithRestartedChain(key, keyMetadata, new NativeStoreEndpoint());
	}
	
	private <T> void removeValueWithRestartedChain(String key, PreferenceKeyMetadata<T> keyMetadata, TransformRemoveChain endpoint) 
	throws UnsupportedDataTypeException, PreferencesException
	{
		//Start with all the phases
		CurrentTransformRemoveChain chain = new CurrentTransformRemoveChain(phaseOrder, endpoint);
		chain.removeValue(key, keyMetadata);
	}

	@Override
	public PreferenceNode getChildNode(String name) throws PreferencesException
	{
		PreferenceNode childNode = node.getChildNode(name);
		return(new TransformerPreferenceNode(childNode, transformers));
	}

	@Override
	public void removeChildNode(String name) throws PreferencesException
	{
		node.removeChildNode(name);
	}
	
	@Override
	public void removeAllValues() throws PreferencesException
	{
		node.removeAllValues();
	}

	/**
	 * Returns a set of key names that exist in the <i>underlying</i> store.  This set of key names is not affected by any transforms.
	 * <p>
	 * 
	 * Because of how transforms work, just because a paricular key name does not exist in the returned set does not mean that retrieving a value for that
	 * key will not return a value.  For transforms that transform key names as well as values, the set returned from this method does not reflect the
	 * values that can actually be retrieved from the transform-backed store.
	 * <p>
	 * 
	 * The returned set is unmodifiable.
	 * 
	 * @return a set of key names.
	 */
	@Override
	public Set<String> getKeyNames() throws PreferencesException
	{
		//This might not accurately reflect all the key names available with transforms, but impossible to calculate without key metadata
		return(node.getKeyNames());
	}

	@Override
	public Set<String> getNodeNames() throws PreferencesException
	{
		return(node.getNodeNames());
	}
	
	@Override
	public void flush() throws PreferencesException
	{
		node.flush();
	}
	
	@Override
	public void close() throws PreferencesException
	{
		node.close();
	}
	
	/**
	 * A chain endpoint that always hits the underlying preference node.
	 * 
	 * @author prunge
	 */
	private class NativeStoreEndpoint implements TransformGetChain, TransformPutChain, TransformRemoveChain
	{
		@Override
		public boolean isDataTypeSupported(PreferenceKeyMetadata<?> keyMetadata) throws PreferencesException
		{
			return(node.isDataTypeSupported(keyMetadata));
		}
		
		@Override
		public boolean isDataTypeSupportedNatively(PreferenceKeyMetadata<?> keyMetadata) throws PreferencesException
		{
			return(node.isDataTypeSupported(keyMetadata));
		}
		
		@Override
		public boolean isDataTypeSupportedNativelyWithRestartedChain(PreferenceKeyMetadata<?> keyMetadata, TransformDataTypeSupportChain endpoint)
		throws PreferencesException
		{
			//It's an endpoint, not a chain
			return(node.isDataTypeSupported(keyMetadata));
		}
		
		@Override
		public boolean isDataTypeSupportedWithRestartedChain(PreferenceKeyMetadata<?> keyMetadata, TransformDataTypeSupportChain endpoint)
				throws PreferencesException
		{
			//It's an endpoint, not a chain
			return(node.isDataTypeSupported(keyMetadata));
		}
		
		@Override
		public <T> T getValue(String key, PreferenceKeyMetadata<T> keyMetadata) throws UnsupportedDataTypeException, PreferencesException
		{
			return(node.getValue(key, keyMetadata));
		}
		
		@Override
		public <T> T getValueWithRestartedChain(String key, PreferenceKeyMetadata<T> keyMetadata, TransformGetChain newEndpoint)
		throws UnsupportedDataTypeException, PreferencesException
		{
			return(getValue(key, keyMetadata));
		}
		
		@Override
		public <T> void putValue(String key, T value, PreferenceKeyMetadata<T> keyMetadata) throws UnsupportedDataTypeException, PreferencesException
		{
			node.putValue(key, value, keyMetadata);
		}
		
		@Override
		public <T> void putValueWithRestartedChain(String key, T value, PreferenceKeyMetadata<T> keyMetadata, TransformPutChain newEndpoint)
				throws UnsupportedDataTypeException, PreferencesException
		{
			putValue(key, value, keyMetadata);
		}
		
		@Override
		public <T> void removeValue(String key, PreferenceKeyMetadata<T> keyMetadata) throws PreferencesException
		{
			node.removeValue(key, keyMetadata);
		}
		
		@Override
		public <T> void removeValueWithRestartedChain(String key, PreferenceKeyMetadata<T> keyMetadata, TransformRemoveChain newEndpoint)
		throws PreferencesException
		{
			removeValue(key, keyMetadata);			
		}
	}
	
	/**
	 * For a currently executing transform, this chain provides the ability for transforms to call other transforms.  Used for data type support 
	 * queries.
	 * 
	 * @author prunge
	 */
	private class CurrentTransformDataTypeSupportChain implements TransformDataTypeSupportChain
	{
		private final List<? extends TransformPhase> remainingPhases;
		private final TransformDataTypeSupportChain endpoint;
		
		/**
		 * Executions already processed or in-progress of processing.  Used to avoid infinite recursion.
		 */
		private final Set<DataTypeChainExecution> processedExecutions;
		
		/**
		 * Caches data type support results to avoid expensive multiple lookups.
		 */
		private final Map<PreferenceKeyMetadata<?>, Boolean> executionResultMap;
		
		public CurrentTransformDataTypeSupportChain(List<? extends TransformPhase> remainingPhases, TransformDataTypeSupportChain endpoint)
		{
			this(remainingPhases, endpoint, new HashSet<DataTypeChainExecution>(), new HashMap<PreferenceKeyMetadata<?>, Boolean>());
		}
		
		public CurrentTransformDataTypeSupportChain(List<? extends TransformPhase> remainingPhases, TransformDataTypeSupportChain endpoint, Set<DataTypeChainExecution> processedExecutions, Map<PreferenceKeyMetadata<?>, Boolean> executionResultMap)
		{
			this.remainingPhases = ImmutableList.copyOf(remainingPhases);
			this.endpoint = endpoint;
			this.processedExecutions = processedExecutions;
			this.executionResultMap = executionResultMap;
		}
		
		@Override
		public boolean isDataTypeSupported(PreferenceKeyMetadata<?> keyMetadata) throws PreferencesException
		{
			//If we have the result cached then use that
			Boolean cachedResult = executionResultMap.get(keyMetadata);
			if (cachedResult != null)
			{
				log.debug("isDataTypeSupported() cached result: " + cachedResult);
				return(cachedResult);
			}
			
			//Keep going until we hit a transform that actually does something
			for (int i = 0; i < remainingPhases.size(); i++)
			{
				TransformPhase phase = remainingPhases.get(i);
				CurrentTransformDataTypeSupportChain chainForThisPhase = new CurrentTransformDataTypeSupportChain(remainingPhases.subList(i, remainingPhases.size()), endpoint, processedExecutions, executionResultMap);
				
				for (PreferenceTransformer transformer : transformers.get(phase))
				{
					//To avoid infinite loops don't execute one we've already done
					DataTypeChainExecution curExecution = new DataTypeChainExecution(keyMetadata, transformer);
					if (!processedExecutions.contains(curExecution))
					{
						processedExecutions.add(curExecution);
						
						log.debug("Attempting transform with " + transformer);
						DataTypeSupport supportResult = transformer.applyDataTypeSupport(keyMetadata, chainForThisPhase);
						
						//If the transformer executed, it's chain will do the rest of the work
						if (supportResult != null)
						{
							log.debug("Transformer " + transformer + " produced a value.");
							switch (supportResult)
							{
								case ADD_SUPPORT:
									executionResultMap.put(keyMetadata, true);
									return(true);
								case BLOCK_SUPPORT:
									executionResultMap.put(keyMetadata, false);
									return(false);
								default: //Fall through to next iteration of loop if transformer did not expicitly return support value
									break;
							}
						}
						else
							log.debug("Transformer " + transformer + " did not produce a value.");
					}
					else
						log.debug("Already processed transformer " + transformer);
				}
			}
			
			//If we get here, no transformer executed so hit the store itself
			boolean endpointResult = endpoint.isDataTypeSupported(keyMetadata);
			executionResultMap.put(keyMetadata, endpointResult);
			return(endpointResult);
		}
		
		@Override
		public boolean isDataTypeSupportedNatively(PreferenceKeyMetadata<?> keyMetadata) throws PreferencesException
		{
			return(endpoint.isDataTypeSupportedNatively(keyMetadata));
		}
		
		@Override
		public boolean isDataTypeSupportedWithRestartedChain(PreferenceKeyMetadata<?> keyMetadata, TransformDataTypeSupportChain newEndpoint)
		throws PreferencesException
		{
			return(TransformerPreferenceNode.this.isDataTypeSupportedCustomEndpoint(keyMetadata, newEndpoint));
		}
		
		@Override
		public boolean isDataTypeSupportedNativelyWithRestartedChain(PreferenceKeyMetadata<?> keyMetadata, TransformDataTypeSupportChain newEndpoint)
		throws PreferencesException
		{
			return(endpoint.isDataTypeSupportedNatively(keyMetadata));
		}
	}
	
	/**
	 * For a currently executing transform, this chain provides the ability for transforms to call other transforms.  Superclass for the various
	 * retrival and storage chains.
	 * 
	 * @author prunge
	 */
	private abstract class AbstractCurrentTransformChain implements TransformDataTypeSupportChain
	{
		private final TransformDataTypeSupportChain endpoint;
		
		public AbstractCurrentTransformChain(TransformDataTypeSupportChain endpoint)
		{
			this.endpoint = endpoint;
		}
		
		@Override
		public boolean isDataTypeSupported(PreferenceKeyMetadata<?> keyMetadata) 
		throws PreferencesException 
		{
			return(TransformerPreferenceNode.this.isDataTypeSupported(keyMetadata));
		}
		
		@Override
		public boolean isDataTypeSupportedNatively(PreferenceKeyMetadata<?> keyMetadata) throws PreferencesException
		{
			return(endpoint.isDataTypeSupportedNatively(keyMetadata));
		}
		
		@Override
		public boolean isDataTypeSupportedWithRestartedChain(PreferenceKeyMetadata<?> keyMetadata, TransformDataTypeSupportChain newEndpoint)
		throws PreferencesException
		{
			return(TransformerPreferenceNode.this.isDataTypeSupportedCustomEndpoint(keyMetadata, newEndpoint));
		}
		
		@Override
		public boolean isDataTypeSupportedNativelyWithRestartedChain(PreferenceKeyMetadata<?> keyMetadata, TransformDataTypeSupportChain newEndpoint)
		throws PreferencesException
		{
			return(newEndpoint.isDataTypeSupportedNatively(keyMetadata));
		}
	}
	
	/**
	 * For a currently executing transform, this chain provides the ability for transforms to call other transforms - for put transformations.
	 * 
	 * @author prunge
	 */
	private class CurrentTransformPutChain extends AbstractCurrentTransformChain implements TransformPutChain
	{
		private final List<? extends TransformPhase> remainingPhases;
		private final TransformPutChain endpoint;
		
		public CurrentTransformPutChain(List<? extends TransformPhase> remainingPhases, TransformPutChain endpoint)
		{
			super(endpoint);
			
			this.remainingPhases = ImmutableList.copyOf(remainingPhases);
			this.endpoint = endpoint;
		}
		
		@Override
		public <T> void putValue(String key, T value, PreferenceKeyMetadata<T> keyMetadata)
		throws UnsupportedDataTypeException, PreferencesException
		{
			//Keep going until we hit a transform that actually does something
			for (int i = 0; i < remainingPhases.size(); i++)
			{
				TransformPhase phase = remainingPhases.get(i);
				CurrentTransformPutChain chainForThisPhase = new CurrentTransformPutChain(remainingPhases.subList(i, remainingPhases.size()), endpoint);
				
				for (PreferenceTransformer transformer : transformers.get(phase))
				{
					log.debug("Attempting transform with " + transformer);
					boolean executed = transformer.applyPut(key, value, keyMetadata, chainForThisPhase);
					
					//If the transformer executed, it's chain will do the rest of the work
					if (executed)
					{
						log.debug("Transformer " + transformer + " produced a value.");
						return;
					}
					else
						log.debug("Transformer " + transformer + " did not produce a value.");
				}
			}
			
			//If we get here no transformer executed in this particular chain so write to the underlying store
			endpoint.putValue(key, value, keyMetadata);
		}
		
		@Override
		public <T> void putValueWithRestartedChain(String key, T value, PreferenceKeyMetadata<T> keyMetadata, TransformPutChain newEndpoint)
		throws UnsupportedDataTypeException, PreferencesException
		{
			TransformerPreferenceNode.this.putValueWithRestartedChain(key, value, keyMetadata, newEndpoint);
		}
	}
	
	/**
	 * For a currently executing transform, this chain provides the ability for transforms to call other transforms - for remove transformations.
	 * 
	 * @author prunge
	 */
	private class CurrentTransformRemoveChain extends AbstractCurrentTransformChain implements TransformRemoveChain
	{
		private final List<? extends TransformPhase> remainingPhases;
		private final TransformRemoveChain endpoint;
		
		public CurrentTransformRemoveChain(List<? extends TransformPhase> remainingPhases, TransformRemoveChain endpoint)
		{
			super(endpoint);
			this.remainingPhases = ImmutableList.copyOf(remainingPhases);
			this.endpoint = endpoint;
		}
		
		@Override
		public <T> void removeValue(String key, PreferenceKeyMetadata<T> keyMetadata)
		throws UnsupportedDataTypeException, PreferencesException
		{
			//Keep going until we hit a transform that actually does something
			for (int i = 0; i < remainingPhases.size(); i++)
			{
				TransformPhase phase = remainingPhases.get(i);
				CurrentTransformRemoveChain chainForThisPhase = new CurrentTransformRemoveChain(remainingPhases.subList(i, remainingPhases.size()), endpoint);
				
				for (PreferenceTransformer transformer : transformers.get(phase))
				{
					log.debug("Attempting transform with " + transformer);
					boolean executed = transformer.applyRemove(key, keyMetadata, chainForThisPhase);
					
					//If the transformer executed, it's chain will do the rest of the work
					if (executed)
					{
						log.debug("Transformer " + transformer + " produced a value.");
						return;
					}
					else
						log.debug("Transformer " + transformer + " did not produce a value.");
				}
			}
			
			//If we get here no transformer executed in this particular chain so write to the underlying store
			endpoint.removeValue(key, keyMetadata);
		}
		
		@Override
		public <T> void removeValueWithRestartedChain(String key, PreferenceKeyMetadata<T> keyMetadata, TransformRemoveChain newEndpoint) throws PreferencesException
		{
			TransformerPreferenceNode.this.removeValueWithRestartedChain(key, keyMetadata, newEndpoint);
		}
	}
	
	/**
	 * For a currently executing transform, this chain provides the ability for transforms to call other transforms - for get transformations.
	 * 
	 * @author prunge
	 */
	private class CurrentTransformGetChain extends AbstractCurrentTransformChain implements TransformGetChain
	{
		private final List<? extends TransformPhase> remainingPhases;
		private final TransformGetChain endpoint;
		
		public CurrentTransformGetChain(List<? extends TransformPhase> remainingPhases, TransformGetChain endpoint)
		{
			super(endpoint);
			this.remainingPhases = ImmutableList.copyOf(remainingPhases);
			this.endpoint = endpoint;
		}
		
		@Override
		public <T> T getValue(String key, PreferenceKeyMetadata<T> keyMetadata) 
		throws UnsupportedDataTypeException, PreferencesException
		{
			//Keep going until we hit a transform that actually does something
			for (int i = 0; i < remainingPhases.size(); i++)
			{
				TransformPhase phase = remainingPhases.get(i);
				CurrentTransformGetChain chainForThisPhase = new CurrentTransformGetChain(remainingPhases.subList(i, remainingPhases.size()), endpoint);
				
				for (PreferenceTransformer transformer : transformers.get(phase))
				{
					log.debug("Attempting transform with " + transformer);
					TransformResult<T> result = transformer.applyGet(key, keyMetadata, chainForThisPhase);
					
					//If the transformer executed, it's chain will do the rest of the work
					if (result != null)
					{
						log.debug("Transformer " + transformer + " produced a value.");
						return(result.getValue());
					}
					else
						log.debug("Transformer " + transformer + " did not produce a value.");
				}
			}
			
			//If we get here, no transformer executed so hit the store itself
			return(endpoint.getValue(key, keyMetadata));
		}
		
		@Override
		public <T> T getValueWithRestartedChain(String key, PreferenceKeyMetadata<T> keyMetadata, TransformGetChain newEndpoint)
		throws UnsupportedDataTypeException, PreferencesException
		{
			return(TransformerPreferenceNode.this.getValueWithRestartedChain(key, keyMetadata, newEndpoint));
		}
	}

	/**
	 * Holds both key metadata and a transformer instance and can be used as a key in a hash map.
	 * 
	 * @author prunge
	 */
	private static class DataTypeChainExecution
	{
		private final PreferenceKeyMetadata<?> keyMetadata;
		private final PreferenceTransformer transformer;
		
		public DataTypeChainExecution(PreferenceKeyMetadata<?> keyMetadata, PreferenceTransformer transformer)
		{
			this.keyMetadata = keyMetadata;
			this.transformer = transformer;
		}

		@Override
		public int hashCode()
		{
			final int prime = 31;
			int result = 1;
			result = prime * result + keyMetadata.hashCode();
			result = prime * result + transformer.hashCode();
			return result;
		}

		@Override
		public boolean equals(Object obj)
		{
			if (this == obj)
				return true;
			if (obj == null)
				return false;
			if (getClass() != obj.getClass())
				return false;
			DataTypeChainExecution other = (DataTypeChainExecution) obj;
			if (!keyMetadata.equals(other.keyMetadata))
				return false;
			if (!transformer.equals(other.transformer))
				return false;
			return true;
		}
		
		
	}
}
