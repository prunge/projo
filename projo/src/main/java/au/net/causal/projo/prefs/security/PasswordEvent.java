package au.net.causal.projo.prefs.security;

import java.util.EventObject;

/**
 * An event that occurs when the user does something with a password.
 * 
 * @author prunge
 */
public class PasswordEvent extends EventObject
{
	private final JPasswordPane pane;
	
	/**
	 * Creates a <code>PasswordEvent</code>.
	 * 
	 * @param source the source of the event.
	 */
	public PasswordEvent(JPasswordPane source)
	{
		super(source);
		this.pane = source;
	}
	
	/**
	 * Returns the same as {@link #getSource()} but with appropriate type.
	 * 
	 * @return the source of the event.
	 */
	public JPasswordPane getPasswordPaneSource()
	{
		return(pane);
	}

}
