package au.net.causal.projo.prefs.transform;

import java.net.URI;
import java.net.URISyntaxException;

import au.net.causal.projo.prefs.PreferencesException;

/**
 * Converts between {@link URI} data type and string.
 * 
 * @author prunge
 */
public class UriToStringTransformer extends GenericToStringTransformer<URI>
{
	public UriToStringTransformer()
	{
		super(URI.class);
	}
	
	@Override
	protected URI stringToValue(String s) throws PreferencesException
	{
		try
		{
			return(new URI(s));
		}
		catch (URISyntaxException e)
		{
			throw new PreferencesException("Failed to convert value '" + s + "' to a URI.", e);
		}
	}
	
	@Override
	protected String valueToString(URI value) throws PreferencesException
	{
		return(value.toString());
	}
}
