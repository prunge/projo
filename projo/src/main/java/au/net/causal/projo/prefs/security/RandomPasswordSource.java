package au.net.causal.projo.prefs.security;

import java.security.SecureRandom;

import au.net.causal.projo.prefs.PreferencesException;

import com.google.common.io.BaseEncoding;

/**
 * A random password generator, generating secure-random base64 encoded passwords.
 * <p>
 * 
 * Useful when a master password is needed for being encrypted by the native OS and no user interaction is desired.
 * 
 * @author prunge
 */
public class RandomPasswordSource implements PasswordSource
{
	private int passwordLengthInBytes;
	private final BaseEncoding base64Encoding = BaseEncoding.base64().omitPadding();
	
	/**
	 * Creates a <code>RandomPasswordSource</code> with a default length.
	 */
	public RandomPasswordSource()
	{
		this(32);
	}
	
	/**
	 * Creates a <code>RandomPasswordSource</code> with the specified length.
	 * 
	 * @param passwordLengthInBytes the length of the random password in bytes.  This is not the base64 length.
	 */
	public RandomPasswordSource(int passwordLengthInBytes)
	{
		this.passwordLengthInBytes = passwordLengthInBytes;
	}

	@Override
	public char[] readPassword(Mode mode) throws PreferencesException
	{
		SecureRandom random = new SecureRandom();
		byte[] passwordBuf = new byte[getPasswordLengthInBytes()];
		random.nextBytes(passwordBuf);
		
		//Encode as base64
		//TODO once again, password finds its way as a string, eventually make this more 'secure' (but is it seriously worth it?)
		String s = base64Encoding.encode(passwordBuf);
		
		return(s.toCharArray());
	}
	
	/**
	 * Returns the length of newly generated passwords in bytes.
	 * 
	 * @return the password length.
	 * 
	 * @see #setPasswordLengthInBytes(int)
	 */
	public int getPasswordLengthInBytes()
	{
		return(passwordLengthInBytes);
	}

	/**
	 * Sets the length that newly generated passwords will be, in bytes.
	 * 
	 * @param passwordLengthInBytes the length of newly generated passwords.
	 * 
	 * @see #getPasswordLengthInBytes()
	 */
	public void setPasswordLengthInBytes(int passwordLengthInBytes)
	{
		this.passwordLengthInBytes = passwordLengthInBytes;
	}

}
