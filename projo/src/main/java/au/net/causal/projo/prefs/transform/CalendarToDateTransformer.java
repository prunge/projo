package au.net.causal.projo.prefs.transform;

import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

import au.net.causal.projo.prefs.DataTypeSupport;
import au.net.causal.projo.prefs.PreferenceKeyMetadata;
import au.net.causal.projo.prefs.PreferencesException;
import au.net.causal.projo.prefs.TransformDataTypeSupportChain;
import au.net.causal.projo.prefs.TransformGetChain;
import au.net.causal.projo.prefs.TransformPutChain;
import au.net.causal.projo.prefs.TransformRemoveChain;
import au.net.causal.projo.prefs.TransformResult;

import com.google.common.reflect.TypeToken;

/**
 * Transforms {@link Calendar} objects to {@link Date} objects.
 * <p>
 * 
 * In combination with {@link DateToStringTransformer} (or with native store support), gives preference stores the ability to store {@link Calendar} objects.
 * 
 * @author prunge
 */
public class CalendarToDateTransformer implements PreferenceTransformer
{
	@Override
	public <T> TransformResult<T> applyGet(String key, PreferenceKeyMetadata<T> keyMetadata, TransformGetChain chain) throws PreferencesException
	{
		if (!isSupported(keyMetadata, chain))
			return(null);
		
		//Read as string from underlying store, convert to Long and pass up
		Date storeValue = chain.getValue(key, keyMetadata.withDataType(Date.class));
		
		Calendar cValue;
		if (storeValue == null)
			cValue = null;
		else
		{
			cValue = new GregorianCalendar();
			cValue.setTime(storeValue);
		}
		
		//Safe because isSupported() ensured that T is only of File type
		@SuppressWarnings("unchecked")
		TransformResult<T> result = new TransformResult<>((T)cValue);
		
		return(result);
	}

	@Override
	public <T> boolean applyPut(String key, T value, PreferenceKeyMetadata<T> keyMetadata, TransformPutChain chain) throws PreferencesException
	{
		if (!isSupported(keyMetadata, chain))
			return(false);
		
		//The value must be of Calendar type if we get here
		Calendar cValue = (Calendar)value;
		
		//Convert to Date
		Date dValue;
		if (cValue == null)
			dValue = null;
		else
			dValue = cValue.getTime();
		
		chain.putValue(key, dValue, keyMetadata.withDataType(Date.class));
		
		return(true);
	}

	@Override
	public <T> boolean applyRemove(String key, PreferenceKeyMetadata<T> keyMetadata, TransformRemoveChain chain) throws PreferencesException
	{
		if (!isSupported(keyMetadata, chain))
			return(false);
		
		chain.removeValue(key, keyMetadata.withDataType(Date.class));
		
		return(true);
	}

	@Override
	public DataTypeSupport applyDataTypeSupport(PreferenceKeyMetadata<?> keyMetadata, TransformDataTypeSupportChain chain) throws PreferencesException
	{
		if (!keyMetadata.getDataType().equals(TypeToken.of(Calendar.class)))
			return(null);
		if (!chain.isDataTypeSupported(keyMetadata.withDataType(Date.class)))
			return(null);
		
		return(DataTypeSupport.ADD_SUPPORT);
	}

	/**
	 * Supported when:
	 * 
	 * <ul>
	 * 	<li>key is of {@link Calendar} type</li>
	 * 	<li>{@link Calendar} type is not supported natively by the store</li>
	 * 	<li>{@link Date} is supported by the chain</li>
	 * </ul>
	 */
	private boolean isSupported(PreferenceKeyMetadata<?> keyMetadata, TransformDataTypeSupportChain chain)
	throws PreferencesException
	{
		if (!keyMetadata.getDataType().equals(TypeToken.of(Calendar.class)))
			return(false);
		if (chain.isDataTypeSupportedNatively(keyMetadata.withDataType(Calendar.class)))
			return(false);
		if (!chain.isDataTypeSupported(keyMetadata.withDataType(Date.class)))
			return(false);
		
		return(true);
	}
}
