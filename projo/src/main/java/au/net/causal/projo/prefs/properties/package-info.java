/**
 * A Projo prefs implementation based on property files.
 * 
 * @author prunge
 */
package au.net.causal.projo.prefs.properties;