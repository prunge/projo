package au.net.causal.projo.prefs.transform;

import au.net.causal.projo.prefs.PreferencesException;

/**
 * Converts between boolean values and strings.
 * 
 * @author prunge
 */
public class BooleanToStringTransformer extends GenericToStringTransformer<Boolean>
{
	public BooleanToStringTransformer()
	{
		super(Boolean.class);
	}
	
	@Override
	protected Boolean stringToValue(String s) throws PreferencesException
	{
		if (s == null)
			return(null);
		
		return(Boolean.valueOf(s));
	}
	
	@Override
	protected String valueToString(Boolean value) throws PreferencesException
	{
		if (value == null)
			return(null);
		
		return(value.toString());
	}
}
