package au.net.causal.projo.prefs.transform;

import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;

import au.net.causal.projo.prefs.DataTypeSupport;
import au.net.causal.projo.prefs.PreferenceKeyMetadata;
import au.net.causal.projo.prefs.PreferencesException;
import au.net.causal.projo.prefs.TransformDataTypeSupportChain;
import au.net.causal.projo.prefs.TransformGetChain;
import au.net.causal.projo.prefs.TransformPutChain;
import au.net.causal.projo.prefs.TransformRemoveChain;
import au.net.causal.projo.prefs.TransformResult;

import com.google.common.reflect.TypeToken;

/**
 * Converts between strings and byte arrays using UTF-8 character encoding.
 * 
 * @author prunge
 */
public class StringToByteTransformer implements PreferenceTransformer
{
	private Charset encodingCharset = StandardCharsets.UTF_8;
	
	@Override
	public <T> TransformResult<T> applyGet(String key, PreferenceKeyMetadata<T> keyMetadata, TransformGetChain chain)
	throws PreferencesException
	{
		if (!isSupported(keyMetadata, chain))
			return(null);
		
		//Read as string from underlying store, convert to integer and pass up
		byte[] storeValue = chain.getValue(key, keyMetadata.withDataType(byte[].class));
		
		String strValue;
		if (storeValue == null)
			strValue = null;
		else
			strValue = new String(storeValue, encodingCharset);
		
		//Safe because isSupported() ensured that T is only Integer
		@SuppressWarnings("unchecked")
		TransformResult<T> result = new TransformResult<>((T)strValue);
		
		return(result);
	}
	
	@Override
	public <T> boolean applyPut(String key, T value, PreferenceKeyMetadata<T> keyMetadata, TransformPutChain chain)
	throws PreferencesException
	{
		if (!isSupported(keyMetadata, chain))
			return(false);
		
		//The value must be an String if we get here
		String strValue = (String)value;
		
		//Convert to byte[]
		byte[] storeValue;
		if (strValue == null)
			storeValue = null;
		else
			storeValue = strValue.getBytes(encodingCharset);
		
		chain.putValue(key, storeValue, keyMetadata.withDataType(byte[].class));
		
		return(true);
	}
	
	@Override
	public <T> boolean applyRemove(String key, PreferenceKeyMetadata<T> keyMetadata, TransformRemoveChain chain)
	throws PreferencesException
	{
		if (!isSupported(keyMetadata, chain))
			return(false);
		
		chain.removeValue(key, keyMetadata.withDataType(byte[].class));
		
		return(true);
	}
	
	@Override
	public DataTypeSupport applyDataTypeSupport(PreferenceKeyMetadata<?> keyMetadata, TransformDataTypeSupportChain chain)
	throws PreferencesException
	{
		//Only for String keys
		if (!keyMetadata.getDataType().equals(TypeToken.of(String.class)))
			return(null);
		
		//If underlying store supports byte arrays then add support, otherwise we can't touch it
		if (!chain.isDataTypeSupported(keyMetadata.withDataType(byte[].class)))
			return(null);
		
		//If we get here the transform will work
		return(DataTypeSupport.ADD_SUPPORT);
	}

	/**
	 * Only use this transformer if:
	 * 
	 * <ul>
	 * 	<li>Store does not support String data type natively</li>
	 * 	<li>Store supports byte array data type</li>
	 * 	<li>Key is a String data type</li>
	 * </ul>
	 * 
	 * @throws PreferencesException if the underlying store fails to retrieve data type support information.
	 */
	private boolean isSupported(PreferenceKeyMetadata<?> keyMetadata, TransformDataTypeSupportChain chain)
	throws PreferencesException
	{
		if (!keyMetadata.getDataType().equals(TypeToken.of(String.class)))
			return(false);
		if (chain.isDataTypeSupportedNatively(keyMetadata.withDataType(String.class)))
			return(false);
		if (!chain.isDataTypeSupported(keyMetadata.withDataType(byte[].class)))
			return(false);
		
		return(true);
	}
}
