package au.net.causal.projo.prefs.transform;

import java.math.BigInteger;

import au.net.causal.projo.prefs.PreferencesException;

/**
 * Converts between {@link BigInteger} values and strings.
 * 
 * @author prunge
 */
public class BigIntegerToStringTransformer extends GenericToStringTransformer<BigInteger>
{
	public BigIntegerToStringTransformer()
	{
		super(BigInteger.class);
	}
	
	@Override
	protected BigInteger stringToValue(String s) throws PreferencesException
	{
		try
		{
			return(new BigInteger(s));
		}
		catch (NumberFormatException e)
		{
			throw new PreferencesException("Failed to convert value '" + s + "' to an integer.", e);
		}
	}
	
	@Override
	protected String valueToString(BigInteger value) throws PreferencesException
	{
		return(value.toString());
	}
}
