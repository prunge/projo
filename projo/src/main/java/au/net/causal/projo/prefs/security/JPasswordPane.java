package au.net.causal.projo.prefs.security;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Container;
import java.awt.Dialog;
import java.awt.Frame;
import java.awt.Window;
import java.awt.event.ActionEvent;
import java.awt.event.ComponentAdapter;
import java.awt.event.ComponentEvent;
import java.awt.event.KeyEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.util.Arrays;
import java.util.Locale;

import javax.swing.AbstractAction;
import javax.swing.Action;
import javax.swing.BorderFactory;
import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JPasswordField;
import javax.swing.JRootPane;
import javax.swing.KeyStroke;
import javax.swing.SwingUtilities;
import javax.swing.UIManager;
import javax.swing.border.Border;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;

import org.apache.commons.lang3.StringUtils;

/**
 * A Swing password pane that prompts the user to enter a password.
 * 
 * @author prunge
 */
public class JPasswordPane extends JPanel
{
	private JLabel detailLabel;
	private JPanel detailPanel;
	private JLabel passwordLabel;
	private JPasswordField passwordField;
	private JLabel confirmLabel;
	private JPasswordField confirmField;
	
	private String unmatchingConfirmPasswordMessage = "Passwords do not match";
	
	private boolean confirming;
	
	private char[] password;
	
	private final Action okAction = new OKAction();
	private final Action cancelAction = new CancelAction();
	
	private JButton okButton;
	private JButton cancelButton;
	
	private Border standardPasswordFieldBorder;
	private Border warningBorder;
	
	public static void main(String... args)
	throws Exception
	{
		UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
		
		JPasswordPane pane = new JPasswordPane();
		pane.setConfirming(true);
		pane.setDetailMessage("Please enter the master password");
		char[] result = pane.showDialog();
		
		System.out.println("Result: " + (result == null ? null : String.valueOf(result)));
		
		/*
		JPasswordPane pane = new JPasswordPane();
		pane.setConfirming(true);
		pane.setDetailMessage("Please enter the master password");
		
		JDialog dialog = pane.createDialog();
		dialog.setAlwaysOnTop(true);
		dialog.setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
		
		
		pane.addPasswordListener(new PasswordListener()
		{
			
			@Override
			public void passwordEntered(PasswordEnteredEvent event)
			{
				System.out.println("Password: " + String.valueOf(event.getPassword()));
			}
			
			@Override
			public void passwordCancelled(PasswordEvent event)
			{
				System.out.println("Password cancelled");
			}
		});
		
		dialog.setVisible(true);
		*/
	}
	
	/**
	 * Creates a <code>JPasswordPane</code>.
	 */
	public JPasswordPane()
	{
		initComponents();
		
		//Listeners for non-matching passwords
		passwordField.getDocument().addDocumentListener(new PasswordValidationListener());
		confirmField.getDocument().addDocumentListener(new PasswordValidationListener());
		
		//Defaults
		setDetailMessage(null);
		setPasswordPromptText("Password: ");
		setConfirmPromptText("Confirm password: ");
		setConfirming(false);
	}
	
	private void initComponents()
	{
		JPanel mainPane = new JPanel();
		
		GroupLayout layout = new GroupLayout(mainPane);
		layout.setAutoCreateGaps(true);
		layout.setAutoCreateContainerGaps(true);
		mainPane.setLayout(layout);
		
		detailLabel = new JLabel();
		passwordLabel = new JLabel();
		passwordField = new JPasswordField(30);
		passwordLabel.setLabelFor(passwordField);
		confirmLabel = new JLabel();
		confirmField = new JPasswordField(30);
		confirmLabel.setLabelFor(confirmField);
		standardPasswordFieldBorder = passwordField.getBorder();
		warningBorder = BorderFactory.createCompoundBorder(BorderFactory.createDashedBorder(Color.ORANGE, 4.0f, 3.0f), 
							standardPasswordFieldBorder);

		GroupLayout.SequentialGroup hGroup = layout.createSequentialGroup();
		hGroup.addGroup(layout.createParallelGroup().addComponent(passwordLabel).addComponent(confirmLabel));
		hGroup.addGroup(layout.createParallelGroup().addComponent(passwordField).addComponent(confirmField));
		layout.setHorizontalGroup(hGroup);
		
		GroupLayout.SequentialGroup vGroup = layout.createSequentialGroup();
		vGroup.addGroup(layout.createParallelGroup(Alignment.BASELINE).addComponent(passwordLabel).addComponent(passwordField));
		vGroup.addGroup(layout.createParallelGroup(Alignment.BASELINE).addComponent(confirmLabel).addComponent(confirmField));
		layout.setVerticalGroup(vGroup);
		
		detailPanel = new JPanel();
		GroupLayout detailLayout = new GroupLayout(detailPanel);
		detailPanel.setLayout(detailLayout);
		detailLayout.setAutoCreateGaps(true);
		detailLayout.setAutoCreateContainerGaps(true);
		
		GroupLayout.SequentialGroup detailHGroup = detailLayout.createSequentialGroup();
		detailHGroup.addGroup(detailLayout.createParallelGroup().addComponent(detailLabel));
		detailLayout.setHorizontalGroup(detailHGroup);
		
		GroupLayout.SequentialGroup detailVGroup = detailLayout.createSequentialGroup();
		detailVGroup.addGroup(detailLayout.createParallelGroup(Alignment.BASELINE).addComponent(detailLabel));
		detailLayout.setVerticalGroup(detailVGroup);
		
		//Buttons
		JPanel buttonPane = new JPanel();
		okButton = new JButton(okAction);
		cancelButton = new JButton(cancelAction);
		buttonPane.add(okButton);
		buttonPane.add(cancelButton);
		
		setLayout(new BorderLayout());
		add(mainPane, BorderLayout.CENTER);
		add(detailPanel, BorderLayout.NORTH);
		add(buttonPane, BorderLayout.SOUTH);
	}
	
	/**
	 * Displays the password dialog.
	 * 
	 * @return the user entered password, or <code>null</code> if the user cancelled.
	 * 
	 * @see #showDialog(Component, String)
	 */
	public char[] showDialog()
	{
		return(showDialog(null));
	}

	/**
	 * Displays the password dialog with a custom title.
	 * 
	 * @param title the dialog title.
	 * 
	 * @return the user entered password, or <code>null</code> if the user cancelled.
	 * 
	 * @see #showDialog(Component, String)
	 */
	public char[] showDialog(String title)
	{
		return(showDialog(null, title));
	}
	
	/**
	 * Displays the password dialog with a custom title over the specified parent component.  The dialog will be modal.
	 *
	 * @param parentComponent the parent component.  The dialog will be modal over the window of this component.
	 * @param title the dialog title.
	 * 
	 * @return the user entered password, or <code>null</code> if the user cancelled.
	 */
	public char[] showDialog(Component parentComponent, String title)
	{
		JDialog dialog = createDialog(parentComponent, title);
		dialog.setModal(true);
		dialog.setVisible(true);
		
		return(getPassword());
	}
	
	/**
	 * Creates a dialog with the password pane component with the default title.  This does not actually display the dialog.
	 * 
	 * @return the created dialog.
	 */
	public JDialog createDialog()
	{
		return(createDialog("Password"));
	}
	
	/**
	 * Creates a dialog with the password pane component with the specified title.  This does not actually display the dialog.
	 * 
	 * @param title the dialog title.
	 * 
	 * @return the created dialog.
	 */
	public JDialog createDialog(String title)
	{
		return(createDialog(null, title));
	}
	
	/**
	 * Creates a dialog with the password pane component with the specified title over a parent component.  This does not actually display the dialog.
	 * 
	 * @param parentComponent the parent component to use.  The dialog, when displayed, will be modal over the window of this component.
	 * @param title the dialog title.
	 * 
	 * @return the created dialog.
	 */
	public JDialog createDialog(Component parentComponent, String title)
	{
		JDialog dialog;
		Window window;
		if (parentComponent != null)
			window = SwingUtilities.getWindowAncestor(parentComponent);
		else
			window = JOptionPane.getRootFrame();
		
		if (window instanceof Frame) 
			dialog = new JDialog((Frame)window, title, true);
		else
			dialog = new JDialog((Dialog)window, title, true);
		
		initDialog(dialog, parentComponent);
		
		return(dialog);
	}
	
	private void initDialog(final JDialog dialog, Component parentComponent) 
	{
		int style = JRootPane.PLAIN_DIALOG;
		
        dialog.setComponentOrientation(this.getComponentOrientation());
        Container contentPane = dialog.getContentPane();

        contentPane.setLayout(new BorderLayout());
        contentPane.add(this, BorderLayout.CENTER);
        //dialog.setResizable(false);
        if (JDialog.isDefaultLookAndFeelDecorated()) 
        {
            boolean supportsWindowDecorations = UIManager.getLookAndFeel().getSupportsWindowDecorations();
            if (supportsWindowDecorations) 
            {
                dialog.setUndecorated(true);
                getRootPane().setWindowDecorationStyle(style);
            }
        }
        
        //OK/cancel shortcut handling
        dialog.getRootPane().setDefaultButton(okButton);
        
        getRootPane().getInputMap(JComponent.WHEN_ANCESTOR_OF_FOCUSED_COMPONENT).put(KeyStroke.getKeyStroke(KeyEvent.VK_ESCAPE, 0), "cancel");
        getRootPane().getActionMap().put("cancel", cancelAction);
        
        dialog.pack();
        dialog.setLocationRelativeTo(parentComponent);

        //A listener that closes the dialog off when the user OKs or cancels
        final PasswordListener listener = new PasswordListener()
		{
			@Override
			public void passwordEntered(PasswordEnteredEvent event)
			{
				if (dialog.isVisible())
					dialog.dispose();
			}
			
			@Override
			public void passwordCancelled(PasswordEvent event)
			{
				if (dialog.isVisible())
					dialog.dispose();
			}
		};
        
        WindowAdapter adapter = new WindowAdapter() 
        {
            private boolean gotFocus = false;
            
            @Override
            public void windowClosing(WindowEvent we) 
            {
                clearPassword();
                firePasswordCancelled();
            }

            @Override
            public void windowClosed(WindowEvent e) 
            {
                removePasswordListener(listener);
                dialog.getContentPane().removeAll();
            }

            @Override
            public void windowGainedFocus(WindowEvent we) 
            {
                // Once window gets focus, set initial focus
                if (!gotFocus) 
                {
                	focusOnPasswordField();
                	gotFocus = true;
                }
            }
        };
        dialog.addWindowListener(adapter);
        dialog.addWindowFocusListener(adapter);
        dialog.addComponentListener(new ComponentAdapter() 
        {
        	@Override
            public void componentShown(ComponentEvent ce) 
        	{
                // reset value to ensure closing works properly
                clearPassword();
            }
        });

        addPasswordListener(listener);
    }
	
	/**
	 * Attempts to focus the password field for user input.
	 */
	public void focusOnPasswordField()
	{
		passwordField.requestFocusInWindow();
	}

	/**
	 * Adds a listener that is notified when the user enters a password or cancels.
	 * 
	 * @param listener the listener to add.
	 * 
	 * @throws NullPointerException if <code>listener</code> is null.
	 */
	public void addPasswordListener(PasswordListener listener)
	{
		if (listener == null)
			throw new NullPointerException("listener == null");
		
		listenerList.add(PasswordListener.class, listener);
	}
	
	/**
	 * Removes a previously registered password listener.  If <code>listener</code> was not previously registered this method does nothing.
	 * 
	 * @param listener the listener to remove.
	 * 
	 * @throws NullPointerException if <code>listener</code> is null.
	 */
	public void removePasswordListener(PasswordListener listener)
	{
		if (listener == null)
			throw new NullPointerException("listener == null");
		
		listenerList.remove(PasswordListener.class, listener);
	}
	
	/**
	 * Dispatches a password entered event to all registered listeners.
	 * 
	 * @param password the password that was entered.
	 * 
	 * @throws NullPointerException if <code>password</code> is null.
	 */
	protected void firePasswordEntered(char[] password)
	{
		if (password == null)
			throw new NullPointerException("password == null");
		
		PasswordEnteredEvent event = new PasswordEnteredEvent(this, password);
		
		for (PasswordListener listener : getListeners(PasswordListener.class))
		{
			listener.passwordEntered(event);
		}
	}
	
	/**
	 * Dispatches a password cancelled event to all registered listeners.
	 */
	protected void firePasswordCancelled()
	{
		PasswordEvent event = new PasswordEvent(this);
		
		for (PasswordListener listener : getListeners(PasswordListener.class))
		{
			listener.passwordCancelled(event);
		}
	}
	
	/**
	 * Returns the detail message displayed in the dialog.
	 * 
	 * @return the detail message.
	 * 
	 * @see #setDetailMessage(String)
	 */
	public String getDetailMessage()
	{
		return(detailLabel.getText());
	}
	
	/**
	 * Sets the detail message displayed in the dialog.   This is typically some prompt text such as 'Please enter network password'.
	 * 
	 * @param detailMessage the detail message.  If <code>null</code> then no detail message is displayed.
	 * 
	 * @see #getDetailMessage()
	 */
	public void setDetailMessage(String detailMessage)
	{
		detailPanel.setVisible(!StringUtils.isEmpty(detailMessage));
		detailLabel.setText(detailMessage);
	}
	
	/**
	 * Returns the password field's label.
	 * 
	 * @return the password field's label.
	 * 
	 * @see #setPasswordPromptText(String)
	 */
	public String getPasswordPromptText()
	{
		return(passwordLabel.getText());
	}
	
	/**
	 * Set the password field's label.
	 * 
	 * @param passwordPromptText the label to set.
	 * 
	 * @see #getPasswordPromptText()
	 */
	public void setPasswordPromptText(String passwordPromptText)
	{
		passwordLabel.setText(passwordPromptText);
	}
	
	/**
	 * Returns the confirm password field's label.
	 * 
	 * @return the confirm password field's label.
	 * 
	 * @see #setConfirming(boolean)
	 * @see #setConfirmPromptText(String)
	 */
	public String getConfirmPromptText()
	{
		return(confirmLabel.getText());
	}
	
	/**
	 * Sets the confirm password field's label.
	 * 
	 * @param confirmPromptText the label to set.
	 * 
	 * @see #setConfirming(boolean)
	 * @see #getConfirmPromptText(String)
	 */
	public void setConfirmPromptText(String confirmPromptText)
	{
		confirmLabel.setText(confirmPromptText);
	}
	
	/**
	 * Returns whether the user is required to enter the password a second time to confirm their password.
	 * 
	 * @return true if the password must be confirmed by the user, false if not.
	 * 
	 * @see #setConfirming(boolean)
	 */
	public boolean isConfirming()
	{
		return(confirming);
	}
	
	/**
	 * Sets whether the user is required to enter the password a second time to confirm their password.
	 * 
	 * @param confirming true if the password must be confirmed, false if not.
	 * 
	 * @see #isConfirming()
	 */
	public void setConfirming(boolean confirming)
	{
		this.confirming = confirming;
		
		confirmLabel.setVisible(confirming);
		confirmField.setVisible(confirming);
	}
	
	/**
	 * Returns the password the user entered.  This value will only be filled in after the user has confirmed entering the password (e.g. clicking OK
	 * button). 
	 * 
	 * @return the entered password, or null if not entered.
	 */
	public char[] getPassword()
	{
		return(password);
	}
	
	/**
	 * Clears the password from the UI.
	 */
	public void clearPassword()
	{
		password = null;
		passwordField.setText("");
		confirmField.setText("");
		passwordsChanged();
	}
	
	/**
	 * Returns the message displayed to the user when the confirm password is different from the original one.
	 * 
	 * @return the message.
	 * 
	 * @see #setUnmatchingConfirmPasswordMessage(String)
	 */
	public String getUnmatchingConfirmPasswordMessage()
	{
		return(unmatchingConfirmPasswordMessage);
	}

	/**
	 * Sets the message displayed to the user when the confirm password entered is different from the original one.
	 * 
	 * @param unmatchingConfirmPasswordMessage the message to set.
	 * 
	 * @see #getUnmatchingConfirmPasswordMessage()
	 */
	public void setUnmatchingConfirmPasswordMessage(String unmatchingConfirmPasswordMessage)
	{
		this.unmatchingConfirmPasswordMessage = unmatchingConfirmPasswordMessage;
	}

	private void setDifferentPasswordsPromptVisible(boolean show)
	{
		if (show)
		{
			passwordField.setBorder(warningBorder);
			confirmField.setBorder(warningBorder);
			passwordField.setToolTipText(getUnmatchingConfirmPasswordMessage());
			confirmField.setToolTipText(getUnmatchingConfirmPasswordMessage());
		}
		else
		{
			passwordField.setBorder(standardPasswordFieldBorder);
			confirmField.setBorder(standardPasswordFieldBorder);
			passwordField.setToolTipText(null);
			confirmField.setToolTipText(null);
		}
	}
	
	private void passwordsChanged()
	{
		if (passwordField.getPassword().length == 0)
		{
			setDifferentPasswordsPromptVisible(false);
			
			okAction.setEnabled(false);
			return;
		}
		
		if (isConfirming() && !Arrays.equals(passwordField.getPassword(), confirmField.getPassword()))
		{
			setDifferentPasswordsPromptVisible(true);
			
			okAction.setEnabled(false);
			return;
		}
		
		//Everything's OK if we get here
		setDifferentPasswordsPromptVisible(false);
		okAction.setEnabled(true);
	}
	
	private class PasswordValidationListener implements DocumentListener
	{
		@Override
		public void changedUpdate(DocumentEvent e)
		{
			passwordsChanged();
		}
		
		@Override
		public void insertUpdate(DocumentEvent e)
		{
			passwordsChanged();
		}
		
		@Override
		public void removeUpdate(DocumentEvent e)
		{
			passwordsChanged();
		}
	}
	
	private class OKAction extends AbstractAction
	{
		public OKAction()
		{
			super();
			
			Locale locale = getLocale();
			putValue(Action.NAME, UIManager.getString("OptionPane.okButtonText", locale));
            putValue(Action.MNEMONIC_KEY, UIManager.getInt("OptionPane.okButtonMnemonic", locale));
            putValue(Action.LARGE_ICON_KEY, UIManager.getIcon("OptionPane.okIcon", locale));
		}
		
		@Override
		public void actionPerformed(ActionEvent e)
		{
			password = passwordField.getPassword();
			firePasswordEntered(getPassword());
		}
	}
	
	private class CancelAction extends AbstractAction
	{
		public CancelAction()
		{
			super();
			
			Locale locale = getLocale();
			putValue(Action.NAME, UIManager.getString("OptionPane.cancelButtonText", locale));
            putValue(Action.MNEMONIC_KEY, UIManager.getInt("OptionPane.cancelButtonMnemonic", locale));
            putValue(Action.LARGE_ICON_KEY, UIManager.getIcon("OptionPane.cancelIcon", locale));
		}
		
		@Override
		public void actionPerformed(ActionEvent e)
		{
			firePasswordCancelled();
		}
	}
}
