package au.net.causal.projo.metadata;

import au.net.causal.projo.bind.MetadataReader;

public interface SettingsMetadataReader extends MetadataReader<SettingsEntityMetadata, SettingsPropertyMetadata>
{

}
