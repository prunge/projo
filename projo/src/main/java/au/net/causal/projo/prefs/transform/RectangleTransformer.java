package au.net.causal.projo.prefs.transform;

import java.awt.Dimension;
import java.awt.Rectangle;

import au.net.causal.projo.prefs.DataTypeSupport;
import au.net.causal.projo.prefs.PreferenceKeyMetadata;
import au.net.causal.projo.prefs.PreferencesException;
import au.net.causal.projo.prefs.TransformDataTypeSupportChain;
import au.net.causal.projo.prefs.TransformGetChain;
import au.net.causal.projo.prefs.TransformPutChain;
import au.net.causal.projo.prefs.TransformRemoveChain;
import au.net.causal.projo.prefs.TransformResult;

import com.google.common.reflect.TypeToken;

/**
 * Allows storage and retrieval of {@link Rectangle} values in stores that do not natively support this data type.
 * <p>
 * 
 * The rectangle value's x, y, width and height components are stored under separate keys in the preference store with {@link Integer} data type.
 * <p>
 * 
 * This transformer will only declare support if:
 * <ul>
 * 	<li>Rectangle is not supported natively by the store</li>
 * 	<li>Key is of Rectangle type</li>
 * 	<li>Store supports (either directly or indirectly through other transformers) the {@link Integer} data type.</li>
 * </ul>
 * 
 * @author prunge
 */
public class RectangleTransformer implements PreferenceTransformer
{
	private final String xPropertySuffix = "X";
	private final String yPropertySuffix = "Y";
	private final String widthPropertySuffix = "Width";
	private final String heightPropertySuffix = "Height";
	
	@Override
	public <T> TransformResult<T> applyGet(String key, PreferenceKeyMetadata<T> keyMetadata, TransformGetChain chain)
	throws PreferencesException
	{
		if (!isSupported(keyMetadata, chain))
			return(null);
		
		//Look up the component values
		Integer x = chain.getValue(key + xPropertySuffix, keyMetadata.withDataType(Integer.class));
		Integer y = chain.getValue(key + yPropertySuffix, keyMetadata.withDataType(Integer.class));
		Integer width = chain.getValue(key + widthPropertySuffix, keyMetadata.withDataType(Integer.class));
		Integer height = chain.getValue(key + heightPropertySuffix, keyMetadata.withDataType(Integer.class));
		
		if (x == null || y == null || width == null || height == null)
			return(new TransformResult<>(null));
		
		Rectangle r = new Rectangle(x, y, width, height);
		
		@SuppressWarnings("unchecked")
		TransformResult<T> result = new TransformResult<>((T)r);
		
		return(result);
	}

	@Override
	public <T> boolean applyPut(String key, T value, PreferenceKeyMetadata<T> keyMetadata, TransformPutChain chain)
	throws PreferencesException
	{
		if (!isSupported(keyMetadata, chain))
			return(false);
		
		//Store component values
		Rectangle rValue = (Rectangle)value;
		
		Integer x, y, width, height;
		if (rValue == null)
		{
			x = null;
			y = null;
			width = null;
			height = null;
		}
		else
		{
			x = rValue.x;
			y = rValue.y;
			width = rValue.width;
			height = rValue.height;
		}
		
		PreferenceKeyMetadata<Integer> componentMetadata = keyMetadata.withDataType(Integer.class);
		chain.putValue(key + xPropertySuffix, x, componentMetadata);
		chain.putValue(key + yPropertySuffix, y, componentMetadata);
		chain.putValue(key + widthPropertySuffix, width, componentMetadata);
		chain.putValue(key + heightPropertySuffix, height, componentMetadata);
		
		return(true);
	}
	
	@Override
	public <T> boolean applyRemove(String key, PreferenceKeyMetadata<T> keyMetadata, TransformRemoveChain chain)
			throws PreferencesException
	{
		if (!isSupported(keyMetadata, chain))
			return(false);
		
		PreferenceKeyMetadata<Integer> componentMetadata = keyMetadata.withDataType(Integer.class);
		chain.removeValue(key + xPropertySuffix, componentMetadata);
		chain.removeValue(key + yPropertySuffix, componentMetadata);
		chain.removeValue(key + widthPropertySuffix, componentMetadata);
		chain.removeValue(key + heightPropertySuffix, componentMetadata);
		
		return(true);
	}
	
	@Override
	public DataTypeSupport applyDataTypeSupport(PreferenceKeyMetadata<?> keyMetadata, TransformDataTypeSupportChain chain)
	throws PreferencesException
	{
		//Only for Rectangle keys
		if (!keyMetadata.getDataType().equals(TypeToken.of(Rectangle.class)))
			return(null);
		
		//If underlying store supports integers then add support, otherwise we can't touch it
		if (!chain.isDataTypeSupported(keyMetadata.withDataType(Integer.class)))
			return(null);
		
		//If we get here the transform will work
		return(DataTypeSupport.ADD_SUPPORT);
	}

	/**
	 * Only use this transformer if:
	 * 
	 * <ul>
	 * 	<li>Store does not support {@link Rectangle} data type natively</li>
	 * 	<li>Store supports {@link Integer} data type</li>
	 * 	<li>Key is a Rectangle data type</li>
	 * </ul>
	 * 
	 * @throws PreferencesException if the underlying store fails to retrieve data type support information.
	 */
	private boolean isSupported(PreferenceKeyMetadata<?> keyMetadata, TransformDataTypeSupportChain chain)
	throws PreferencesException
	{
		if (!TypeToken.of(Rectangle.class).isAssignableFrom(keyMetadata.getDataType()))
			return(false);
		if (chain.isDataTypeSupportedNatively(keyMetadata.withDataType(Rectangle.class)))
			return(false);
		if (!chain.isDataTypeSupported(keyMetadata.withDataType(Integer.class)))
			return(false);
		
		return(true);
	}
}
