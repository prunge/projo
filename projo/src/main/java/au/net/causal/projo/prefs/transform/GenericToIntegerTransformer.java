package au.net.causal.projo.prefs.transform;

import java.math.BigInteger;
import java.util.List;

import au.net.causal.projo.prefs.DataTypeSupport;
import au.net.causal.projo.prefs.PreferenceKeyMetadata;
import au.net.causal.projo.prefs.PreferencesException;
import au.net.causal.projo.prefs.TransformDataTypeSupportChain;
import au.net.causal.projo.prefs.TransformGetChain;
import au.net.causal.projo.prefs.TransformPutChain;
import au.net.causal.projo.prefs.TransformRemoveChain;
import au.net.causal.projo.prefs.TransformResult;

import com.google.common.collect.ImmutableList;
import com.google.common.primitives.Primitives;
import com.google.common.reflect.TypeToken;

/**
 * Superclass to make it easier to convert from arbitrary data types to an numeric integer data type.
 * <p>
 * 
 * This transformer will be supported only if the store has support for {@link Integer}, {@link Long} or {@link BigInteger} (which might be through other 
 * transformers).
 * <p>
 * 
 * Typical use of this class involves subclassing this transformer, specifying a specific data type for generic parameter <code>D</code>, and implementing the
 * conversion methods {@link #integerToValue(int)} and {@link #valueToInteger(Object)}.
 * 
 * @author prunge
 *
 * @param <D> the data type to convert from.
 */
public abstract class GenericToIntegerTransformer<D> implements PreferenceTransformer
{
	//List is ordered, we try these types in order so we prefer the ones with the lower index
	private static final List<NumericDestinationType> DESTINATION_TYPES = ImmutableList.of(NumericDestinationType.INTEGER, NumericDestinationType.LONG, NumericDestinationType.BIGINTEGER);
	
	private final Class<D> dataType;
	private final Class<?> primitiveDataType;
	private final boolean allowSubclasses;
	
	/**
	 * Creates a <code>GenericToIntegerTransformer</code> that converts from the specified data type to a numeric integer store data type.
	 * 
	 * @param dataType the data type to convert from.
	 * 
	 * @throws NullPointerException if <code>dataType</code> is null.
	 */
	protected GenericToIntegerTransformer(Class<D> dataType)
	{
		this(dataType, false);
	}
	
	/**
	 * Creates a <code>GenericToIntegerTransformer</code> that converts from the specified data type to a numeric integer store data type, also optionally
	 * supporting subclasses of <code>dataType</code> as well.
	 * 
	 * @param dataType the data type to convert from.
	 * @param allowSubclasses true if keys that are subclasses of <code>dataType</code> should be processed by the transformer as well, false if not.
	 * 
	 * @throws NullPointerException if <code>dataType</code> is null.
	 */
	protected GenericToIntegerTransformer(Class<D> dataType, boolean allowSubclasses)
	{
		if (dataType == null)
			throw new NullPointerException("dataType == null");
		
		if (dataType.isPrimitive())
		{
			this.dataType = Primitives.wrap(dataType);
			this.primitiveDataType = dataType;
		}
		else
		{
			this.dataType = dataType;
			if (Primitives.isWrapperType(dataType))
				this.primitiveDataType = Primitives.unwrap(dataType);
			else
				this.primitiveDataType = null;
		}
		
		this.allowSubclasses = allowSubclasses;
	}
	
	@Override
	public <T> TransformResult<T> applyGet(String key, PreferenceKeyMetadata<T> keyMetadata, TransformGetChain chain)
	throws PreferencesException
	{
		NumericDestinationType destType = getStoreSupport(keyMetadata, chain);
		if (destType == null)
			return(null);
		
		//Read as string from underlying store, convert to Long and pass up
		Number storeValue = chain.getValue(key, keyMetadata.withDataType(destType.getType()));
		
		D gValue;
		if (storeValue == null)
			gValue = null;
		else
			gValue = integerToValue(storeValue.intValue());
		
		//Safe because isSupported() ensured that T is only of generic type
		@SuppressWarnings("unchecked")
		TransformResult<T> result = new TransformResult<>((T)gValue);
		
		return(result);
	}
	
	@Override
	public <T> boolean applyPut(String key, T value, PreferenceKeyMetadata<T> keyMetadata, TransformPutChain chain)
	throws PreferencesException
	{
		NumericDestinationType destType = getStoreSupport(keyMetadata, chain);
		if (destType == null)
			return(false);
		
		//The value must be of datatype if we get here
		D gValue = dataType.cast(value);
		
		//Convert to string
		Integer iValue;
		if (gValue == null)
			iValue = null;
		else
			iValue = valueToInteger(gValue);
		
		if (iValue == null)
			chain.putValue(key, null, keyMetadata.withDataType(destType.getType()));
		else
			putValueTypeSafe(chain, key, destType.getType(), destType.fromInteger(iValue), keyMetadata);
		
		return(true);
	}
	
	private <N extends Number> void putValueTypeSafe(TransformPutChain chain, String key, Class<N> type, Number value, PreferenceKeyMetadata<?> keyMetadata)
	throws PreferencesException
	{
		chain.putValue(key, type.cast(value), keyMetadata.withDataType(type));
	}
	
	/**
	 * Converts from the declared data type to an integer value.
	 * 
	 * @param value the value to convert.  Will never be null.
	 * 
	 * @return the resulting value.
	 * 
	 * @throws PreferencesException if a conversion error occurs.
	 */
	protected abstract int valueToInteger(D value)
	throws PreferencesException;
	
	/**
	 * Converts from an integer value to the declared data type.
	 * 
	 * @param n the value to convert.
	 * 
	 * @return the converted value.
	 * 
	 * @throws PreferencesException if a conversion error occurs.
	 */
	protected abstract D integerToValue(int n)
	throws PreferencesException;
	
	@Override
	public <T> boolean applyRemove(String key, PreferenceKeyMetadata<T> keyMetadata, TransformRemoveChain chain)
	throws PreferencesException
	{
		NumericDestinationType destType = getStoreSupport(keyMetadata, chain);
		if (destType == null)
			return(false);
		
		chain.removeValue(key, keyMetadata.withDataType(destType.getType()));
		
		return(true);
	}
	
	@Override
	public DataTypeSupport applyDataTypeSupport(PreferenceKeyMetadata<?> keyMetadata, TransformDataTypeSupportChain chain)
	throws PreferencesException
	{
		//Only for keys for dataType or primitiveDataType
		boolean isApplicableDataType;
		if (keyMetadata.getDataType().equals(TypeToken.of(dataType)))
			isApplicableDataType = true;
		else if (primitiveDataType != null && keyMetadata.getDataType().equals(TypeToken.of(primitiveDataType)))
			isApplicableDataType = true;
		else if (allowSubclasses && TypeToken.of(dataType).isAssignableFrom(keyMetadata.getDataType()))
			isApplicableDataType = true;
		else
			isApplicableDataType = false;
		
		if (!isApplicableDataType)
			return(null);
		
		for (NumericDestinationType type : DESTINATION_TYPES)
		{
			if (chain.isDataTypeSupported(keyMetadata.withDataType(type.getType())))
				return(DataTypeSupport.ADD_SUPPORT);
		}
		
		//No numeric destination type supported by store
		return(null);
	}

	/**
	 * Only use this transformer if:
	 * 
	 * <ul>
	 * 	<li>Store does not support data type natively</li>
	 * 	<li>Store supports {@link Integer} or {@link Long} data types</li>
	 * 	<li>Key is for data type</li>
	 * </ul>
	 * 
	 * @return the destination type that is supported - integer, long or BigInteger.  Returns null if there is no store support for the destination type or
	 * 			if the key should not be supported for another reason.
	 * 
	 * @throws PreferencesException if the underlying store fails to retrieve data type support information.
	 */
	private NumericDestinationType getStoreSupport(PreferenceKeyMetadata<?> keyMetadata, TransformDataTypeSupportChain chain)
	throws PreferencesException
	{
		//Only for keys for dataType or primitiveDataType
		boolean isApplicableDataType;
		if (keyMetadata.getDataType().equals(TypeToken.of(dataType)))
			isApplicableDataType = true;
		else if (primitiveDataType != null && keyMetadata.getDataType().equals(TypeToken.of(primitiveDataType)))
			isApplicableDataType = true;
		else if (allowSubclasses && TypeToken.of(dataType).isAssignableFrom(keyMetadata.getDataType()))
			isApplicableDataType = true;
		else
			isApplicableDataType = false;
		
		if (!isApplicableDataType)
			return(null);
		if (chain.isDataTypeSupportedNatively(keyMetadata.withDataType(dataType)))
			return(null);
		
		for (NumericDestinationType type : DESTINATION_TYPES)
		{
			if (chain.isDataTypeSupported(keyMetadata.withDataType(type.getType())))
				return(type);
		}
		
		//No numeric destination type supported by store
		return(null);
	}
	
	/**
	 * Holds a destination integer data type as well as some conversion logic.
	 * 
	 * @author prunge
	 */
	private static enum NumericDestinationType
	{
		INTEGER(Integer.class)
		{
			@Override
			public Number fromInteger(int value)
			{
				return(value);
			}
		}, 
		
		LONG(Long.class)
		{
			@Override
			public Number fromInteger(int value)
			{
				return((long)value);
			}
		}, 
		
		BIGINTEGER(BigInteger.class)
		{
			@Override
			public Number fromInteger(int value)
			{
				return(BigInteger.valueOf(value));
			}
		};
		
		private final Class<? extends Number> type;
		
		private NumericDestinationType(Class<? extends Number> type)
		{
			this.type = type;
		}
		
		public Class<? extends Number> getType()
		{
			return(type);
		}
		
		public abstract Number fromInteger(int value);
	}
}
