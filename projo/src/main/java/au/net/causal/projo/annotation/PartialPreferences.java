package au.net.causal.projo.annotation;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * If specified on a Projo, a remove operation will remove individual keys rather than just clear out all the nodes of the Projo.
 * <p>
 * 
 *  This is useful when creating Projos with a subset of all the keys (e.g. for read/write performance).
 * 
 * @author prunge
 */
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.TYPE)
public @interface PartialPreferences
{
	
}
