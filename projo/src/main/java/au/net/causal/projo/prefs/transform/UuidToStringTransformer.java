package au.net.causal.projo.prefs.transform;

import java.util.UUID;

import au.net.causal.projo.prefs.PreferencesException;

/**
 * Converter that can store UUIDs as strings.
 * 
 * @author prunge
 */
public class UuidToStringTransformer extends GenericToStringTransformer<UUID>
{
	public UuidToStringTransformer()
	{
		super(UUID.class);
	}
	
	@Override
	protected UUID stringToValue(String s) throws PreferencesException
	{
		try
		{
			return(UUID.fromString(s));
		}
		catch (IllegalArgumentException e)
		{
			throw new PreferencesException("Failed to convert value '" + s + "' to an integer.", e);
		}
	}
	
	@Override
	protected String valueToString(UUID value) throws PreferencesException
	{
		return(value.toString());
	}
}
