package au.net.causal.projo.prefs.transform;

import java.net.URI;
import java.net.URISyntaxException;
import java.nio.file.FileSystem;
import java.nio.file.FileSystemNotFoundException;
import java.nio.file.FileSystems;
import java.nio.file.InvalidPathException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.ProviderNotFoundException;

import au.net.causal.projo.prefs.PreferencesException;

/**
 * Transformer for converting {@link Path} objects into strings suitable for writing to configuration.
 * <p>
 * 
 * There are two forms paths are written as.  The simplest form, which simply converts the path into a string using {@link Path#toString()} is used when
 * the filesystem is the default local filesystem.  For other filesystem types, a URI is written including the scheme which links back to the filesystem 
 * provider.
 * <p>
 * 
 * It is also possible to subclass this transformer and override the {@link #pathToStringForSpecialFileSystem(Path)} and {@link #stringToPathForSpecialFileSystem(String)}
 * methods to provide a more customized conversions for special filesystem types, such as TrueVFS.
 * 
 * @author prunge
 */
public class PathToStringTransformer extends GenericToStringTransformer<Path>
{
	public PathToStringTransformer()
	{
		super(Path.class);
	}

	@Override
	protected String valueToString(Path value) throws PreferencesException
	{
		//Saving
		
		//If this is the local file system then just write the path in its string form
		FileSystem fs = value.getFileSystem();
		if (FileSystems.getDefault().equals(fs))
			return(value.toString());
		
		//Special file system handling
		String result = pathToStringForSpecialFileSystem(value);
		if (result != null)
			return(result);
		
		//Other file systems not handled specially use a URI
		return(value.toUri().toString());
	}
	
	/**
	 * Performs any special filesystem/path conversion.  This method does nothing and returns null, however subclasses may override this method to perform
	 * handling on special types of filesystems or paths.
	 * 
	 * @param path the path to convert to string form.
	 * 
	 * @return the converted path, or null if no special action should be taken for this path.
	 * 
	 * @throws PreferencesException if an error occurs.
	 * 
	 * @see #stringToPathForSpecialFileSystem(String)
	 */
	protected String pathToStringForSpecialFileSystem(Path path)
	throws PreferencesException
	{
		return(null);
	}
	
	/**
	 * Performs conversion from string to path for special filesystems.  Thi smethod does nothing and returns null, however subclasses may override this method
	 * to perform handling on special types of filesystems or paths.
	 * 
	 * @param s the string to convert to path.
	 * 
	 * @return the converted string, or null if no special action should be taken for the path.
	 * 
	 * @throws PreferencesException if an error occurs.
	 * 
	 * @see #pathToStringForSpecialFileSystem(Path)
	 */
	protected Path stringToPathForSpecialFileSystem(String s)
	throws PreferencesException
	{
		return(null);
	}

	@Override
	protected Path stringToValue(String s) throws PreferencesException
	{
		//Loading
		
		//If the string looks like a URI (with a scheme that is longer than one character so it is not confused with Windows drive letters)
		//then load using custom file system
		try
		{
			URI uri = new URI(s);
			
			//So as to not confuse with drive letter
			if (uri.isAbsolute() && uri.getScheme() != null && uri.getScheme().length() > 1)
			{
				try
				{
					return(Paths.get(uri));
				}
				catch (ProviderNotFoundException | FileSystemNotFoundException e)
				{
					//Error
					throw new PreferencesException("Failed to load filesystem for URI '" + uri.toString() + "'.", e);
				}
			}
		}
		catch (URISyntaxException e)
		{
			//It's not a URI, proceed to next step
		}
		
		//Otherwise this might be specially encoded
		Path p = stringToPathForSpecialFileSystem(s);
		if (p != null)
			return(p);
		
		//If not then treat as a path from the local file system
		try
		{
			return(Paths.get(s));
		}
		catch (InvalidPathException e)
		{
			throw new PreferencesException("Invalid file path: " + s, e);
		}
	}

}
