package au.net.causal.projo.annotation;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * When specified on a nested Projo property, preference values are stored in the same node as the parent.  Otherwise a suitable child node is used to store
 * the nested Projo properties.
 * 
 * @author prunge
 */
@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.FIELD, ElementType.METHOD})
public @interface Embedded
{
}
