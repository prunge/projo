package au.net.causal.projo.prefs.transform;

import au.net.causal.projo.annotation.Secure;
import au.net.causal.projo.prefs.DataTypeSupport;
import au.net.causal.projo.prefs.PreferenceKeyMetadata;
import au.net.causal.projo.prefs.PreferencesException;
import au.net.causal.projo.prefs.TransformDataTypeSupportChain;
import au.net.causal.projo.prefs.TransformGetChain;
import au.net.causal.projo.prefs.TransformPutChain;
import au.net.causal.projo.prefs.TransformRemoveChain;
import au.net.causal.projo.prefs.TransformResult;

/**
 * A transformer that will instantly fail when any {@link Secure} keys are encountered.  This is useful when there are secure nodes but no security has been
 * configured.
 * 
 * @author prunge
 */
public class AlwaysFailSecurityTransformer implements PreferenceTransformer
{
	@Override
	public <T> TransformResult<T> applyGet(String key, PreferenceKeyMetadata<T> keyMetadata, TransformGetChain chain) throws PreferencesException
	{
		if (!isSecure(keyMetadata))
			return(null);
		
		throw new PreferencesException("Attempt to read secure value under '" + key + "' but no security configured.");
	}

	@Override
	public <T> boolean applyPut(String key, T value, PreferenceKeyMetadata<T> keyMetadata, TransformPutChain chain) throws PreferencesException
	{
		if (!isSecure(keyMetadata))
			return(false);
		
		throw new PreferencesException("Attempt to store secure value under '" + key + "' but no security configured."); 
	}

	@Override
	public <T> boolean applyRemove(String key, PreferenceKeyMetadata<T> keyMetadata, TransformRemoveChain chain) throws PreferencesException
	{
		if (!isSecure(keyMetadata))
			return(false);
		
		//It's safe to remove secure keys
		chain.removeValue(key, keyMetadata.withDataType(byte[].class).withoutAnnotationType(Secure.class));
		
		return(true);
		
	}

	@Override
	public DataTypeSupport applyDataTypeSupport(PreferenceKeyMetadata<?> keyMetadata, TransformDataTypeSupportChain chain) throws PreferencesException
	{
		//Since we don't add any supported data types just ignore this
		return(null);
	}
	
	/**
	 * Returns true if a preference key is annoated with {@literal @}{@link Secure} annotation.
	 * 
	 * @param keyMetadata key metadata.
	 * 
	 * @return true if annotated with {@literal @}{@link Secure} annotation, false if not.
	 */
	private boolean isSecure(PreferenceKeyMetadata<?> keyMetadata)
	{
		return(keyMetadata.isAnnotationPresent(Secure.class));
	}
}
