package au.net.causal.projo.prefs;

import java.util.regex.Pattern;

import org.apache.commons.lang3.ClassUtils;
import org.apache.commons.lang3.StringUtils;

import au.net.causal.projo.annotation.Embedded;
import au.net.causal.projo.annotation.PartialPreferences;
import au.net.causal.projo.annotation.Preference;
import au.net.causal.projo.annotation.PreferenceRoot;
import au.net.causal.projo.bind.EntityMetadataException;
import au.net.causal.projo.metadata.AnnotatedSettingsMetadataReader;
import au.net.causal.projo.metadata.SettingsEntityMetadata;
import au.net.causal.projo.metadata.SettingsMetadataReader;
import au.net.causal.projo.metadata.SettingsPropertyMetadata;

import com.google.common.reflect.TypeToken;

/**
 * Retrieves and loads preferences from a store, binding values to class properties.
 * <p>
 * 
 * A projo store can be more easily created and configured through a {@link ProjoStoreBuilder}, which provides a fluent API.
 *  
 * @author prunge
 * 
 * @see ProjoStoreBuilder
 */
public class ProjoStore implements AutoCloseable
{
	private final PreferenceNode node;
	
	//TODO might need some metadata caching later on
	private final SettingsMetadataReader metadataReader = new AnnotatedSettingsMetadataReader();

	private boolean preferenceRootSettingUsed = true;
	
	/**
	 * Creates a <code>ProjoStore</code> from a base preference node.
	 * <p>
	 * 
	 * Depending on the {@linkplain #setPreferenceRootUsed(boolean) preference root used} setting, this node may be either used directly for storing configuration
	 * or a child of this node will be used.
	 * 
	 * @param node the base preference node.
	 * 
	 * @throws NullPointerException if <code>node</code> is null.
	 */
	public ProjoStore(PreferenceNode node)
	{
		if (node == null)
			throw new NullPointerException("node == null");
		
		this.node = node;
	}

	/**
	 * Returns whether the {@link PreferenceRoot} setting is honoured.  
	 * 
	 * @return true if the preference root is honoured, false if it is ignored.
	 * 
	 * @see #setPreferenceRootUsed(boolean)
	 */
	public boolean isPreferenceRootSettingUsed()
	{
		return(preferenceRootSettingUsed);
	}

	/**
	 * Sets whether the {@link PreferenceRoot} setting on preference POJOs is used.
	 * <p>
	 * 
	 * If false, the base node passed into the {@linkplain #ProjoStore(PreferenceNode) constructor} will be used directly for 
	 * settings.  If true, the node name will be derived from the {@link PreferenceRoot} annotation setting on settings POJOs or calculated from the 
	 * package and class name if it doesn't exist and the corresponding child node will be used. 
	 * 
	 * @param preferenceRootSettingUsed true to use preference root setting, false to ignore it.
	 * 
	 * @see #isPreferenceRootSettingUsed()
	 */
	public void setPreferenceRootUsed(boolean preferenceRootSettingUsed)
	{
		this.preferenceRootSettingUsed = preferenceRootSettingUsed;
	}
	
	/**
	 * Determines the actual preference node to use for reading/writing preferences for a Projo.  Takes into account the 
	 * {@linkplain #setPreferenceRootUsed(boolean) preference root used} setting.
	 * 
	 * @param metadata metdata for the entity.
	 * 
	 * @return the actual preference node to use.
	 * 
	 * @throws PreferencesException if an error occurs reading nodes from the preference store.
	 */
	private PreferenceNode nodeToUseForMetadata(SettingsEntityMetadata metadata)
	throws PreferencesException
	{
		if (!isPreferenceRootSettingUsed())
			return(node);
		
		PreferenceRoot rootConfig = metadata.getAnnotation(PreferenceRoot.class);
		String rootName;
		if (rootConfig == null || StringUtils.isEmpty(rootConfig.name()))
			rootName = metadata.getEntityType().getCanonicalName();
		else
			rootName = rootConfig.name();
		
		PreferenceNode curNode = node;
		for (String nodeToken : rootName.split(Pattern.quote(ClassUtils.PACKAGE_SEPARATOR)))
		{
			curNode = curNode.getChildNode(nodeToken);
		}
		
		return(curNode);
	}

	/**
	 * Reads preferences from the store, creating a preference object of the specified type.
	 * <p>
	 * 
	 * <code>type</code> must be annotated appropriately using Projo annotations, and have a public zero argument contructor.
	 * 
	 * @param type the type of preference object to create and fill in.
	 * 
	 * @return the created preference object.
	 * 
	 * @throws NullPointerException if <code>type</code> is null.
	 * @throws PreferencesException if an error occurs instantiating <code>type</code>, or if an error occurs reading from the preference store.
	 * @throws EntityMetadataException if there is a problem with the annotations or properties of <code>type</code>.
	 * 
	 * @see PreferenceRoot
	 * @see Preference
	 */
	public <T> T read(Class<T> type)
	throws PreferencesException, EntityMetadataException
	{
		if (type == null)
			throw new NullPointerException("type == null");
		
		SettingsEntityMetadata metadata = metadataReader.read(type);
		if (metadata == null)
			throw new EntityMetadataException("Not a Projo: " + type.getCanonicalName());
		
		PreferenceNode nodeToUse = nodeToUseForMetadata(metadata);
		
		return(readInternal(type, metadata, nodeToUse));
	}
	
	/**
	 * Internal preference reading method that has the node to read from and the metadata for a type already defined.
	 *  
	 * @param type the type of preference object being instantiated.
	 * @param metadata metadata for <code>type</code>.
	 * @param fromNode the preference node to read from.
	 * 
	 * @return an instance of <code>type</code> that has its preference properties filled in.
	 * 
	 * @throws PreferencesException if an error occurs instantiating <code>type</code>, or if an error occurs reading from the preference store.
	 * @throws EntityMetadataException if there is a problem with the annotations or properties of <code>type</code>.
	 */
	private <T> T readInternal(Class<T> type, SettingsEntityMetadata metadata, PreferenceNode fromNode)
	throws PreferencesException, EntityMetadataException
	{
		if (type == null)
			throw new NullPointerException("type == null");
		
		try
		{
			T projo = type.getConstructor().newInstance();
			return(readInternal(projo, metadata, fromNode));
		}
		catch (ReflectiveOperationException e)
		{
			throw new PreferencesException("Could not create instance of " + type.getCanonicalName() + ": " + e, e);
		}
	}
	
	/**
	 * Reads preferences from the store into an already existing preference object.
	 * 
	 * @param projo the preference object. This must be annotated appropriately using Projo annotations.
	 * 
	 * @return the same object, <code>projo</code>.
	 * 
	 * @throws NullPointerException if <code>projo</code> is null.
	 * @throws PreferencesException if an error occurs reading from the preference store or filling in <code>projo</code>.
	 * @throws EntityMetadataException if there is a problem with the annotations or properties of <code>projo</code>.
	 * 
	 * @see PreferenceRoot
	 * @see Preference
	 */
	public <T> T read(T projo)
	throws PreferencesException, EntityMetadataException
	{
		if (projo == null)
			throw new NullPointerException("projo == null");
		
		SettingsEntityMetadata metadata = metadataReader.read(projo.getClass());
		if (metadata == null)
			throw new EntityMetadataException("Not a Projo: " + projo.getClass().getCanonicalName());
		
		PreferenceNode nodeToUse = nodeToUseForMetadata(metadata);
		
		return(readInternal(projo, metadata, nodeToUse));
	}
	
	/**
	 * Internal preference reading method that has the node to read from and the metadata for a type already defined which fills in an existing
	 * preference object.
	 *  
	 * @param projo the preference object.  This must be annotated appropriately using Projo annotations.
	 * @param metadata metadata for <code>type</code>.
	 * @param fromNode the preference node to read from.
	 * 
	 * @return the same object that was filled in, <code>projo</code>.
	 * 
	 * @throws PreferencesException if an error occurs reading from the preference store or filling in <code>projo</code>.
	 * @throws EntityMetadataException if there is a problem with the annotations or properties of <code>projo</code>.
	 */
	private <T> T readInternal(T projo, SettingsEntityMetadata metadata, PreferenceNode fromNode)
	throws PreferencesException, EntityMetadataException
	{
		if (projo == null)
			throw new NullPointerException("projo == null");
		
		for (SettingsPropertyMetadata property : metadata.getProperties())
		{
			//Nested entity?
			if (property.getTargetEntity() != null)
			{
				PreferenceNode targetNode = targetNodeForChildEntity(property, fromNode);
				
				Object nestedProjo = null;
				try
				{
					if (property.getAccessor().isReadable())
						nestedProjo = property.getAccessor().get(projo);
				
					if (nestedProjo == null)
					{
						nestedProjo = readInternal(property.getTargetEntity().getEntityType(), property.getTargetEntity(), targetNode);
						if (property.getAccessor().isWritable())
							property.getAccessor().set(projo, nestedProjo);
						else
							throw new PreferencesException("Nested projo on " + projo.getClass().getCanonicalName() + "." + property.getName() + " was null and could not be saved back into parent.");
					}
					else
						nestedProjo = readInternal(nestedProjo, property.getTargetEntity(), targetNode);
				}
				catch (ReflectiveOperationException e)
				{
					throw new PreferencesException("Failed to read property " + property.getName() + ": " + e, e);
				}
			}
			else
			{
				PreferenceKeyMetadata<?> keyMeta = new PreferenceKeyMetadata<>(property.getAnnotations(), TypeToken.of(property.getPropertyType()));
				String key = property.getKey();
				Object value = fromNode.getValue(key, keyMeta);
				
				try
				{
					property.getAccessor().set(projo, value);
				}
				catch (ReflectiveOperationException e)
				{
					throw new PreferencesException("Failed to set property " + property.getName() + ": " + e, e);
				}
			}
		}
		
		return(projo);
	}
	
	/**
	 * Saves preferences from a preference object into the preference store.
	 * 
	 * @param projo the preference object.  This must be annotated appropriately using Projo annotations.
	 * 
	 * @throws NullPointerException if <code>projo</code> is null.
	 * @throws PreferencesException if an error occurs writing to the preference store or reading from <code>projo</code>'s properties.
	 * @throws EntityMetadataException if there is a problem with the annotations or properties of <code>projo</code>.
	 * 
	 * @see PreferenceRoot
	 * @see Preference
	 */
	public void save(Object projo)
	throws PreferencesException, EntityMetadataException
	{
		SettingsEntityMetadata metadata = metadataReader.read(projo.getClass());
		if (metadata == null)
			throw new EntityMetadataException("Not a Projo: " + projo.getClass().getCanonicalName());
		
		PreferenceNode nodeToUse = nodeToUseForMetadata(metadata);
		
		saveInternal(projo, metadata, nodeToUse);
	}
	
	/**
	 * For a nested Projo's property, finds the target preference node from a base node.  This uses the metadata of the property to determine the child 
	 * node's name. 
	 * 
	 * @param property the property being processed.
	 * @param base the base node.
	 * 
	 * @return the child node to use for the property.
	 * 
	 * @throws PreferencesException if an error occurs getting the child node from the preference store.
	 */
	private PreferenceNode targetNodeForChildEntity(SettingsPropertyMetadata property, PreferenceNode base)
	throws PreferencesException
	{
		//Determine target node
		PreferenceNode targetNode;
		if (property.isAnnotationPresent(Embedded.class))
			targetNode = base;
		else
		{
			String childNodeName = property.getKey();
			targetNode = base.getChildNode(childNodeName);
		}
		
		return(targetNode);
	}
	
	/**
	 * Internal Projo save method that has metadata and target node already calculated.
	 * 
	 * @param projo the preference object being saved.
	 * @param metadata metadata for <code>projo</code>.
	 * @param toNode the target node to write the projo to.
	 * 
	 * @throws PreferencesException if an error occurs writing to the target node or reading from <code>projo</code>'s properties.
	 * @throws EntityMetadataException if there is a problem with the annotations or properties of <code>projo</code>.
	 */
	private void saveInternal(Object projo, SettingsEntityMetadata metadata, PreferenceNode toNode)
	throws PreferencesException, EntityMetadataException
	{
		if (projo == null)
			throw new NullPointerException("projo == null");
		
		for (SettingsPropertyMetadata property : metadata.getProperties())
		{
			PreferenceKeyMetadata<?> keyMeta = new PreferenceKeyMetadata<>(property.getAnnotations(), TypeToken.of(property.getPropertyType()));
			
			//Nested projo
			if (property.getTargetEntity() != null)
			{
				PreferenceNode targetNode = targetNodeForChildEntity(property, toNode);
				try
				{
					Object childProjo = property.getAccessor().get(projo);
					if (childProjo != null)
						saveInternal(childProjo, property.getTargetEntity(), targetNode);
				}
				catch (ReflectiveOperationException e)
				{
					throw new PreferencesException("Failed to read property " + property.getName() + ": " + e, e);
				}
			}
			else
				saveKey(projo, property, keyMeta, toNode);
		}
	}
	
	/**
	 * Removes all preferences defined in <code>projo</code> from the preference store.  
	 * <p>
	 * 
	 * The values of properties are irrelevant and a processed even if they have null values.  Only the metadata of the projo class is used.
	 * <p>
	 * 
	 * If the preference object type is annotated with {@literal @}{@link PartialPreferences}, then only the properties defined in the preference object
	 * are removed.  If this annotation does not exist, all keys and values are removed from the node.
	 * 
	 * @param projo a preference object.  This must be annotated appropriately using Projo annotations.
	 * 
	 * @throws NullPointerException if <code>projo</code> is null.
	 * @throws PreferencesException if an error occurs removing preferences from the store.
	 * @throws EntityMetadataException if there is a problem with the annotations or properties of <code>projo</code>.
	 */
	public void remove(Object projo)
	throws PreferencesException, EntityMetadataException	
	{
		if (projo == null)
			throw new NullPointerException("projo == null");
		
		SettingsEntityMetadata metadata = metadataReader.read(projo.getClass());
		if (metadata == null)
			throw new EntityMetadataException("Not a Projo: " + projo.getClass().getCanonicalName());
		
		PreferenceNode nodeToUse = nodeToUseForMetadata(metadata);
		
		removeInternal(projo, projo.getClass(), metadata, nodeToUse, false);
	}
	
	/**
	 * Removes all preferences defined in <code>projoType</code> from the preference store.
	 * <p>
	 * 
	 * If the preference object type is annotated with {@literal @}{@link PartialPreferences}, then only the properties defined in the preference object
	 * are removed.  If this annotation does not exist, all keys and values are removed from the node.
	 * 
	 * @param projoType the class of a preference object.  This must be annotated appropriately using Projo annotations.
	 * 
	 * @throws NullPointerException if <code>projoType</code> is null.
	 * @throws PreferencesException if an error occurs removing preferences from the store.
	 * @throws EntityMetadataException if there is a problem with the annotations or properties of <code>projoType</code>.
	 */
	public void remove(Class<?> projoType)
	throws PreferencesException, EntityMetadataException	
	{
		if (projoType == null)
			throw new NullPointerException("projoType == null");

		SettingsEntityMetadata metadata = metadataReader.read(projoType);
		if (metadata == null)
			throw new EntityMetadataException("Not a Projo: " + projoType.getCanonicalName());
		
		PreferenceNode nodeToUse = nodeToUseForMetadata(metadata);
		
		removeInternal(null, projoType, metadata, nodeToUse, false);
	}
	
	/**
	 * Internal preference value removal method.  
	 * 
	 * @param projo the preference object being processed.  May be null, in which case <code>projoType</code> is used.
	 * @param projoType the preference object type being processed.
	 * @param metadata metadata for the preference object.
	 * @param fromNode the node keys and values are being removed from.
	 * @param forcePartialPreferences if true, {@link PartialPreferences} will be assumed even if the annotation is not present in the metadata.  This
	 * 			is used for child projo objects.
	 * 
	 * @throws PreferencesException if an error occurs removing values from the preference store.
	 * @throws EntityMetadataException if there is a problem with the annotations or properties of <code>projoType</code>.
	 */
	private void removeInternal(Object projo, Class<?> projoType, SettingsEntityMetadata metadata, PreferenceNode fromNode, boolean forcePartialPreferences)
	throws PreferencesException, EntityMetadataException
	{		
		if (forcePartialPreferences || metadata.isAnnotationPresent(PartialPreferences.class))
		{
			for (SettingsPropertyMetadata property : metadata.getProperties())
			{
				//Nested projos
				if (property.getTargetEntity() != null)
				{
					PreferenceNode targetNode = targetNodeForChildEntity(property, fromNode);
					try
					{
						Object childProjo;
						if (projo == null)
							childProjo = null;
						else
							childProjo = property.getAccessor().get(projo);
						
						boolean forcePartialPreferencesOnChild;
						if (targetNode == fromNode) //Same node, so we force partial preferences
							forcePartialPreferencesOnChild = true;
						else
							forcePartialPreferencesOnChild = false;
						
						removeInternal(childProjo, TypeToken.of(property.getPropertyType()).getRawType(), property.getTargetEntity(), targetNode, forcePartialPreferencesOnChild);
					}
					catch (ReflectiveOperationException e)
					{
						throw new PreferencesException("Failed to read property " + property.getName() + ": " + e, e);
					}
				}
				else
				{
					PreferenceKeyMetadata<?> keyMeta = new PreferenceKeyMetadata<>(property.getAnnotations(), TypeToken.of(property.getPropertyType()));
					String key = property.getKey();
					fromNode.removeValue(key, keyMeta);
				}
			}
		}
		else
		{
			fromNode.removeAllValues();
			
			//Nested projos
			for (SettingsPropertyMetadata property : metadata.getProperties())
			{
				if (property.getTargetEntity() != null)
				{
					PreferenceNode targetNode = targetNodeForChildEntity(property, fromNode);
					try
					{
						Object childProjo = property.getAccessor().get(projo);
						if (targetNode != fromNode) //Only wipe out child nodes as the other values are already gone
							removeInternal(childProjo, TypeToken.of(property.getPropertyType()).getRawType(), property.getTargetEntity(), targetNode, false);
					}
					catch (ReflectiveOperationException e)
					{
						throw new PreferencesException("Failed to read property " + property.getName() + ": " + e, e);
					}
				}
			}
		}
		
	}
	
	/**
	 * Saves the value of a single key to the preference store.
	 * 
	 * @param projo the preference object being processed.
	 * @param property the property of the preference object being processed.
	 * @param keyMeta metadata for the key being processed.
	 * @param toNode the preference node being saved to.
	 * 
	 * @throws PreferencesException if an error occurs writing to the preference store or reading the property from the projo.
	 * @throws EntityMetadataException if there is a problem with the annotations or properties of <code>projo</code>.
	 */
	private <T> void saveKey(Object projo, SettingsPropertyMetadata property, PreferenceKeyMetadata<T> keyMeta, PreferenceNode toNode)
	throws PreferencesException, EntityMetadataException
	{
		String key = property.getKey();
		try
		{
			T value = (T)property.getAccessor().get(projo);
			toNode.putValue(key, value, keyMeta);
		}
		catch (ReflectiveOperationException e)
		{
			throw new PreferencesException("Failed to read property " + property.getName() + ": " + e, e);
		}
	}
	
	/**
	 * Flushes any cached data through to the underlying store. Some preference stores may cache data in memory.
	 * 
	 * @throws PreferencesException if an error occurs.
	 */
	public void flush()
	throws PreferencesException
	{
		node.flush();
	}
	
	/**
	 * Closes the preference store.  No further action should be performed on it.  This will also cause any cached data to be 
	 * {@linkplain #flush() flushed}.
	 * 
	 * @throws PreferencesException if an error occurs.
	 */
	@Override
	public void close()
	throws PreferencesException
	{
		node.close();
	}
}
