package au.net.causal.projo.prefs;

import au.net.causal.projo.annotation.MustUnderstand;

/**
 * Metadata interface for a preference store.  Allows preference stores to be queried for their data type support.
 * 
 * @author prunge
 */
public interface PreferenceStoreMetadata
{
	/**
	 * Returns whether the data type and annotations are supported by the preference store.
	 * <p>
	 * 
	 * <h3>Note for preference store implementations handling annotations of key metadata</h3>
	 * Annotations of the key metadata can be ignored if they are not recognized, however if any annotation present on the key metadata that is marked with 
	 * {@literal @}{@link MustUnderstand} is not supported natively by the store then this method <i>must</i> return <code>false</code>.
	 * 
	 * @param keyType the key metadata to check for support.
	 * 
	 * @return true if the key metadata is supported, false if not.
	 *
	 * @throws NullPointerException if <code>keyType</code> is null.
	 * @throws PreferencesException if an error occurs accessing the preference store.
	 */
	public boolean isDataTypeSupported(PreferenceKeyMetadata<?> keyType)
	throws PreferencesException;
}
