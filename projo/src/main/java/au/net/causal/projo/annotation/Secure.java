package au.net.causal.projo.annotation;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * When a preference is marked with this annotation, it will be stored securely.
 * <p>
 * 
 * This might cause the value to be encrypted or the entire preference file to be secured at the file system level.
 * <p>
 * 
 * If there is no available secure method of storing the value, the preference storage operation will fail.
 * 
 * @author prunge
 */
@MustUnderstand
@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.FIELD, ElementType.METHOD})
public @interface Secure
{

}
