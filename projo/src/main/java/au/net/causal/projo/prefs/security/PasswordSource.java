package au.net.causal.projo.prefs.security;

import au.net.causal.projo.prefs.PreferencesException;

/**
 * Reads password from either the user or another source.  Can be used for the source of a master password for encrypting settings.
 * 
 * @author prunge
 */
public interface PasswordSource
{
	/**
	 * Reads the password.
	 * 
	 * @param mode the mode used which may affect UI for reading the password. 
	 * 
	 * @return the password that was read, or <code>null</code> if, through some user interaction, the process was cancelled.
	 * 
	 * @throws NullPointerException if <code>mode</code> is null.
	 * @throws PreferencesException if an error occurs reading the password.
	 */
	public char[] readPassword(Mode mode)
	throws PreferencesException;
	
	/**
	 * The mode used for reading passwords.  This might affect the UI, for example, when reading a password for encryption the user might be asked to 
	 * verify the password.
	 * 
	 * @author prunge
	 */
	public static enum Mode
	{
		ENCRYPTION,
		DECRYPTION;
	}
}
