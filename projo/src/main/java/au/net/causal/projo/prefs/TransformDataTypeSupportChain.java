package au.net.causal.projo.prefs;

/**
 * A chain allows transformers to call other transformers indirectly.  A single transformer node call might result in a chain of transformers being
 * called.
 * 
 * @author prunge
 */
public interface TransformDataTypeSupportChain
{
	/**
	 * Checks whether a preference key is supported by the chain or the underlying store from the current transform phase.
	 * 
	 * @param keyMetadata the key metadata to check.
	 * 
	 * @return true if the data type is supported either by other transformers or the underlying store, false otherwise.
	 * 
	 * @throws NullPointerException if <code>keyMetadata</code> is null.
	 * @throws PreferencesException if an error occurs.
	 */
	public boolean isDataTypeSupported(PreferenceKeyMetadata<?> keyMetadata)
	throws PreferencesException;
	
	/**
	 * Checks whether a preference key is supported by the underlying store from the current transform phase.  Other transformers are not checked.
	 * <p>
	 * 
	 * Useful for determining whether a transformer is needed or not - if the native store supports a particular data type then usually a transform should 
	 * not do any conversion itself.
	 * 
	 * @param keyMetadata the key metadata to check.
	 * 
	 * @return true if the data type is supported by the underlying store, false otherwise.
	 * 
	 * @throws NullPointerException if <code>keyMetadata</code> is null.
	 * @throws PreferencesException if an error occurs.
	 */
	public boolean isDataTypeSupportedNatively(PreferenceKeyMetadata<?> keyMetadata)
	throws PreferencesException;
	
	/**
	 * Checks whether a preference key is supported by the chain or the underlying store, restarting the chain at the first phase again with a new 
	 * store endpoint.
	 * <p>
	 * 
	 * This is used when a particular target datatype is required even if the native store offers more.
	 * 
	 * @param keyMetadata the key metadata to check.
	 * @param newEndpoint the endpoint will be used in place of the underlying store in the new chain.
	 * 
	 * @return true if the data type is supported either by other transformers or the underlying store, false otherwise.
	 * 
	 * @throws NullPointerException if <code>keyMetadata</code> or <code>newEndpoint</code> is null.
	 * @throws PreferencesException if an error occurs.
	 */
	public boolean isDataTypeSupportedWithRestartedChain(PreferenceKeyMetadata<?> keyMetadata, TransformDataTypeSupportChain newEndpoint)
	throws PreferencesException;
	
	/**
	 * Checks whether a preference key is supported by the underlying store, restarting the chain at the first phase again with a new store endpoint.  
	 * <p>
	 * 
	 * This is used when a particular target datatype is required even if the native store offers more.
	 * 
	 * @param keyMetadata the key metadata to check.
	 * @param newEndpoint the endpoint will be used in place of the underlying store in the new chain.
	 * 
	 * @return true if the data type is supported by the underlying store, false otherwise.
	 * 
	 * @throws NullPointerException if <code>keyMetadata</code> or <code>newEndpoint</code> is null.
	 * @throws PreferencesException if an error occurs.
	 */
	public boolean isDataTypeSupportedNativelyWithRestartedChain(PreferenceKeyMetadata<?> keyMetadata, TransformDataTypeSupportChain newEndpoint)
	throws PreferencesException;
}
